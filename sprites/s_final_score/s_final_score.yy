{
    "id": "036d2981-14ce-47a1-90c4-206b0c476cc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_final_score",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ae0dd36-2eaf-4b3d-8720-be157885e848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "036d2981-14ce-47a1-90c4-206b0c476cc7",
            "compositeImage": {
                "id": "e407d412-fed8-4b64-854c-481c7b0c9b82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae0dd36-2eaf-4b3d-8720-be157885e848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a018576-4fd4-4ec4-b458-3b2c7cfa9dff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae0dd36-2eaf-4b3d-8720-be157885e848",
                    "LayerId": "387f567f-d49f-42f3-889f-68abd05a7f14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "387f567f-d49f-42f3-889f-68abd05a7f14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "036d2981-14ce-47a1-90c4-206b0c476cc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}