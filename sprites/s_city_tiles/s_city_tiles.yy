{
    "id": "cc03430c-75fc-4d56-b664-679dbd53398d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_city_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 0,
    "bbox_right": 122,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d558c142-77d4-42af-b991-cf6538b891db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc03430c-75fc-4d56-b664-679dbd53398d",
            "compositeImage": {
                "id": "9bf276cd-a529-4621-ac42-caa44ead5433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d558c142-77d4-42af-b991-cf6538b891db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9b132c-d6cd-42b5-8702-a3c66d25b633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d558c142-77d4-42af-b991-cf6538b891db",
                    "LayerId": "ce5e3c02-079c-44d9-8e5b-2d24b92abd1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "ce5e3c02-079c-44d9-8e5b-2d24b92abd1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc03430c-75fc-4d56-b664-679dbd53398d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 123,
    "xorig": 0,
    "yorig": 0
}