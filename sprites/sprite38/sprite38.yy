{
    "id": "b80843c9-4879-4ec3-b939-19ed9ae02bba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite38",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "436ade6c-a3ce-4375-b734-bb76528a6a0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b80843c9-4879-4ec3-b939-19ed9ae02bba",
            "compositeImage": {
                "id": "6d327c58-578b-49da-a28b-8f43927aef80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "436ade6c-a3ce-4375-b734-bb76528a6a0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc782e7a-e405-471c-8ed9-572672d72cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "436ade6c-a3ce-4375-b734-bb76528a6a0f",
                    "LayerId": "a32fd08e-126b-4da6-bd45-bb66011bd72c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a32fd08e-126b-4da6-bd45-bb66011bd72c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b80843c9-4879-4ec3-b939-19ed9ae02bba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}