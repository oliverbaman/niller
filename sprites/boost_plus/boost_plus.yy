{
    "id": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "boost_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 4,
    "bbox_right": 8,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c535ac80-246c-4323-88c4-fbc84ca23a88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "compositeImage": {
                "id": "7eb8a680-490d-4228-8d6d-d5ebb7ec89c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c535ac80-246c-4323-88c4-fbc84ca23a88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5adfe96e-3b3c-4505-b7de-ffea6ec37cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c535ac80-246c-4323-88c4-fbc84ca23a88",
                    "LayerId": "9f38e7b4-b204-4277-87da-93765bc4ee9e"
                }
            ]
        },
        {
            "id": "8841a0ec-718a-45ab-9c29-acbde081b3fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "compositeImage": {
                "id": "d5b6c888-7947-42b0-ad82-87be9b6578af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8841a0ec-718a-45ab-9c29-acbde081b3fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80a7283a-b7c6-46b0-b70c-d97b807fd4e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8841a0ec-718a-45ab-9c29-acbde081b3fc",
                    "LayerId": "9f38e7b4-b204-4277-87da-93765bc4ee9e"
                }
            ]
        },
        {
            "id": "8c90e2c5-27ce-4a3e-8f41-c1d8536f71c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "compositeImage": {
                "id": "84efc681-97f2-4380-bd05-05a4f270f97a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c90e2c5-27ce-4a3e-8f41-c1d8536f71c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004786b2-9dce-4e18-b1c6-6f99e003fddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c90e2c5-27ce-4a3e-8f41-c1d8536f71c4",
                    "LayerId": "9f38e7b4-b204-4277-87da-93765bc4ee9e"
                }
            ]
        },
        {
            "id": "0851d732-28c0-428c-a198-6a41e3ca6742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "compositeImage": {
                "id": "29e74f61-b3e9-4871-9de8-ae268b4b7513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0851d732-28c0-428c-a198-6a41e3ca6742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a8b497e-317e-4a8d-9177-a790f9669a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0851d732-28c0-428c-a198-6a41e3ca6742",
                    "LayerId": "9f38e7b4-b204-4277-87da-93765bc4ee9e"
                }
            ]
        },
        {
            "id": "b05b4323-e01c-4820-a7a5-df7eed452019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "compositeImage": {
                "id": "5470c159-1828-4eae-a2ba-0738ad3462f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05b4323-e01c-4820-a7a5-df7eed452019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76652643-0301-4fe9-8d31-a78cd0175ddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05b4323-e01c-4820-a7a5-df7eed452019",
                    "LayerId": "9f38e7b4-b204-4277-87da-93765bc4ee9e"
                }
            ]
        },
        {
            "id": "201a4181-911f-4a54-9f67-b31df15bd912",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "compositeImage": {
                "id": "769304a8-1acd-41df-b458-6e3f6b1eb4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "201a4181-911f-4a54-9f67-b31df15bd912",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbcaf3dd-cdde-47a1-b383-d3ae4455e399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "201a4181-911f-4a54-9f67-b31df15bd912",
                    "LayerId": "9f38e7b4-b204-4277-87da-93765bc4ee9e"
                }
            ]
        },
        {
            "id": "43233d74-b8eb-45ce-93fc-a218c0396385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "compositeImage": {
                "id": "4b0c8803-e406-4bee-a517-3be4ff2c1045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43233d74-b8eb-45ce-93fc-a218c0396385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d8b8b5-e787-49a4-91e7-e6f11d7db01f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43233d74-b8eb-45ce-93fc-a218c0396385",
                    "LayerId": "9f38e7b4-b204-4277-87da-93765bc4ee9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "9f38e7b4-b204-4277-87da-93765bc4ee9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 1,
    "yorig": 8
}