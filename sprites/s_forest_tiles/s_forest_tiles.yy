{
    "id": "e1018b90-40f5-4a90-9844-628fc5039be3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_forest_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 90,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32282cab-033d-4fa4-a3a5-61b3f1146058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1018b90-40f5-4a90-9844-628fc5039be3",
            "compositeImage": {
                "id": "f74cc8d1-96f3-45ac-bfd0-fe9b306e7e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32282cab-033d-4fa4-a3a5-61b3f1146058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b79b1a5-76d8-44b4-8336-6b22a7cf8dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32282cab-033d-4fa4-a3a5-61b3f1146058",
                    "LayerId": "054e90ec-81fe-4add-bc7c-7d5350fa31f1"
                },
                {
                    "id": "a0885e46-8260-4e24-a228-614c9e2c0be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32282cab-033d-4fa4-a3a5-61b3f1146058",
                    "LayerId": "7938df8f-7983-4518-90ef-13a2a35c1e8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "7938df8f-7983-4518-90ef-13a2a35c1e8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1018b90-40f5-4a90-9844-628fc5039be3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 51,
            "visible": true
        },
        {
            "id": "054e90ec-81fe-4add-bc7c-7d5350fa31f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1018b90-40f5-4a90-9844-628fc5039be3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 91,
    "xorig": 0,
    "yorig": 0
}