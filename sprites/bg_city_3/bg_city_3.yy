{
    "id": "cbc8cb33-082b-47a2-a368-52794ad9c647",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_city_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 201,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "250c526f-4693-4019-908a-dbc3bf2922cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbc8cb33-082b-47a2-a368-52794ad9c647",
            "compositeImage": {
                "id": "69da08b1-665d-46b8-9ec6-4fd8f4086483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "250c526f-4693-4019-908a-dbc3bf2922cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4819227f-2e79-4efd-83d7-1cc5459d13f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "250c526f-4693-4019-908a-dbc3bf2922cd",
                    "LayerId": "b0bd8985-e4bc-4fae-8e8d-dba105d8c48f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "b0bd8985-e4bc-4fae-8e8d-dba105d8c48f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbc8cb33-082b-47a2-a368-52794ad9c647",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}