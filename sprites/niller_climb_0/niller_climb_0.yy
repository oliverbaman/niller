{
    "id": "b73087e1-6fcf-4178-982e-27c42ff92564",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_climb_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0a5028f-afb2-4494-a235-ad4e106c6c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73087e1-6fcf-4178-982e-27c42ff92564",
            "compositeImage": {
                "id": "8dc12df4-3d90-4daa-8e42-e9405def7b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0a5028f-afb2-4494-a235-ad4e106c6c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34a0f195-81d5-4725-a9ec-c781663706a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0a5028f-afb2-4494-a235-ad4e106c6c34",
                    "LayerId": "227527db-01d7-4228-a8b0-0d0a1d814b28"
                }
            ]
        },
        {
            "id": "a0def35e-8fcf-4a54-b555-b05c24922647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73087e1-6fcf-4178-982e-27c42ff92564",
            "compositeImage": {
                "id": "b716e8b4-811c-4892-a09f-cef968df1e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0def35e-8fcf-4a54-b555-b05c24922647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fb54b7d-1855-47fd-a2f4-22a4d65d0468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0def35e-8fcf-4a54-b555-b05c24922647",
                    "LayerId": "227527db-01d7-4228-a8b0-0d0a1d814b28"
                }
            ]
        },
        {
            "id": "9ab0cdfb-5068-4081-8ab7-75cebe72212b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73087e1-6fcf-4178-982e-27c42ff92564",
            "compositeImage": {
                "id": "0626df3f-4f4e-409b-b3b8-5666279f8bad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab0cdfb-5068-4081-8ab7-75cebe72212b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "708d7f73-20bb-4023-a21b-70dcc221984f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab0cdfb-5068-4081-8ab7-75cebe72212b",
                    "LayerId": "227527db-01d7-4228-a8b0-0d0a1d814b28"
                }
            ]
        },
        {
            "id": "9a399d4d-0c0c-4324-b557-fc4c97ecd6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73087e1-6fcf-4178-982e-27c42ff92564",
            "compositeImage": {
                "id": "5c45c05f-4fde-417c-b1f2-281f9ff7e02e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a399d4d-0c0c-4324-b557-fc4c97ecd6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847d0078-82c8-4752-ade7-3389df2ab9d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a399d4d-0c0c-4324-b557-fc4c97ecd6e7",
                    "LayerId": "227527db-01d7-4228-a8b0-0d0a1d814b28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "227527db-01d7-4228-a8b0-0d0a1d814b28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b73087e1-6fcf-4178-982e-27c42ff92564",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 6,
    "yorig": 8
}