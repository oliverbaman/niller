{
    "id": "a13f3bb7-c35c-43df-8020-a81149811b1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_boostUR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbf05229-408e-4c67-9b06-ec3a4c9e95a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a13f3bb7-c35c-43df-8020-a81149811b1e",
            "compositeImage": {
                "id": "feb1ca5b-10d6-4503-ac41-654301a80891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf05229-408e-4c67-9b06-ec3a4c9e95a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7682cc7-96a9-44e5-86db-197ef8a962fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf05229-408e-4c67-9b06-ec3a4c9e95a4",
                    "LayerId": "90bf9b35-cf6e-47f7-8846-538bf2cf197d"
                }
            ]
        },
        {
            "id": "3a9a7a6d-2414-4721-b657-3a19d6b37e71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a13f3bb7-c35c-43df-8020-a81149811b1e",
            "compositeImage": {
                "id": "bda564e2-c2d3-4965-a8f7-a89654c324d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a9a7a6d-2414-4721-b657-3a19d6b37e71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa480d39-52d3-4c53-9a94-8af14e11c027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a9a7a6d-2414-4721-b657-3a19d6b37e71",
                    "LayerId": "90bf9b35-cf6e-47f7-8846-538bf2cf197d"
                }
            ]
        },
        {
            "id": "070f2282-a5f3-4466-90e4-b9c5badbeffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a13f3bb7-c35c-43df-8020-a81149811b1e",
            "compositeImage": {
                "id": "160fff2a-4c5a-4f38-a4f8-0e06c1321c11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070f2282-a5f3-4466-90e4-b9c5badbeffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b040d568-07fb-4bd4-b23f-8f626e5e0c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070f2282-a5f3-4466-90e4-b9c5badbeffb",
                    "LayerId": "90bf9b35-cf6e-47f7-8846-538bf2cf197d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "90bf9b35-cf6e-47f7-8846-538bf2cf197d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a13f3bb7-c35c-43df-8020-a81149811b1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 10,
    "yorig": 8
}