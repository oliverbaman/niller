{
    "id": "2a27e681-aa0f-4ebd-ad6b-e9d2ef2a82e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_forest_51",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 422,
    "bbox_left": 0,
    "bbox_right": 579,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea9184b8-63a3-4fb9-a7e5-8897d0fcbd5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a27e681-aa0f-4ebd-ad6b-e9d2ef2a82e5",
            "compositeImage": {
                "id": "d3ef0fa7-a092-4ce7-b919-b7cc720bc35d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea9184b8-63a3-4fb9-a7e5-8897d0fcbd5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdeb54ad-dbc0-450c-bb09-dd5f7db32dd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea9184b8-63a3-4fb9-a7e5-8897d0fcbd5d",
                    "LayerId": "f0a7389c-6879-4316-9171-cb9ce253a7ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 425,
    "layers": [
        {
            "id": "f0a7389c-6879-4316-9171-cb9ce253a7ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a27e681-aa0f-4ebd-ad6b-e9d2ef2a82e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 580,
    "xorig": 0,
    "yorig": 0
}