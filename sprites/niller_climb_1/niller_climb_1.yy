{
    "id": "4e239151-aa90-412a-889b-506cf8dd3979",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_climb_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "282b454f-094f-47cf-8611-29244957a607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e239151-aa90-412a-889b-506cf8dd3979",
            "compositeImage": {
                "id": "b12a868e-c6dd-451e-b2a9-e3e0cf6be175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282b454f-094f-47cf-8611-29244957a607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5614eb-48dd-4cdb-b5bc-56918bb6572d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282b454f-094f-47cf-8611-29244957a607",
                    "LayerId": "7a52b8a9-7254-42bf-991c-da342689286e"
                }
            ]
        },
        {
            "id": "d8128644-7486-4e2f-93f1-d9ea0b91c682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e239151-aa90-412a-889b-506cf8dd3979",
            "compositeImage": {
                "id": "3be26023-7526-44c5-9b7d-b3763e98b8cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8128644-7486-4e2f-93f1-d9ea0b91c682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545ea535-0404-4912-a1b3-85f00dbc7c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8128644-7486-4e2f-93f1-d9ea0b91c682",
                    "LayerId": "7a52b8a9-7254-42bf-991c-da342689286e"
                }
            ]
        },
        {
            "id": "ec20ae93-b6b1-4668-9013-608d6c381fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e239151-aa90-412a-889b-506cf8dd3979",
            "compositeImage": {
                "id": "dafc3799-2f71-4173-a65e-d3d73682a3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec20ae93-b6b1-4668-9013-608d6c381fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f18848-9367-458f-92f3-7e23866fd299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec20ae93-b6b1-4668-9013-608d6c381fb4",
                    "LayerId": "7a52b8a9-7254-42bf-991c-da342689286e"
                }
            ]
        },
        {
            "id": "69f5ea26-da45-40f1-ae65-602db412226c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e239151-aa90-412a-889b-506cf8dd3979",
            "compositeImage": {
                "id": "d946d094-5c84-4d88-ae02-16a5b3c101cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69f5ea26-da45-40f1-ae65-602db412226c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "189d4af4-c7a1-4d59-b19e-f03f3a948f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f5ea26-da45-40f1-ae65-602db412226c",
                    "LayerId": "7a52b8a9-7254-42bf-991c-da342689286e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7a52b8a9-7254-42bf-991c-da342689286e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e239151-aa90-412a-889b-506cf8dd3979",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 6,
    "yorig": 8
}