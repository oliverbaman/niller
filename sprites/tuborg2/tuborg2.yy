{
    "id": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tuborg2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 1,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72caf8d7-0495-405a-9ffe-d05e8a852ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
            "compositeImage": {
                "id": "a7fa26a6-d438-45eb-9272-d191abcef85e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72caf8d7-0495-405a-9ffe-d05e8a852ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc086836-1a6e-4266-8768-788b696b8fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72caf8d7-0495-405a-9ffe-d05e8a852ad6",
                    "LayerId": "3915375a-57a6-4e61-a757-f4b9eee9a152"
                }
            ]
        },
        {
            "id": "e6dceb1f-1f10-4b60-9425-62b4a2e2c3d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
            "compositeImage": {
                "id": "a3c691de-34ed-4465-9953-63ce0181fa86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6dceb1f-1f10-4b60-9425-62b4a2e2c3d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca12e1b4-66ef-4195-8adc-93b83abaaa19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6dceb1f-1f10-4b60-9425-62b4a2e2c3d2",
                    "LayerId": "3915375a-57a6-4e61-a757-f4b9eee9a152"
                }
            ]
        },
        {
            "id": "47245d97-a0c8-48a9-91d7-4fa50c77c2e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
            "compositeImage": {
                "id": "e3f4ae9e-e82f-483d-bc97-2840f919df05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47245d97-a0c8-48a9-91d7-4fa50c77c2e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afeacab1-2a71-4ee7-b0cb-619e875d90de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47245d97-a0c8-48a9-91d7-4fa50c77c2e2",
                    "LayerId": "3915375a-57a6-4e61-a757-f4b9eee9a152"
                }
            ]
        },
        {
            "id": "33835d93-c617-4c8c-8f9b-eaa17d9b80ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
            "compositeImage": {
                "id": "ef2187e7-ae2a-4e58-9732-6bb10d953ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33835d93-c617-4c8c-8f9b-eaa17d9b80ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4923c152-6492-458e-99fa-825bd5d05a7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33835d93-c617-4c8c-8f9b-eaa17d9b80ef",
                    "LayerId": "3915375a-57a6-4e61-a757-f4b9eee9a152"
                }
            ]
        },
        {
            "id": "f695819d-8657-449a-8327-cd6f547643c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
            "compositeImage": {
                "id": "8eecc087-309a-4ecc-b0d6-281d410bbf97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f695819d-8657-449a-8327-cd6f547643c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00cc95f0-0ba6-4965-96e8-d33f44e2dbf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f695819d-8657-449a-8327-cd6f547643c5",
                    "LayerId": "3915375a-57a6-4e61-a757-f4b9eee9a152"
                }
            ]
        },
        {
            "id": "082b39a3-0b03-4b87-97d2-68f51cfcb4fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
            "compositeImage": {
                "id": "999ed87c-c99b-40b8-bc83-2d0726334c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "082b39a3-0b03-4b87-97d2-68f51cfcb4fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29dfe6d4-69b8-44d6-b69f-6131aa126ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082b39a3-0b03-4b87-97d2-68f51cfcb4fa",
                    "LayerId": "3915375a-57a6-4e61-a757-f4b9eee9a152"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "3915375a-57a6-4e61-a757-f4b9eee9a152",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 16
}