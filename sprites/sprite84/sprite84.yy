{
    "id": "66e54b8f-219b-469d-a75c-c0510cc591ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite84",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 18,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59cba00c-57ad-4b89-9af2-602317f05dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66e54b8f-219b-469d-a75c-c0510cc591ff",
            "compositeImage": {
                "id": "80ff8fe1-844d-4783-962e-ebd8f99f643c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59cba00c-57ad-4b89-9af2-602317f05dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa6b011-79d5-4f74-8d6f-1ebec06825dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59cba00c-57ad-4b89-9af2-602317f05dcd",
                    "LayerId": "3b766078-4fff-49dd-8f2b-719af2396492"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "3b766078-4fff-49dd-8f2b-719af2396492",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66e54b8f-219b-469d-a75c-c0510cc591ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 11,
    "yorig": 10
}