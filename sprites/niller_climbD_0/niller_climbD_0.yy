{
    "id": "fac70b63-f028-47ec-96f0-4c1ac43f85a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_climbD_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c4f4a09-8337-4ecb-9ef2-95ee8f81c018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fac70b63-f028-47ec-96f0-4c1ac43f85a1",
            "compositeImage": {
                "id": "6b26ab15-e167-4a17-8fe7-b9dffcc1de3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c4f4a09-8337-4ecb-9ef2-95ee8f81c018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dbf4a6f-7ddf-4be0-9006-5b30985c1b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c4f4a09-8337-4ecb-9ef2-95ee8f81c018",
                    "LayerId": "b005e2a3-7365-431d-9e72-92190de01419"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b005e2a3-7365-431d-9e72-92190de01419",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fac70b63-f028-47ec-96f0-4c1ac43f85a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 6,
    "yorig": 8
}