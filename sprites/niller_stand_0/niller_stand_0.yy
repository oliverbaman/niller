{
    "id": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_stand_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "698d2ed1-0571-4ec1-892c-554ec9b3ed9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "88c4d415-8cde-4884-bfd3-e6152cadf95f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698d2ed1-0571-4ec1-892c-554ec9b3ed9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc60ffba-652a-414f-9d40-be0ca927b8e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698d2ed1-0571-4ec1-892c-554ec9b3ed9c",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "5d683077-9a9e-4db1-af34-6fab295684ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698d2ed1-0571-4ec1-892c-554ec9b3ed9c",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "e9b36231-903b-482c-b4e1-683b143bef8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "71bd01b4-1de0-4e25-90d2-0369cb37bda3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b36231-903b-482c-b4e1-683b143bef8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb1556fe-8ed1-49e8-96c0-7bb291b6ff7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b36231-903b-482c-b4e1-683b143bef8f",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "5fc706b4-1278-4dcc-8bb4-8f80e9c1906f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b36231-903b-482c-b4e1-683b143bef8f",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "f51d91f8-06ab-4aac-a231-56dd5cc418f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "6e671bb2-1f3f-4143-9db9-5b32d2d155b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f51d91f8-06ab-4aac-a231-56dd5cc418f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d47ccb85-f764-4b2d-a5cb-1317d7e1e7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51d91f8-06ab-4aac-a231-56dd5cc418f1",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "a969f05b-d4f5-4120-839e-3dab18b7278d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51d91f8-06ab-4aac-a231-56dd5cc418f1",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "26659c5b-780b-445e-9eb6-8d95efdbb1db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "c44c71e8-fa55-4062-80aa-6a8879f691ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26659c5b-780b-445e-9eb6-8d95efdbb1db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26bfc392-0c08-4188-ae9e-7dcf5893e12e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26659c5b-780b-445e-9eb6-8d95efdbb1db",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "83334ab4-32c7-4e4c-aa22-f68cd15b5ded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26659c5b-780b-445e-9eb6-8d95efdbb1db",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "9c9098d7-4a36-4ff8-a3ed-e4416bd3963c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "4d27d87d-01f3-4260-ae91-9bf5aec035b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c9098d7-4a36-4ff8-a3ed-e4416bd3963c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a890b8d-21cb-492a-8d3d-6acbf19229e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c9098d7-4a36-4ff8-a3ed-e4416bd3963c",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "0656ebfa-c820-4f5f-9663-55f524b800bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c9098d7-4a36-4ff8-a3ed-e4416bd3963c",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "76df4527-dd1f-4265-9fc2-61e4da76bea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "d80fb0e9-a7e9-4f63-ae58-f20fd2b34468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76df4527-dd1f-4265-9fc2-61e4da76bea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1367a7-1cab-4248-983f-32cfd4655e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76df4527-dd1f-4265-9fc2-61e4da76bea5",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "16edff28-03b9-461e-9a95-60b6e81db91d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76df4527-dd1f-4265-9fc2-61e4da76bea5",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "c80a4599-cf83-42b2-8e2f-beec7d0ac682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "0ae5aeb8-eff8-470d-b46c-076845b20b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c80a4599-cf83-42b2-8e2f-beec7d0ac682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a47c508-5893-4634-9d1c-ea999da350a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c80a4599-cf83-42b2-8e2f-beec7d0ac682",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "3f79bcf1-ebd3-4602-93c1-83213c104e8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c80a4599-cf83-42b2-8e2f-beec7d0ac682",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "8dd3a58f-fab5-40ed-82e8-969aa3721f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "4da69a93-c15e-467b-8d94-87ca9d49372c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dd3a58f-fab5-40ed-82e8-969aa3721f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d33327fb-3690-40dc-9c45-c3a00e846719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd3a58f-fab5-40ed-82e8-969aa3721f75",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "cead3b41-ecdd-4aad-b7d6-c3d2cdf3a312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd3a58f-fab5-40ed-82e8-969aa3721f75",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "efa120df-de66-4aa2-8565-12bbcc30c5b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "19585c7d-e499-4a8f-9f8b-90c0dd1cab7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efa120df-de66-4aa2-8565-12bbcc30c5b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a02f9690-6bf7-450a-ad7e-bb1485e489c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efa120df-de66-4aa2-8565-12bbcc30c5b2",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "8d905dec-32c5-40c9-8019-3d4fc8bb5009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efa120df-de66-4aa2-8565-12bbcc30c5b2",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "0475af1a-a945-4d4c-871e-029d127a423e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "ea8373fd-938c-4433-84b9-347acdbc984a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0475af1a-a945-4d4c-871e-029d127a423e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cec0747-abfd-4868-87a6-2b22ed37b499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0475af1a-a945-4d4c-871e-029d127a423e",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "8d9e4068-12a9-4ed4-95e2-c6998148f13e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0475af1a-a945-4d4c-871e-029d127a423e",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "9e1f80cb-c689-4fcd-928e-17a2a8c25fc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "a53beef6-a24f-4833-9be8-27bd83320457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e1f80cb-c689-4fcd-928e-17a2a8c25fc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3657e392-e661-46fb-9ef0-1c5ca11a3f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e1f80cb-c689-4fcd-928e-17a2a8c25fc0",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "c233b2e7-981f-47a6-89a7-60a8ae16dd79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e1f80cb-c689-4fcd-928e-17a2a8c25fc0",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        },
        {
            "id": "6c9888f7-4cbf-401d-bfd5-aad7cf3e9f4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "compositeImage": {
                "id": "bec6488a-7b26-4d09-9bc5-a5e7174e6c9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9888f7-4cbf-401d-bfd5-aad7cf3e9f4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f113da0-3d79-424b-9c66-a01dae8dd61c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9888f7-4cbf-401d-bfd5-aad7cf3e9f4a",
                    "LayerId": "bf1c911a-ef27-41d6-a84c-2be863086574"
                },
                {
                    "id": "e51bdb38-a639-43c2-93cc-1dec2279b92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9888f7-4cbf-401d-bfd5-aad7cf3e9f4a",
                    "LayerId": "e9e793d3-ea19-404b-8411-fd30efe8169b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e9e793d3-ea19-404b-8411-fd30efe8169b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bf1c911a-ef27-41d6-a84c-2be863086574",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d3a0ca8-2a1d-4b3b-917d-bd89b40a2a85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}