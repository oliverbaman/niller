{
    "id": "55f6eacf-3a13-401c-90f2-6137b1c42900",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 28,
    "bbox_right": 37,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f69a9a2b-751b-417d-bcc0-bcf5beddafbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "b1e30895-270e-461b-ac2f-c5e0dedfbcfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f69a9a2b-751b-417d-bcc0-bcf5beddafbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "814023b6-029d-4167-8d15-51c37271bcda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f69a9a2b-751b-417d-bcc0-bcf5beddafbf",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "2296bda3-d6d9-4868-a25f-879b04001559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f69a9a2b-751b-417d-bcc0-bcf5beddafbf",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "5a255eef-81dc-4fa7-8f93-394b4f7ec780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "286273eb-fea6-4f81-8304-b09f91509ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a255eef-81dc-4fa7-8f93-394b4f7ec780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9b7e2fe-f1c0-44a5-8584-2f3ee7a4a158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a255eef-81dc-4fa7-8f93-394b4f7ec780",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "17ab0c35-ee88-4efb-9e85-8f289b4a5321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a255eef-81dc-4fa7-8f93-394b4f7ec780",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "6591d307-b4ca-4073-9faf-4669cc2e8924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "ee6e0d46-6a60-4542-b2f2-06a8d5d149d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6591d307-b4ca-4073-9faf-4669cc2e8924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c93b3fc-6bf5-41b7-a5c4-adae02c6fd5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6591d307-b4ca-4073-9faf-4669cc2e8924",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "5b988c1e-c53f-4812-a2b2-f9460a57f11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6591d307-b4ca-4073-9faf-4669cc2e8924",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "e4e2bb05-5bd9-4631-a8c7-a2d474d0afc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "d9ce094a-6ed1-465e-bfc5-909a0457c2af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4e2bb05-5bd9-4631-a8c7-a2d474d0afc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c795711-8e87-4a5c-8b70-92a674fc124d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4e2bb05-5bd9-4631-a8c7-a2d474d0afc7",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "a5f6eb73-37da-489d-829c-3e424b22ec39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4e2bb05-5bd9-4631-a8c7-a2d474d0afc7",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "84bbb31f-3875-4d46-be22-2f2eb28b1fd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "ef4f5822-a642-41c0-aebc-88584d49a4b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84bbb31f-3875-4d46-be22-2f2eb28b1fd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e870620-536e-4830-97c2-46a9c09ddf4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84bbb31f-3875-4d46-be22-2f2eb28b1fd9",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "5e6e51b1-db10-4c0e-89e2-f96f4a9da272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84bbb31f-3875-4d46-be22-2f2eb28b1fd9",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "91a2e9fc-c1b5-4827-9626-cc3cc95b3803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "edd1a762-62ce-4dcb-ac7b-4ebf0956e8a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91a2e9fc-c1b5-4827-9626-cc3cc95b3803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d8d7786-89ca-4e68-80cb-3b0fa589532f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91a2e9fc-c1b5-4827-9626-cc3cc95b3803",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "ac8e10d3-38ba-47d4-b11e-b77d1c8b4e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91a2e9fc-c1b5-4827-9626-cc3cc95b3803",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "67eed86a-43f0-4e90-94ee-5b6637b8f871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "37733417-1d05-4df5-8fa2-21dd131f9694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67eed86a-43f0-4e90-94ee-5b6637b8f871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f606d65d-172b-46cf-8ff9-d6b3abb0fa7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67eed86a-43f0-4e90-94ee-5b6637b8f871",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "985912b7-63dd-4959-bc05-e5fed67d7d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67eed86a-43f0-4e90-94ee-5b6637b8f871",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "e55b510e-cfb7-4dec-8902-2947e2354726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "053fd832-7d3f-4869-8528-4321e5520c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55b510e-cfb7-4dec-8902-2947e2354726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f452be99-898c-4fd4-8bb3-505e6e5e8311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55b510e-cfb7-4dec-8902-2947e2354726",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "18cb7388-a361-4439-b3f2-53d904bdfdbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55b510e-cfb7-4dec-8902-2947e2354726",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        },
        {
            "id": "36390402-a47c-4929-a1ec-f868e393a66b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "compositeImage": {
                "id": "c4423a6b-d648-4ea6-92f1-dad8b55c993e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36390402-a47c-4929-a1ec-f868e393a66b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dc75e74-d053-4667-9a48-694b9de31493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36390402-a47c-4929-a1ec-f868e393a66b",
                    "LayerId": "8df4fbbb-20cc-4553-9aaa-84477f219dfc"
                },
                {
                    "id": "12f91990-8bc3-4fbe-95c2-242e220d71c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36390402-a47c-4929-a1ec-f868e393a66b",
                    "LayerId": "7034b85f-4b30-4f5d-b10d-62aba26fb824"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "7034b85f-4b30-4f5d-b10d-62aba26fb824",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8df4fbbb-20cc-4553-9aaa-84477f219dfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55f6eacf-3a13-401c-90f2-6137b1c42900",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 33
}