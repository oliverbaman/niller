{
    "id": "7b53954e-1dac-4edd-a415-dd4443714b7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_menu_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 289,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cea754c-4861-480b-a8d2-061b42e2261c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53954e-1dac-4edd-a415-dd4443714b7d",
            "compositeImage": {
                "id": "73adc1c3-75b5-4614-b208-c73f4c7b5a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cea754c-4861-480b-a8d2-061b42e2261c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474398b5-97a1-43c0-a30f-b61219b71a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cea754c-4861-480b-a8d2-061b42e2261c",
                    "LayerId": "89c83247-071e-4373-8a47-ae72a87bee26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "89c83247-071e-4373-8a47-ae72a87bee26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b53954e-1dac-4edd-a415-dd4443714b7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 290,
    "xorig": 145,
    "yorig": 90
}