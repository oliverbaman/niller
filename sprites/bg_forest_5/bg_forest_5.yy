{
    "id": "f18b2f2d-0c2e-4ceb-9613-89476fed60c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_forest_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 424,
    "bbox_left": 0,
    "bbox_right": 579,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83937ce0-6614-4552-be60-148f1d5b6772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f18b2f2d-0c2e-4ceb-9613-89476fed60c5",
            "compositeImage": {
                "id": "10e3d42e-423b-48b6-bf30-c88473f84f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83937ce0-6614-4552-be60-148f1d5b6772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfab4e68-7535-4a0d-a524-47a9a984b25e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83937ce0-6614-4552-be60-148f1d5b6772",
                    "LayerId": "aca1180b-1d30-4c23-921c-48b4f7b9cec4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 425,
    "layers": [
        {
            "id": "aca1180b-1d30-4c23-921c-48b4f7b9cec4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f18b2f2d-0c2e-4ceb-9613-89476fed60c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 580,
    "xorig": 0,
    "yorig": 0
}