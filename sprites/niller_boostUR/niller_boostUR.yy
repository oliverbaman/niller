{
    "id": "395b6ac7-9a00-4a3c-aa8b-9ca5a7eb4a69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_boostUR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 6,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d619470-78e5-4ac9-a278-ade666a7d685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "395b6ac7-9a00-4a3c-aa8b-9ca5a7eb4a69",
            "compositeImage": {
                "id": "44af91fd-d7b3-4432-84d7-32863f3be042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d619470-78e5-4ac9-a278-ade666a7d685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f658d0-58d9-491f-b7b9-2308a9d032d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d619470-78e5-4ac9-a278-ade666a7d685",
                    "LayerId": "fef20374-a27b-42de-9f27-e646663c68fa"
                }
            ]
        },
        {
            "id": "791b833d-7239-46fb-a65f-32d977f4e454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "395b6ac7-9a00-4a3c-aa8b-9ca5a7eb4a69",
            "compositeImage": {
                "id": "676081f9-eb92-47b1-b085-4645800d3a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "791b833d-7239-46fb-a65f-32d977f4e454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e52b19-3d9e-498d-88ef-6feb089fccf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "791b833d-7239-46fb-a65f-32d977f4e454",
                    "LayerId": "fef20374-a27b-42de-9f27-e646663c68fa"
                }
            ]
        },
        {
            "id": "443c3e94-89cd-4e3b-9cec-a36be817a6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "395b6ac7-9a00-4a3c-aa8b-9ca5a7eb4a69",
            "compositeImage": {
                "id": "e64b095e-cfc3-4d99-aaf1-0617804ac59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "443c3e94-89cd-4e3b-9cec-a36be817a6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0246e7-6601-4392-a381-9acef97903b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "443c3e94-89cd-4e3b-9cec-a36be817a6cf",
                    "LayerId": "fef20374-a27b-42de-9f27-e646663c68fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "fef20374-a27b-42de-9f27-e646663c68fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "395b6ac7-9a00-4a3c-aa8b-9ca5a7eb4a69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 10,
    "yorig": 9
}