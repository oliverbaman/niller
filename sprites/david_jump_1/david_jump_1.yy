{
    "id": "ec1ab3f3-5bc0-4638-a060-0db8380a43c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_jump_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f9f69b6-6725-46b6-9af8-a59d2f932eb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec1ab3f3-5bc0-4638-a060-0db8380a43c7",
            "compositeImage": {
                "id": "ff960e2f-453d-46d8-a868-d6eddd0a6363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9f69b6-6725-46b6-9af8-a59d2f932eb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b27b17c-6e2e-4149-99b9-4809e76cad29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9f69b6-6725-46b6-9af8-a59d2f932eb1",
                    "LayerId": "a297c797-9059-4b48-b9d4-0608aa344451"
                }
            ]
        },
        {
            "id": "e0a75253-ee7b-4ade-a4f5-0bd7505dd50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec1ab3f3-5bc0-4638-a060-0db8380a43c7",
            "compositeImage": {
                "id": "fbc3aaf4-6693-429a-aedc-e8a57ed962e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0a75253-ee7b-4ade-a4f5-0bd7505dd50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf890b9-4b02-483c-864a-654c3752ce69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0a75253-ee7b-4ade-a4f5-0bd7505dd50f",
                    "LayerId": "a297c797-9059-4b48-b9d4-0608aa344451"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "a297c797-9059-4b48-b9d4-0608aa344451",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec1ab3f3-5bc0-4638-a060-0db8380a43c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 5,
    "yorig": 7
}