{
    "id": "7707eb34-3632-4742-9b1e-97be9fd303ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_boostU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "620fe11b-f0b9-41d2-a91f-8bc45b1fc265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7707eb34-3632-4742-9b1e-97be9fd303ff",
            "compositeImage": {
                "id": "d1031de2-3179-4b4e-bc25-b558a6f7894d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "620fe11b-f0b9-41d2-a91f-8bc45b1fc265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d173a673-d53b-4b95-99c6-5fe0aade44b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "620fe11b-f0b9-41d2-a91f-8bc45b1fc265",
                    "LayerId": "f86d675d-b015-485c-98ba-7563c118cfaf"
                }
            ]
        },
        {
            "id": "fc22f607-04e1-45df-92c1-a5651083c52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7707eb34-3632-4742-9b1e-97be9fd303ff",
            "compositeImage": {
                "id": "45edad69-55e0-445d-8888-5c7552cab7d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc22f607-04e1-45df-92c1-a5651083c52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d52692f-d799-4152-ba02-a9822bf97774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc22f607-04e1-45df-92c1-a5651083c52e",
                    "LayerId": "f86d675d-b015-485c-98ba-7563c118cfaf"
                }
            ]
        },
        {
            "id": "bad06385-9b7b-43d3-b6ff-1f60978308d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7707eb34-3632-4742-9b1e-97be9fd303ff",
            "compositeImage": {
                "id": "96ec1405-365a-4b8b-b973-1932168cd561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bad06385-9b7b-43d3-b6ff-1f60978308d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d9a456-21e2-4a6b-a917-613d259dae76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad06385-9b7b-43d3-b6ff-1f60978308d2",
                    "LayerId": "f86d675d-b015-485c-98ba-7563c118cfaf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "f86d675d-b015-485c-98ba-7563c118cfaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7707eb34-3632-4742-9b1e-97be9fd303ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 9
}