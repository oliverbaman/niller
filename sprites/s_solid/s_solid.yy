{
    "id": "57d84dda-af1d-48e7-b602-f4fcf416c88e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf08521a-9e68-4483-80c9-b5db7965278b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d84dda-af1d-48e7-b602-f4fcf416c88e",
            "compositeImage": {
                "id": "43925885-6294-41a8-8dfb-40950cb383ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf08521a-9e68-4483-80c9-b5db7965278b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ce2910-da20-4c13-9ce7-5069510ccc44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf08521a-9e68-4483-80c9-b5db7965278b",
                    "LayerId": "36c77a3a-49b5-4731-836e-4d681b6981e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "36c77a3a-49b5-4731-836e-4d681b6981e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57d84dda-af1d-48e7-b602-f4fcf416c88e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}