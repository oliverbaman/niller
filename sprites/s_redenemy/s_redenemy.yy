{
    "id": "a2eefd5e-a86e-4e2c-bffe-6ff7fabcfe87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_redenemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 72,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e235699-d41b-43b3-b114-06c1d1e4b12d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2eefd5e-a86e-4e2c-bffe-6ff7fabcfe87",
            "compositeImage": {
                "id": "cd9c789b-c15c-4019-9c42-5a7aa5c9df18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e235699-d41b-43b3-b114-06c1d1e4b12d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7a4d20-90f3-46af-a3a1-aaf5e79a92e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e235699-d41b-43b3-b114-06c1d1e4b12d",
                    "LayerId": "d2e5b440-a548-4203-babf-bd1426ed6350"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "d2e5b440-a548-4203-babf-bd1426ed6350",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2eefd5e-a86e-4e2c-bffe-6ff7fabcfe87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 0,
    "yorig": 0
}