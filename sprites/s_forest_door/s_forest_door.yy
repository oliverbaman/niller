{
    "id": "746c1eda-f634-43ed-99b4-10bb5cbc541a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_forest_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc933a58-8f56-429a-8bc6-98dcb68eb8c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746c1eda-f634-43ed-99b4-10bb5cbc541a",
            "compositeImage": {
                "id": "c97928ca-e235-4f23-ac57-26400261e2d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc933a58-8f56-429a-8bc6-98dcb68eb8c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e195c3cd-cef0-4d37-9e0c-3564f30b9f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc933a58-8f56-429a-8bc6-98dcb68eb8c3",
                    "LayerId": "7100bcf9-3d1f-4993-b1f6-0a68673d36ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "7100bcf9-3d1f-4993-b1f6-0a68673d36ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746c1eda-f634-43ed-99b4-10bb5cbc541a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}