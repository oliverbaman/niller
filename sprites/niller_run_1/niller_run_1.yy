{
    "id": "c9b05950-fe25-474e-b95c-da7fb47b7883",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_run_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae637de1-c93e-4952-a9cf-b09dd7fcb98b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "e1636678-bffc-4eef-aad9-79ff07398c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae637de1-c93e-4952-a9cf-b09dd7fcb98b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c12477-5089-443d-ab9c-647f15f95d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae637de1-c93e-4952-a9cf-b09dd7fcb98b",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "0459d82d-acf3-49d4-b2ce-e5a4dd51b989",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "b83af680-0ed6-409e-ac39-69865054ea81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0459d82d-acf3-49d4-b2ce-e5a4dd51b989",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6158d50d-20c4-42c1-b0e3-e933facdf49c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0459d82d-acf3-49d4-b2ce-e5a4dd51b989",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "5752c47f-4ddc-49a2-ba5b-125f772cc5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "b8ae49c9-e7fc-476b-8de9-5745cf87df43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5752c47f-4ddc-49a2-ba5b-125f772cc5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3948c4d3-e742-4ea4-92f9-741bfdd5d01b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5752c47f-4ddc-49a2-ba5b-125f772cc5f2",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "444ad606-3002-4058-a52c-eaee048dca90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "41f22f3f-ce1b-4966-8d9d-2e9a33b79675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444ad606-3002-4058-a52c-eaee048dca90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e7263c-94c2-4a3a-9e07-4027fba2e041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444ad606-3002-4058-a52c-eaee048dca90",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "38f9a5fd-7951-4ac2-a88a-3edf6b084016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "a11a3878-7574-4f58-bc89-800ab0082c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38f9a5fd-7951-4ac2-a88a-3edf6b084016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c20045-ff1c-4ee2-8f4b-5b9acff308c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f9a5fd-7951-4ac2-a88a-3edf6b084016",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "165669c0-3575-4203-94f8-7781290f8006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "522bb77f-b804-4066-9953-3b88750058ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "165669c0-3575-4203-94f8-7781290f8006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a1e687a-1ee0-42f7-9db0-9237fab23349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "165669c0-3575-4203-94f8-7781290f8006",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "3e8f33b3-a601-471a-82b9-e1dd225751af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "3978d87a-d696-40e4-a36d-cee7f5de2b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e8f33b3-a601-471a-82b9-e1dd225751af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a843f6-dd89-4eb9-883c-d362520f16e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e8f33b3-a601-471a-82b9-e1dd225751af",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "f9e46e01-7af5-4050-bb13-49856ab2d35d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "46873eab-8714-4bb4-9356-d0bb78f756f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e46e01-7af5-4050-bb13-49856ab2d35d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ffab456-3349-411f-911f-6fa4a1d09113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e46e01-7af5-4050-bb13-49856ab2d35d",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        },
        {
            "id": "299a148a-f8e9-4f21-951a-bc9c17fa7139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "compositeImage": {
                "id": "ed81dec5-63f3-4565-9dec-3a4444ebdd77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "299a148a-f8e9-4f21-951a-bc9c17fa7139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd3c734-ff0c-47be-b009-11e83c14f487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "299a148a-f8e9-4f21-951a-bc9c17fa7139",
                    "LayerId": "8cd9511f-7fca-4644-a0e4-977fee36a92c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8cd9511f-7fca-4644-a0e4-977fee36a92c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9b05950-fe25-474e-b95c-da7fb47b7883",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}