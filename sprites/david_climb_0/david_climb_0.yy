{
    "id": "18daf8a5-2135-4267-a6ae-65d43dca723d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_climb_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adb6c48a-b93f-46b6-9612-e55032218427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18daf8a5-2135-4267-a6ae-65d43dca723d",
            "compositeImage": {
                "id": "f838fd19-1084-416a-b907-fcf1d3b0a592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adb6c48a-b93f-46b6-9612-e55032218427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36da86f3-d4e6-4b22-b02a-1e03e26064a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb6c48a-b93f-46b6-9612-e55032218427",
                    "LayerId": "44e67998-6edf-4a34-a53f-ad19281fa22f"
                },
                {
                    "id": "34ae876d-99f0-427f-9e30-bec3468110b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb6c48a-b93f-46b6-9612-e55032218427",
                    "LayerId": "6c0db6ee-f68c-4aca-b89d-7f1af9d1cf71"
                }
            ]
        },
        {
            "id": "da9fda5b-5ab0-4f61-943d-c213ba9e0cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18daf8a5-2135-4267-a6ae-65d43dca723d",
            "compositeImage": {
                "id": "8e8be01b-5d49-4607-b955-2cf229522d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9fda5b-5ab0-4f61-943d-c213ba9e0cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3efcd28f-a837-457a-8153-c10bab862682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9fda5b-5ab0-4f61-943d-c213ba9e0cbd",
                    "LayerId": "44e67998-6edf-4a34-a53f-ad19281fa22f"
                },
                {
                    "id": "d60c78fe-182c-465d-b5d9-b6fe11962f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9fda5b-5ab0-4f61-943d-c213ba9e0cbd",
                    "LayerId": "6c0db6ee-f68c-4aca-b89d-7f1af9d1cf71"
                }
            ]
        },
        {
            "id": "b931aa73-c39e-46e0-80bf-ed8176a6ac94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18daf8a5-2135-4267-a6ae-65d43dca723d",
            "compositeImage": {
                "id": "3b2dcf61-11ef-4b38-a298-3015560611f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b931aa73-c39e-46e0-80bf-ed8176a6ac94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f141633-269c-400b-bd80-952fbfa59cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b931aa73-c39e-46e0-80bf-ed8176a6ac94",
                    "LayerId": "44e67998-6edf-4a34-a53f-ad19281fa22f"
                },
                {
                    "id": "1bfd3089-17b6-4bd8-ae99-fb4fcaaec659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b931aa73-c39e-46e0-80bf-ed8176a6ac94",
                    "LayerId": "6c0db6ee-f68c-4aca-b89d-7f1af9d1cf71"
                }
            ]
        },
        {
            "id": "12557c42-72eb-4879-8e43-11c2341b6fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18daf8a5-2135-4267-a6ae-65d43dca723d",
            "compositeImage": {
                "id": "e676cdcb-7f80-4aaa-91ca-c6ba92cb69bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12557c42-72eb-4879-8e43-11c2341b6fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c56ce2-e6a3-448c-9a84-a75059d140e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12557c42-72eb-4879-8e43-11c2341b6fc6",
                    "LayerId": "44e67998-6edf-4a34-a53f-ad19281fa22f"
                },
                {
                    "id": "9a421d03-1942-4ff2-8d03-1e5717bd9be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12557c42-72eb-4879-8e43-11c2341b6fc6",
                    "LayerId": "6c0db6ee-f68c-4aca-b89d-7f1af9d1cf71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "44e67998-6edf-4a34-a53f-ad19281fa22f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18daf8a5-2135-4267-a6ae-65d43dca723d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6c0db6ee-f68c-4aca-b89d-7f1af9d1cf71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18daf8a5-2135-4267-a6ae-65d43dca723d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}