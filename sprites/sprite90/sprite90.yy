{
    "id": "bab7aa8a-a8ab-4efc-87cf-877deda2bf8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite90",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b8f81c9-6984-4376-b4b5-d1adc1645029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bab7aa8a-a8ab-4efc-87cf-877deda2bf8f",
            "compositeImage": {
                "id": "42398987-7a40-4d5a-a495-12e3dae13bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8f81c9-6984-4376-b4b5-d1adc1645029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8a8919-5af3-461a-9956-90f6b08b1f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8f81c9-6984-4376-b4b5-d1adc1645029",
                    "LayerId": "4b425e67-dfd2-4d56-bf3f-b7ebd271f459"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4b425e67-dfd2-4d56-bf3f-b7ebd271f459",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bab7aa8a-a8ab-4efc-87cf-877deda2bf8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}