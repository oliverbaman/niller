{
    "id": "260588a0-a766-4790-9728-836aa039c889",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite53",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 16,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f94d87e7-340f-4134-a914-635a6c298f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "260588a0-a766-4790-9728-836aa039c889",
            "compositeImage": {
                "id": "23be18ee-6e1b-4e32-9a61-0303ffa8d42b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94d87e7-340f-4134-a914-635a6c298f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d44b94d4-a5aa-4025-9739-27699f1adcd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94d87e7-340f-4134-a914-635a6c298f6e",
                    "LayerId": "9e908a12-2dd4-4e17-b5d9-46bb155a92a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "9e908a12-2dd4-4e17-b5d9-46bb155a92a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "260588a0-a766-4790-9728-836aa039c889",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}