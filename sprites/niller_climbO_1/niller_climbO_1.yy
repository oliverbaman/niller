{
    "id": "2f834ce3-10cd-4c81-80bd-84f73b9626e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_climbO_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d96e93fb-05cc-4550-86a1-77e26d47ef99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f834ce3-10cd-4c81-80bd-84f73b9626e7",
            "compositeImage": {
                "id": "ae03f9a4-cc5f-457a-a57b-4949d43cee20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96e93fb-05cc-4550-86a1-77e26d47ef99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6477b791-8748-4b84-a5e8-8fd237ed81af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96e93fb-05cc-4550-86a1-77e26d47ef99",
                    "LayerId": "b2ec1a0b-6d55-40ad-8f7f-4cc5449cf13b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b2ec1a0b-6d55-40ad-8f7f-4cc5449cf13b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f834ce3-10cd-4c81-80bd-84f73b9626e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 4,
    "yorig": 5
}