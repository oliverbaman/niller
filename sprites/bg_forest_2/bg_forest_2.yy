{
    "id": "cd12109d-1c9a-4b54-b930-354caa8aa5ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_forest_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 424,
    "bbox_left": 0,
    "bbox_right": 579,
    "bbox_top": 134,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "521e14f1-f326-47c4-8a42-e0ba275a9b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd12109d-1c9a-4b54-b930-354caa8aa5ae",
            "compositeImage": {
                "id": "1c0da80f-125f-4570-88ab-fefa097cc4ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "521e14f1-f326-47c4-8a42-e0ba275a9b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a6faea-c5eb-4c2e-94f7-1f30a65af81d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "521e14f1-f326-47c4-8a42-e0ba275a9b00",
                    "LayerId": "292f54fd-7f84-42f5-882e-fa1d20482953"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 425,
    "layers": [
        {
            "id": "292f54fd-7f84-42f5-882e-fa1d20482953",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd12109d-1c9a-4b54-b930-354caa8aa5ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 580,
    "xorig": 0,
    "yorig": 0
}