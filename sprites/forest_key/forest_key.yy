{
    "id": "296e1fb2-5dc9-4fd2-bec0-283edccf174c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "forest_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6949b6d-0533-4b46-9139-e33617263672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "296e1fb2-5dc9-4fd2-bec0-283edccf174c",
            "compositeImage": {
                "id": "1e6440db-1747-41a2-915c-2a347075d934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6949b6d-0533-4b46-9139-e33617263672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "234c96bc-9fc2-47be-a2bb-b38d017e41bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6949b6d-0533-4b46-9139-e33617263672",
                    "LayerId": "b7d7986b-8839-4f57-849f-02544ed06448"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "b7d7986b-8839-4f57-849f-02544ed06448",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "296e1fb2-5dc9-4fd2-bec0-283edccf174c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}