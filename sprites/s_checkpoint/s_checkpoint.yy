{
    "id": "ea7b6f4c-48e7-4b61-9610-7978456309de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_checkpoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e7fadcc-0345-4449-bb14-2f0d2128e22c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea7b6f4c-48e7-4b61-9610-7978456309de",
            "compositeImage": {
                "id": "9be6461d-75c0-491d-9a62-793ba1c48b85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e7fadcc-0345-4449-bb14-2f0d2128e22c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ea74cbc-2c30-4ff5-86c8-80a61a1ed536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e7fadcc-0345-4449-bb14-2f0d2128e22c",
                    "LayerId": "708ef69f-98d5-4b91-bd9d-0ab66643bf53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "708ef69f-98d5-4b91-bd9d-0ab66643bf53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea7b6f4c-48e7-4b61-9610-7978456309de",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}