{
    "id": "1a5ddd64-83fc-4cfe-a77d-d19ccf3db0ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_jump_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a0d945d-71cb-4106-aff4-242f6430f5d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a5ddd64-83fc-4cfe-a77d-d19ccf3db0ca",
            "compositeImage": {
                "id": "02f9e630-4822-48a5-bdd1-0453bea9c67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a0d945d-71cb-4106-aff4-242f6430f5d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20be9062-9275-4ebe-86a4-377304dcb168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a0d945d-71cb-4106-aff4-242f6430f5d3",
                    "LayerId": "a6f4fdb2-9a9c-430d-aee2-308b083c1847"
                }
            ]
        },
        {
            "id": "e9c0a2ce-f789-4a2f-9638-1ba6432e6330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a5ddd64-83fc-4cfe-a77d-d19ccf3db0ca",
            "compositeImage": {
                "id": "2dbdd3db-d85f-4343-a638-28952c0d7780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c0a2ce-f789-4a2f-9638-1ba6432e6330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7994579c-3241-48fc-afdc-f02e61e7b8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c0a2ce-f789-4a2f-9638-1ba6432e6330",
                    "LayerId": "a6f4fdb2-9a9c-430d-aee2-308b083c1847"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "a6f4fdb2-9a9c-430d-aee2-308b083c1847",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a5ddd64-83fc-4cfe-a77d-d19ccf3db0ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 5,
    "yorig": 7
}