{
    "id": "4f3fb407-14e1-4547-bcfe-7bd3e91e572a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite68",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e039eaa0-0f58-430f-bdc0-438c4fef8978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f3fb407-14e1-4547-bcfe-7bd3e91e572a",
            "compositeImage": {
                "id": "4eba4e52-50fb-4f60-a4cc-f9136867cefc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e039eaa0-0f58-430f-bdc0-438c4fef8978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de7c44b4-d5e0-4941-8176-c588645b2773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e039eaa0-0f58-430f-bdc0-438c4fef8978",
                    "LayerId": "7f7a89fe-9f63-4e67-90f5-41181d4228f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7f7a89fe-9f63-4e67-90f5-41181d4228f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f3fb407-14e1-4547-bcfe-7bd3e91e572a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}