{
    "id": "853748aa-e653-43ef-a7bc-f37ea6c7cf23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_moving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "591fe781-a99d-4ac8-9ea2-3dbc11ee11ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853748aa-e653-43ef-a7bc-f37ea6c7cf23",
            "compositeImage": {
                "id": "71ce7fa7-5e34-4f2c-9766-f4b25aab708c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591fe781-a99d-4ac8-9ea2-3dbc11ee11ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14ff8ce0-ea36-4ac5-950f-3fe01eb5876f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591fe781-a99d-4ac8-9ea2-3dbc11ee11ba",
                    "LayerId": "856f4129-d65a-48ce-9b15-7a1e21672f82"
                }
            ]
        },
        {
            "id": "53233ec7-0a6c-45b8-b8a5-e0999aa2d881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853748aa-e653-43ef-a7bc-f37ea6c7cf23",
            "compositeImage": {
                "id": "23ab671e-d836-4dd4-ab71-9ade94c91e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53233ec7-0a6c-45b8-b8a5-e0999aa2d881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d929884-74e1-4a1b-8db1-8fdbd2694fd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53233ec7-0a6c-45b8-b8a5-e0999aa2d881",
                    "LayerId": "856f4129-d65a-48ce-9b15-7a1e21672f82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "856f4129-d65a-48ce-9b15-7a1e21672f82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "853748aa-e653-43ef-a7bc-f37ea6c7cf23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}