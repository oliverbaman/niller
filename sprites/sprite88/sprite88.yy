{
    "id": "3b1aecf9-702b-46d7-8ce9-e0cf956d8d65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite88",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 7,
    "bbox_right": 58,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "273c1e40-3805-4c40-8f0f-11666ec55164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b1aecf9-702b-46d7-8ce9-e0cf956d8d65",
            "compositeImage": {
                "id": "8891eff4-dbae-4ae0-8aa3-c53c6f4f6f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273c1e40-3805-4c40-8f0f-11666ec55164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f683b72-6abd-47af-8434-01455e1bda3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273c1e40-3805-4c40-8f0f-11666ec55164",
                    "LayerId": "d9cd687d-9bdb-437c-8fae-b3dbc7f10d24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d9cd687d-9bdb-437c-8fae-b3dbc7f10d24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b1aecf9-702b-46d7-8ce9-e0cf956d8d65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}