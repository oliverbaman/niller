{
    "id": "c71ea742-73f2-41f4-8607-26e5cf5ade68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ghost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 5,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3809635-84f6-462d-960f-31187b4e4b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71ea742-73f2-41f4-8607-26e5cf5ade68",
            "compositeImage": {
                "id": "df40d494-c4eb-420f-b49a-40094bc386c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3809635-84f6-462d-960f-31187b4e4b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a91124-1c0c-4237-941f-03d7ca32959e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3809635-84f6-462d-960f-31187b4e4b6d",
                    "LayerId": "fc34578f-d234-4c27-be91-549080ce00cf"
                }
            ]
        },
        {
            "id": "362ae402-ae5b-4974-9680-cc77a7c80208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71ea742-73f2-41f4-8607-26e5cf5ade68",
            "compositeImage": {
                "id": "0842f6fa-a48d-4d11-a1f1-d2dd0e1910eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362ae402-ae5b-4974-9680-cc77a7c80208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b7a103-f714-43ad-8e02-7da3fd9f74b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362ae402-ae5b-4974-9680-cc77a7c80208",
                    "LayerId": "fc34578f-d234-4c27-be91-549080ce00cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "fc34578f-d234-4c27-be91-549080ce00cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c71ea742-73f2-41f4-8607-26e5cf5ade68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 10,
    "yorig": 10
}