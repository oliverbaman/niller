{
    "id": "d8120e95-8e1b-4087-9887-c448b97c5c18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite69",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40aa8c34-a1a1-471a-94f1-8e43bf5c6775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8120e95-8e1b-4087-9887-c448b97c5c18",
            "compositeImage": {
                "id": "ae3a16e9-9f4d-4d1b-a3b5-31a09d136227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40aa8c34-a1a1-471a-94f1-8e43bf5c6775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a793e697-8baa-46ae-aceb-f9327f5890d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40aa8c34-a1a1-471a-94f1-8e43bf5c6775",
                    "LayerId": "7e20e2c2-4255-4af2-9361-9fd9b668f20b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7e20e2c2-4255-4af2-9361-9fd9b668f20b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8120e95-8e1b-4087-9887-c448b97c5c18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}