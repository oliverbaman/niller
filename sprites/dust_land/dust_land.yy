{
    "id": "c6dda367-3c27-4542-a1a8-f8403401fd98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dust_land",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "849bdcce-f594-4418-803a-ce0316165728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6dda367-3c27-4542-a1a8-f8403401fd98",
            "compositeImage": {
                "id": "e72fea51-6623-47a6-81bc-afb25c92224a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "849bdcce-f594-4418-803a-ce0316165728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef84d5cf-20f8-489a-b838-83b9689639e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "849bdcce-f594-4418-803a-ce0316165728",
                    "LayerId": "e713a52a-d879-4b89-b42c-8d4a260fede8"
                }
            ]
        },
        {
            "id": "6492a9ba-668a-4bab-82a0-26567a2dbb30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6dda367-3c27-4542-a1a8-f8403401fd98",
            "compositeImage": {
                "id": "381bc0b8-606e-4dfd-931c-230f5bea6df7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6492a9ba-668a-4bab-82a0-26567a2dbb30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d0f353-519a-4236-b97c-4dc643c66ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6492a9ba-668a-4bab-82a0-26567a2dbb30",
                    "LayerId": "e713a52a-d879-4b89-b42c-8d4a260fede8"
                }
            ]
        },
        {
            "id": "ab806853-f5d2-4ae4-8239-48d9141daeb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6dda367-3c27-4542-a1a8-f8403401fd98",
            "compositeImage": {
                "id": "26ed2862-99e3-4c9a-9f01-4dae03995775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab806853-f5d2-4ae4-8239-48d9141daeb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59a972d3-2ffa-4c3c-be53-b016034872a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab806853-f5d2-4ae4-8239-48d9141daeb3",
                    "LayerId": "e713a52a-d879-4b89-b42c-8d4a260fede8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "e713a52a-d879-4b89-b42c-8d4a260fede8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6dda367-3c27-4542-a1a8-f8403401fd98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 5,
    "yorig": 8
}