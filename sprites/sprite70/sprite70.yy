{
    "id": "45649bcc-b962-4d7a-9a59-970d99de0c2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite70",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "807b257c-3877-4fff-9851-5c9f1a9aa3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45649bcc-b962-4d7a-9a59-970d99de0c2f",
            "compositeImage": {
                "id": "98c815cd-d342-4b12-8546-99148578d3b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "807b257c-3877-4fff-9851-5c9f1a9aa3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e32fe1ce-3555-4939-9a48-8e97b9c0acdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "807b257c-3877-4fff-9851-5c9f1a9aa3cf",
                    "LayerId": "d9ab76b1-0a76-4dd6-aa91-1ce8a1c957b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d9ab76b1-0a76-4dd6-aa91-1ce8a1c957b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45649bcc-b962-4d7a-9a59-970d99de0c2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}