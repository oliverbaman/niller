{
    "id": "89048d67-cd07-4c6d-b873-28de9215ea61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_city_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 205,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59f96ef5-7eb3-4d02-a9d6-1d04c4755041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89048d67-cd07-4c6d-b873-28de9215ea61",
            "compositeImage": {
                "id": "bb7ead83-608a-4516-b1a2-016cc3eb2f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f96ef5-7eb3-4d02-a9d6-1d04c4755041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2963378e-991c-4c15-98ed-408e20f5b646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f96ef5-7eb3-4d02-a9d6-1d04c4755041",
                    "LayerId": "772e2c98-6cc3-47db-b3fc-3fdf52fe764e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "772e2c98-6cc3-47db-b3fc-3fdf52fe764e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89048d67-cd07-4c6d-b873-28de9215ea61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}