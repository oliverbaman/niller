{
    "id": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_run_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fb61653-2553-4bae-869e-fd958764b6c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "c8ec86a9-c723-4a8e-8250-5d661dcd5951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fb61653-2553-4bae-869e-fd958764b6c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88207e98-4889-4d96-bfaf-c485b1024b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fb61653-2553-4bae-869e-fd958764b6c1",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "f6d0f5eb-392d-48d3-8627-d8a3e1dbdf93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fb61653-2553-4bae-869e-fd958764b6c1",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "f4d79a89-ba14-4e90-b298-13e9e2738367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "80dabd0b-cd72-4881-ab4c-1a26bae3fbaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4d79a89-ba14-4e90-b298-13e9e2738367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3db918c8-4f7d-49f5-8361-e7c40e873eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d79a89-ba14-4e90-b298-13e9e2738367",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "e7f77311-69d7-4746-8e79-52b3fd15fb9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d79a89-ba14-4e90-b298-13e9e2738367",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "62a49c45-c140-4e00-b451-f4445acfcb75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "9d9e6a21-e826-48c5-979e-239a30efd784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a49c45-c140-4e00-b451-f4445acfcb75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1cbc687-c562-4ae3-b745-cdc0ece71cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a49c45-c140-4e00-b451-f4445acfcb75",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "845107fd-93f7-437e-8ba9-ddb3ef3c5940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a49c45-c140-4e00-b451-f4445acfcb75",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "658ccbd2-e423-4daf-80cc-bc778d4d890c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "04d2a08c-5107-4cc8-a42a-603b1986b3b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658ccbd2-e423-4daf-80cc-bc778d4d890c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1be136bf-dfc7-477c-bf73-7258410015b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658ccbd2-e423-4daf-80cc-bc778d4d890c",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "b4e7f125-29da-4768-b83b-86b1b5a5ff6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658ccbd2-e423-4daf-80cc-bc778d4d890c",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "6a6571c1-c45b-4e9d-9a6a-c027764375fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "f96fe13e-589a-421b-ba5b-1f856e1191b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a6571c1-c45b-4e9d-9a6a-c027764375fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6acce196-e1ba-41ab-9dcc-f40cf10ba790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a6571c1-c45b-4e9d-9a6a-c027764375fd",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "746070d3-2ebc-49bf-b1bc-cc15a9b01465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a6571c1-c45b-4e9d-9a6a-c027764375fd",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "fb1775fb-9414-4035-a5a3-744592f88726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "d94c26b4-8bdb-4d25-9344-df2247af9c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb1775fb-9414-4035-a5a3-744592f88726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09511a17-022e-4e8c-9cad-24091323dffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb1775fb-9414-4035-a5a3-744592f88726",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "9844f003-112b-48f8-a86d-a024268e5b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb1775fb-9414-4035-a5a3-744592f88726",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "72772c96-f138-4bba-8cee-8999a0184737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "2e5a263f-d3fd-463d-9a8c-5628fb35ebb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72772c96-f138-4bba-8cee-8999a0184737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8ede78-4a5b-4699-b20b-852abe81554e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72772c96-f138-4bba-8cee-8999a0184737",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "68d60186-c2a1-4fc6-bf19-a5eb28cc9bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72772c96-f138-4bba-8cee-8999a0184737",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "9b731d10-2b1e-41cb-a780-c27556349322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "1b9618b3-2aba-4f50-b590-63d33da1b3c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b731d10-2b1e-41cb-a780-c27556349322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc68204-520d-405b-8acc-ff09fa66b233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b731d10-2b1e-41cb-a780-c27556349322",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "e1cf5e8c-da92-46b2-9ca7-f1616a607dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b731d10-2b1e-41cb-a780-c27556349322",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        },
        {
            "id": "5adbfe2e-9483-4d41-9fd6-7651de08f645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "compositeImage": {
                "id": "9846eac8-fb7b-4226-a13c-1ad1e4f80cee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5adbfe2e-9483-4d41-9fd6-7651de08f645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2739036b-60f4-4403-a793-117a4ffe9b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5adbfe2e-9483-4d41-9fd6-7651de08f645",
                    "LayerId": "f95d2113-6787-4d3b-9932-ffa368585341"
                },
                {
                    "id": "320c6f4f-e341-42d9-9081-8d012fd0eb37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5adbfe2e-9483-4d41-9fd6-7651de08f645",
                    "LayerId": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f95d2113-6787-4d3b-9932-ffa368585341",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5a04b51f-7abb-4852-a7aa-48f8c45b78d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab4e4b87-24d2-4fa4-9fb0-e65d5a290713",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4280824586
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}