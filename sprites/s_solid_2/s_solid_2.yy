{
    "id": "c47f297c-ccdd-4221-9e4c-adeb86dc531b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_solid_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85248dc1-3e51-4f54-a0c4-890ef4eb0827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c47f297c-ccdd-4221-9e4c-adeb86dc531b",
            "compositeImage": {
                "id": "435ce175-103f-402d-9e61-1aede5859bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85248dc1-3e51-4f54-a0c4-890ef4eb0827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2261d68-66ea-46ed-8345-db01da5b9734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85248dc1-3e51-4f54-a0c4-890ef4eb0827",
                    "LayerId": "cf004dcc-b603-4f06-8cb0-7559aed96310"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "cf004dcc-b603-4f06-8cb0-7559aed96310",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c47f297c-ccdd-4221-9e4c-adeb86dc531b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}