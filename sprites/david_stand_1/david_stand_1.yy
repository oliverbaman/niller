{
    "id": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_stand_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77df1912-f302-49fb-a7f4-e191ae71758b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "a9b9d106-0887-4f97-b74c-81c14ded2e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77df1912-f302-49fb-a7f4-e191ae71758b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abfec334-5cb2-476c-8494-e99382716bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77df1912-f302-49fb-a7f4-e191ae71758b",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "f6252e2d-f806-49f0-aa7f-c14fa74a5b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "4de97b70-6e02-446d-9156-ca6a02c3bf5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6252e2d-f806-49f0-aa7f-c14fa74a5b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3365a99b-5d4f-4b19-8c43-adf069e351d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6252e2d-f806-49f0-aa7f-c14fa74a5b0d",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "aa223805-0a50-44bd-a003-1d736906291e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "5f95fc98-1074-415c-837b-e559029a8ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa223805-0a50-44bd-a003-1d736906291e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b0f084-6b9a-4ae4-976c-24e442e17ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa223805-0a50-44bd-a003-1d736906291e",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "ccf63548-de1f-456f-a511-54cb519d23de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "0ba74c7d-13bd-4a54-99ef-54bb10c1987f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf63548-de1f-456f-a511-54cb519d23de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb90e8dc-1c6e-449d-a7ea-d406adced8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf63548-de1f-456f-a511-54cb519d23de",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "58e11b95-a85c-4fa5-b34e-53191ab3f6b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "5f3064fe-b7c1-4336-8e5e-794abc64a0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e11b95-a85c-4fa5-b34e-53191ab3f6b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9bfff98-2c47-49ee-9662-fcf91c087436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e11b95-a85c-4fa5-b34e-53191ab3f6b1",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "03acab17-d226-47a3-bb68-48168c1df93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "d4fb77a7-fea6-49b3-8fb2-8457391483de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03acab17-d226-47a3-bb68-48168c1df93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff93258-5a2e-4210-8e00-7ae2f6b11bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03acab17-d226-47a3-bb68-48168c1df93c",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "3db647c9-5392-4ae9-98da-29fe598fb159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "bfa55a73-7c74-4fef-9db1-041f95c04c9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db647c9-5392-4ae9-98da-29fe598fb159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1df4e0-24e0-4cca-b58f-974f2b99fbb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db647c9-5392-4ae9-98da-29fe598fb159",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "ffb4f0ca-ccf0-4984-8aa1-04f2d41e6a56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "bff1a457-939a-46ba-bec0-66c0112fff17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb4f0ca-ccf0-4984-8aa1-04f2d41e6a56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c946ba-efbb-4943-b18e-99fe78f84c98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb4f0ca-ccf0-4984-8aa1-04f2d41e6a56",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "ee3d6baf-0e42-4793-a116-644cd0f05ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "f42a2826-be44-4a49-b260-dec99bef5cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3d6baf-0e42-4793-a116-644cd0f05ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abbb330e-9e50-4e52-b8fb-f06ff2b61282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3d6baf-0e42-4793-a116-644cd0f05ca8",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "df101137-f675-490e-b098-c930e648b338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "bda0e4cb-003a-4937-b3d3-37ae964c8f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df101137-f675-490e-b098-c930e648b338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a755973e-dee4-4ca4-a24d-531a78a1908c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df101137-f675-490e-b098-c930e648b338",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "7beb2e73-e95c-470e-9798-a1a1a87134c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "3d19685e-7c81-4cb0-81a0-d3055d8eee1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7beb2e73-e95c-470e-9798-a1a1a87134c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493fc00b-731d-49bd-b640-1e20c61f1b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7beb2e73-e95c-470e-9798-a1a1a87134c9",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        },
        {
            "id": "f7efc34b-672c-4cc2-a0c6-866835def012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "compositeImage": {
                "id": "1941274f-f1fd-423d-b3c8-3fc6ccf3f6c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7efc34b-672c-4cc2-a0c6-866835def012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc136357-17e0-4c86-aeef-4165af71aa65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7efc34b-672c-4cc2-a0c6-866835def012",
                    "LayerId": "5313ba56-6b61-4c11-8750-edd03116ba92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5313ba56-6b61-4c11-8750-edd03116ba92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "397078dc-b8cf-4814-a8d8-63067fe5deb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4278786099,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}