{
    "id": "499a2e40-c4a6-4eb6-8572-37c8193b1fd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_redenemy_moving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a33c61ae-6434-4920-bcb8-83f3b56bda0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "499a2e40-c4a6-4eb6-8572-37c8193b1fd1",
            "compositeImage": {
                "id": "56225e77-91b5-4581-87dc-58e5acfb2e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33c61ae-6434-4920-bcb8-83f3b56bda0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9ac5e1-bac6-4185-9bf3-2cec02764d16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33c61ae-6434-4920-bcb8-83f3b56bda0c",
                    "LayerId": "f1b054be-954b-4eb2-82f9-26f96640d563"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "f1b054be-954b-4eb2-82f9-26f96640d563",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "499a2e40-c4a6-4eb6-8572-37c8193b1fd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}