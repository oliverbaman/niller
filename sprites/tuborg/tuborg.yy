{
    "id": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tuborg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 13,
    "bbox_right": 26,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aace1136-8f3d-4338-8c67-2c8ee813b6dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
            "compositeImage": {
                "id": "f555b5fa-6caa-4937-87e3-09fee7a416b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aace1136-8f3d-4338-8c67-2c8ee813b6dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abd8615b-f442-4479-b3fc-7a041f4adf31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aace1136-8f3d-4338-8c67-2c8ee813b6dc",
                    "LayerId": "01621461-b4cb-448b-b2cc-e1899cd054e6"
                }
            ]
        },
        {
            "id": "60b98446-b4bf-4e96-bf04-fd0ebf48feef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
            "compositeImage": {
                "id": "41b07f2f-411a-43aa-aa28-d577d2551540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b98446-b4bf-4e96-bf04-fd0ebf48feef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58dbf97e-38cc-4a02-bfef-a87ce7200bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b98446-b4bf-4e96-bf04-fd0ebf48feef",
                    "LayerId": "01621461-b4cb-448b-b2cc-e1899cd054e6"
                }
            ]
        },
        {
            "id": "720bb15a-dc3b-4ed4-8665-251cb30b2f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
            "compositeImage": {
                "id": "07ecdddc-e79b-4e74-aeca-31a6f95c0322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720bb15a-dc3b-4ed4-8665-251cb30b2f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1628719f-44ab-4ba8-9d56-05c4f7ac0a0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720bb15a-dc3b-4ed4-8665-251cb30b2f84",
                    "LayerId": "01621461-b4cb-448b-b2cc-e1899cd054e6"
                }
            ]
        },
        {
            "id": "b100b0e6-d30f-4fa7-9c81-18dfdf4fd101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
            "compositeImage": {
                "id": "c6c86444-f72c-4172-87cb-27cd0d6ed80c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b100b0e6-d30f-4fa7-9c81-18dfdf4fd101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7279e44e-e8a1-4204-9977-05f087fdd7eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b100b0e6-d30f-4fa7-9c81-18dfdf4fd101",
                    "LayerId": "01621461-b4cb-448b-b2cc-e1899cd054e6"
                }
            ]
        },
        {
            "id": "4dea9525-3bf9-403e-a953-1f8c8173104b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
            "compositeImage": {
                "id": "ab078042-5e0a-4211-8e17-575b88d23109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dea9525-3bf9-403e-a953-1f8c8173104b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ce9274-6b24-4db0-ac0f-742180aca911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dea9525-3bf9-403e-a953-1f8c8173104b",
                    "LayerId": "01621461-b4cb-448b-b2cc-e1899cd054e6"
                }
            ]
        },
        {
            "id": "ba6585a2-1f26-45f9-89bd-0e96c33a9dab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
            "compositeImage": {
                "id": "29d63222-661c-4df9-a97a-1eab60b48aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba6585a2-1f26-45f9-89bd-0e96c33a9dab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "affd4dbb-704f-4cc2-afbd-7ba64a4d3284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba6585a2-1f26-45f9-89bd-0e96c33a9dab",
                    "LayerId": "01621461-b4cb-448b-b2cc-e1899cd054e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "01621461-b4cb-448b-b2cc-e1899cd054e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 10,
    "yorig": 16
}