{
    "id": "8c29524e-5cb3-4f6b-90e9-ba1dc9524fee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_climb_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5d06171-a462-4d37-9ba7-87b6a24be9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c29524e-5cb3-4f6b-90e9-ba1dc9524fee",
            "compositeImage": {
                "id": "44d9a10d-b3f7-4e04-b429-eb88a6819251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5d06171-a462-4d37-9ba7-87b6a24be9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed40aea5-74e9-422b-8565-ba815cf849f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5d06171-a462-4d37-9ba7-87b6a24be9dd",
                    "LayerId": "34f49a55-5776-4bff-b365-40e652b226e4"
                },
                {
                    "id": "811e0fb3-3734-4779-b412-3aa8164f128f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5d06171-a462-4d37-9ba7-87b6a24be9dd",
                    "LayerId": "76e30822-1891-4670-a574-419e22c46aee"
                }
            ]
        },
        {
            "id": "e7b62ac0-64d8-47cf-994a-730248c9ce98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c29524e-5cb3-4f6b-90e9-ba1dc9524fee",
            "compositeImage": {
                "id": "4821d168-b931-4ba8-adc2-1b09e48dafd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b62ac0-64d8-47cf-994a-730248c9ce98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24924841-7880-42c4-b09e-6bb055db0678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b62ac0-64d8-47cf-994a-730248c9ce98",
                    "LayerId": "34f49a55-5776-4bff-b365-40e652b226e4"
                },
                {
                    "id": "92fbcfe5-91ac-4d49-84e2-c55154cdbe8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b62ac0-64d8-47cf-994a-730248c9ce98",
                    "LayerId": "76e30822-1891-4670-a574-419e22c46aee"
                }
            ]
        },
        {
            "id": "18b9cdd6-d9e4-4a7e-a387-113c477b53db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c29524e-5cb3-4f6b-90e9-ba1dc9524fee",
            "compositeImage": {
                "id": "801f95fd-e4de-415d-9f5c-9365ea239a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18b9cdd6-d9e4-4a7e-a387-113c477b53db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2112e09b-1d78-4a49-95c5-84da371a35f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18b9cdd6-d9e4-4a7e-a387-113c477b53db",
                    "LayerId": "34f49a55-5776-4bff-b365-40e652b226e4"
                },
                {
                    "id": "7dfaaafb-a416-4cd6-bb36-2894b45ddf40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18b9cdd6-d9e4-4a7e-a387-113c477b53db",
                    "LayerId": "76e30822-1891-4670-a574-419e22c46aee"
                }
            ]
        },
        {
            "id": "a6726ac5-b197-4444-91f2-8236dea227a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c29524e-5cb3-4f6b-90e9-ba1dc9524fee",
            "compositeImage": {
                "id": "f823e834-5b97-4927-b410-13fed5f9d949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6726ac5-b197-4444-91f2-8236dea227a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28492b1b-e6d3-4ffa-8b7b-299d07602b95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6726ac5-b197-4444-91f2-8236dea227a3",
                    "LayerId": "34f49a55-5776-4bff-b365-40e652b226e4"
                },
                {
                    "id": "f546f843-815b-454e-b82a-2a2af15aa786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6726ac5-b197-4444-91f2-8236dea227a3",
                    "LayerId": "76e30822-1891-4670-a574-419e22c46aee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "76e30822-1891-4670-a574-419e22c46aee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c29524e-5cb3-4f6b-90e9-ba1dc9524fee",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "34f49a55-5776-4bff-b365-40e652b226e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c29524e-5cb3-4f6b-90e9-ba1dc9524fee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}