{
    "id": "44fcdd4c-1638-496d-b34e-d03e21131d37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_city_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 290,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17c4275f-d01d-47f7-8560-a89e6ad093ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44fcdd4c-1638-496d-b34e-d03e21131d37",
            "compositeImage": {
                "id": "96acfb4a-aa42-4ecc-bcb1-13d6963a2ef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17c4275f-d01d-47f7-8560-a89e6ad093ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3727053-d35d-4539-8fb1-211909cca3a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17c4275f-d01d-47f7-8560-a89e6ad093ee",
                    "LayerId": "59129f97-7c5e-49c5-a634-45eb61d868f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "59129f97-7c5e-49c5-a634-45eb61d868f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44fcdd4c-1638-496d-b34e-d03e21131d37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}