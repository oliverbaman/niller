{
    "id": "9f333332-8dea-4168-a27b-f6e43039f9c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_jump_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73987919-ca68-45ea-a891-4b76e625700c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f333332-8dea-4168-a27b-f6e43039f9c3",
            "compositeImage": {
                "id": "aba8cf45-3f38-4318-be4f-7c63680bc465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73987919-ca68-45ea-a891-4b76e625700c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1986cbe4-fb1b-42b3-b34b-fd5eaea79ac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73987919-ca68-45ea-a891-4b76e625700c",
                    "LayerId": "120cf904-20d7-4962-8d01-01904ee623a2"
                }
            ]
        },
        {
            "id": "10c8f660-ae27-46bd-8b5e-1268138c1b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f333332-8dea-4168-a27b-f6e43039f9c3",
            "compositeImage": {
                "id": "0e363edd-1a90-4b05-bc96-68182cbbf9b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10c8f660-ae27-46bd-8b5e-1268138c1b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc1694d-1180-4f8d-9726-7df6a6b3c7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10c8f660-ae27-46bd-8b5e-1268138c1b1e",
                    "LayerId": "120cf904-20d7-4962-8d01-01904ee623a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "120cf904-20d7-4962-8d01-01904ee623a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f333332-8dea-4168-a27b-f6e43039f9c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 5,
    "yorig": 8
}