{
    "id": "c9b14349-79d5-46ab-8b76-3ff37dfe9134",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite851",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2891001c-1b79-4ba2-a5c3-6bf7f06c28c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9b14349-79d5-46ab-8b76-3ff37dfe9134",
            "compositeImage": {
                "id": "6f5a7c24-0ee2-4ef1-b421-924663123136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2891001c-1b79-4ba2-a5c3-6bf7f06c28c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83290b94-0c5d-484d-973e-d2cb0896498c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2891001c-1b79-4ba2-a5c3-6bf7f06c28c2",
                    "LayerId": "56e8dfc0-d161-4674-a58e-c4bb9683cfc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "56e8dfc0-d161-4674-a58e-c4bb9683cfc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9b14349-79d5-46ab-8b76-3ff37dfe9134",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}