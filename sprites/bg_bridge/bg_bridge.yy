{
    "id": "c2f306f5-aad9-4f97-aebe-9de139b7f282",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_bridge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 602,
    "bbox_left": 0,
    "bbox_right": 822,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03da87f1-fca7-4270-af07-e5fa3e841347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2f306f5-aad9-4f97-aebe-9de139b7f282",
            "compositeImage": {
                "id": "813c9a5b-719e-4da3-9e87-a80d8dfc0366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03da87f1-fca7-4270-af07-e5fa3e841347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92db8804-0823-4db1-b513-f4b30ec7458f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03da87f1-fca7-4270-af07-e5fa3e841347",
                    "LayerId": "6c209f78-d0f9-4c4a-b36e-2fa559cb02d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 603,
    "layers": [
        {
            "id": "6c209f78-d0f9-4c4a-b36e-2fa559cb02d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2f306f5-aad9-4f97-aebe-9de139b7f282",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 823,
    "xorig": 0,
    "yorig": 0
}