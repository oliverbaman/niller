{
    "id": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite89",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 13,
    "bbox_right": 90,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b2df922-db28-40ed-a97a-71f23f70c643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
            "compositeImage": {
                "id": "1be3a0f7-2314-4cbe-bd42-bbdff7301fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2df922-db28-40ed-a97a-71f23f70c643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f8308e-4a4c-408e-a74b-0918ed2850b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2df922-db28-40ed-a97a-71f23f70c643",
                    "LayerId": "60676d4e-0b32-4b9d-a99a-1c7abd539fed"
                }
            ]
        },
        {
            "id": "0c218436-3ca0-4dee-ab52-f93153057d45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
            "compositeImage": {
                "id": "28859f43-6a63-44bb-8037-3358471eebda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c218436-3ca0-4dee-ab52-f93153057d45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70dc39f3-f6d1-419d-9a68-427c1a08607c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c218436-3ca0-4dee-ab52-f93153057d45",
                    "LayerId": "60676d4e-0b32-4b9d-a99a-1c7abd539fed"
                }
            ]
        },
        {
            "id": "b1827422-d5bc-4e1f-88a2-8f9d848be9fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
            "compositeImage": {
                "id": "e7c3b47a-c5c4-4561-a14e-f7713eee28c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1827422-d5bc-4e1f-88a2-8f9d848be9fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6b89d97-7533-4525-9467-64191f3bbee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1827422-d5bc-4e1f-88a2-8f9d848be9fc",
                    "LayerId": "60676d4e-0b32-4b9d-a99a-1c7abd539fed"
                }
            ]
        },
        {
            "id": "21f3a198-09d8-4bc5-96df-e2cc095429b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
            "compositeImage": {
                "id": "624b9ef3-10c8-47f5-a6f3-d934f63ef602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f3a198-09d8-4bc5-96df-e2cc095429b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca0616f-9650-4fa1-a790-a159b738d8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f3a198-09d8-4bc5-96df-e2cc095429b9",
                    "LayerId": "60676d4e-0b32-4b9d-a99a-1c7abd539fed"
                }
            ]
        },
        {
            "id": "9a511ef1-62c0-41f7-8239-286a5f002925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
            "compositeImage": {
                "id": "037aebc0-efa9-4385-af9f-447e52b84dda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a511ef1-62c0-41f7-8239-286a5f002925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ff1278a-077d-4ae7-a492-1642b4ad0802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a511ef1-62c0-41f7-8239-286a5f002925",
                    "LayerId": "60676d4e-0b32-4b9d-a99a-1c7abd539fed"
                }
            ]
        },
        {
            "id": "49775408-5254-4a18-9b48-07606ea1fc6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
            "compositeImage": {
                "id": "05212305-acc0-4ca4-9b82-d4e15dd52cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49775408-5254-4a18-9b48-07606ea1fc6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2178e061-b3bd-44b3-a335-1817329b45b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49775408-5254-4a18-9b48-07606ea1fc6a",
                    "LayerId": "60676d4e-0b32-4b9d-a99a-1c7abd539fed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "60676d4e-0b32-4b9d-a99a-1c7abd539fed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4289893703,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}