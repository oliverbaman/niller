{
    "id": "4f357bae-726e-4b48-b49f-6aab2c4f6103",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_forest_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 424,
    "bbox_left": 0,
    "bbox_right": 579,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63781f3a-e39c-4b2e-95be-79c3428d9595",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f357bae-726e-4b48-b49f-6aab2c4f6103",
            "compositeImage": {
                "id": "6951ef58-d7e5-4c3c-a95a-76268bd25463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63781f3a-e39c-4b2e-95be-79c3428d9595",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10aa7bc3-21e4-45ca-ad50-31430cf4e13f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63781f3a-e39c-4b2e-95be-79c3428d9595",
                    "LayerId": "15f76186-0eaf-4fa8-b027-b63d81428b9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 425,
    "layers": [
        {
            "id": "15f76186-0eaf-4fa8-b027-b63d81428b9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f357bae-726e-4b48-b49f-6aab2c4f6103",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 580,
    "xorig": 0,
    "yorig": 0
}