{
    "id": "0c3256d0-192b-40bc-92cf-8dc86d867603",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_BG_Objects_city",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 8,
    "bbox_right": 112,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfd22023-fee1-4448-ac2e-4f16a07be7e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c3256d0-192b-40bc-92cf-8dc86d867603",
            "compositeImage": {
                "id": "2aa737ff-2261-4f33-9433-bd1f276b9acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfd22023-fee1-4448-ac2e-4f16a07be7e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21aba868-e2fe-4dfa-957c-915d16045419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfd22023-fee1-4448-ac2e-4f16a07be7e0",
                    "LayerId": "63bfab97-bc46-4c6d-b9b6-c1f6fdfb846c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "63bfab97-bc46-4c6d-b9b6-c1f6fdfb846c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c3256d0-192b-40bc-92cf-8dc86d867603",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 173,
    "xorig": 0,
    "yorig": 0
}