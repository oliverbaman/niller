{
    "id": "3f4024d5-285f-43e6-9264-27da408ec01e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_checkpoint_cover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8222516-ea0a-456f-b1dc-8080d25990de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f4024d5-285f-43e6-9264-27da408ec01e",
            "compositeImage": {
                "id": "a0489fa9-768f-4279-9c78-45d6316935b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8222516-ea0a-456f-b1dc-8080d25990de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36d58c5-58df-4ec4-a31c-4e1b9be21166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8222516-ea0a-456f-b1dc-8080d25990de",
                    "LayerId": "9273dfec-3364-4037-838c-4671475fdf56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9273dfec-3364-4037-838c-4671475fdf56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f4024d5-285f-43e6-9264-27da408ec01e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}