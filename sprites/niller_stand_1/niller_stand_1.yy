{
    "id": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_stand_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f23159a-f6f5-47a0-9ff1-026bc320aeb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "ab7500aa-51e0-4411-b3a7-5b4d6ae92c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f23159a-f6f5-47a0-9ff1-026bc320aeb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5ddc12-48a7-4b66-93c3-d0cd153566d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f23159a-f6f5-47a0-9ff1-026bc320aeb3",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "aa792563-606e-4eec-be59-9e1c27d74a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "b2416193-965f-4f38-a564-23f52ef86877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa792563-606e-4eec-be59-9e1c27d74a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c5e39a-0c3c-4fcf-9763-c1e5e75c78cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa792563-606e-4eec-be59-9e1c27d74a21",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "c980b963-33d9-40f0-97a0-93155c90920f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "e7c7bf43-5885-4440-b6ea-e61a97d42ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c980b963-33d9-40f0-97a0-93155c90920f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48106302-2f1e-4653-ae4c-3d6fe42851c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c980b963-33d9-40f0-97a0-93155c90920f",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "d2b2c93e-7f7f-4c60-a32e-75f6bb38ade9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "858df617-f85f-4fd4-a0c5-8dba426f6e23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2b2c93e-7f7f-4c60-a32e-75f6bb38ade9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2b42d2-94d7-47f2-aece-88587b6e6778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2b2c93e-7f7f-4c60-a32e-75f6bb38ade9",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "aba63159-2a02-4173-923a-066c2120458c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "f57e4bc8-18c7-40ed-be33-2a4e7c84de50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba63159-2a02-4173-923a-066c2120458c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "571b12ca-a0ef-4ed9-8ac9-fbae8c9e94a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba63159-2a02-4173-923a-066c2120458c",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "8a59cc6f-3ca4-43cc-8aa3-a5fd730782af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "7bdfe1a7-a638-4b3e-840b-6aec8e6293e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a59cc6f-3ca4-43cc-8aa3-a5fd730782af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4e708f-d44e-4c84-9daa-271241c9a67c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a59cc6f-3ca4-43cc-8aa3-a5fd730782af",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "849119be-51b0-4b01-83ba-9b90760ff357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "5989fa16-b1e4-48a7-b574-6357f2a35df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "849119be-51b0-4b01-83ba-9b90760ff357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b183389-48ab-4afa-ba44-411d8ba4321f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "849119be-51b0-4b01-83ba-9b90760ff357",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "6ee48906-7e7f-4d18-bf63-17390fb37bd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "ed89ef24-45e0-4011-8039-763368c85316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee48906-7e7f-4d18-bf63-17390fb37bd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d81a754-629d-4bc1-ab59-47c3592f948a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee48906-7e7f-4d18-bf63-17390fb37bd5",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "631e5d8e-064f-44d8-8640-194f16dcdde5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "26a9deea-e795-444c-9580-18f3bea50e28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "631e5d8e-064f-44d8-8640-194f16dcdde5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfd2637-98ba-4bfd-818c-4af91535915b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "631e5d8e-064f-44d8-8640-194f16dcdde5",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "62b77c9e-f5a0-4e88-85e9-032a944517b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "2fc1d090-d133-4b71-be66-62b013187a1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b77c9e-f5a0-4e88-85e9-032a944517b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3afb9d2b-787f-415c-b2ea-45b68ee45abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b77c9e-f5a0-4e88-85e9-032a944517b5",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "18edaa1b-ab65-4cc0-b18f-90eac73ddec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "cdd0d45d-82ad-4a8b-9178-a80bd45c6474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18edaa1b-ab65-4cc0-b18f-90eac73ddec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc115d04-e51f-4d4e-96a1-95e4dec61e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18edaa1b-ab65-4cc0-b18f-90eac73ddec5",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        },
        {
            "id": "abd7b435-58e4-4736-baf1-84a17cf360a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "compositeImage": {
                "id": "3cc51209-95d7-4e2d-979c-65b55f98bf7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd7b435-58e4-4736-baf1-84a17cf360a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8cc330f-7f3c-4757-92bf-59d14b6d8bcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd7b435-58e4-4736-baf1-84a17cf360a3",
                    "LayerId": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "aa2e409b-5a9c-4bfc-9958-d875b9ec8fa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}