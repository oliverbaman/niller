{
    "id": "746e0188-83df-4fec-ac80-eac0e03e1dfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_climbD_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4756544-443d-4e2e-a3d4-1c68b407578e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746e0188-83df-4fec-ac80-eac0e03e1dfa",
            "compositeImage": {
                "id": "899f928c-65a7-4573-a4a9-0b6c1013c4e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4756544-443d-4e2e-a3d4-1c68b407578e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "867cea8b-871d-4438-beb5-47ccb932a97a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4756544-443d-4e2e-a3d4-1c68b407578e",
                    "LayerId": "07be935b-9de2-49fd-a455-1c10c2e42c4c"
                },
                {
                    "id": "d7eb7a1a-ff1d-4516-bd28-24d5f814362e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4756544-443d-4e2e-a3d4-1c68b407578e",
                    "LayerId": "cbcf28de-f82c-46b7-a406-a2c047835333"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cbcf28de-f82c-46b7-a406-a2c047835333",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746e0188-83df-4fec-ac80-eac0e03e1dfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "07be935b-9de2-49fd-a455-1c10c2e42c4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746e0188-83df-4fec-ac80-eac0e03e1dfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}