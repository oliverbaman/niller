{
    "id": "fbab41d4-0af1-453b-b819-e95d3bd1af4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite77",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f72a470d-bbad-4280-a7d3-16ef2270c1d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbab41d4-0af1-453b-b819-e95d3bd1af4a",
            "compositeImage": {
                "id": "b2b01303-f4c8-4edc-9622-1005135cea3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f72a470d-bbad-4280-a7d3-16ef2270c1d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db46231-4991-41a9-8a17-9599cce32194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f72a470d-bbad-4280-a7d3-16ef2270c1d2",
                    "LayerId": "bb6217f6-6cd5-4e2e-ba97-e63f393462ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bb6217f6-6cd5-4e2e-ba97-e63f393462ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbab41d4-0af1-453b-b819-e95d3bd1af4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}