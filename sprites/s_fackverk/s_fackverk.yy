{
    "id": "6bb2aa88-53eb-46a5-849b-76b5f7558eb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fackverk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7630d51-772d-43b9-940d-628429989456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bb2aa88-53eb-46a5-849b-76b5f7558eb6",
            "compositeImage": {
                "id": "63639ddb-76bc-4307-ae53-978cc3b40118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7630d51-772d-43b9-940d-628429989456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3d637b8-4210-45b6-b4c3-bf227912d4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7630d51-772d-43b9-940d-628429989456",
                    "LayerId": "8df01815-8bab-48d0-9666-1cab9a12dccf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "8df01815-8bab-48d0-9666-1cab9a12dccf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bb2aa88-53eb-46a5-849b-76b5f7558eb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}