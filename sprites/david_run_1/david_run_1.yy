{
    "id": "630cfe44-a2b2-42bf-802e-04d2bb556374",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_run_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8156b35e-b8e4-4aec-82cb-af1b186e6524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "19739522-86e8-40b1-93d3-84435bf69bed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8156b35e-b8e4-4aec-82cb-af1b186e6524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c634a83b-ca6f-49b3-84fa-d66090558869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8156b35e-b8e4-4aec-82cb-af1b186e6524",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "13fa9f2c-8cdc-442a-9e57-d70ac1b88ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8156b35e-b8e4-4aec-82cb-af1b186e6524",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "7a78c8dc-7ae8-43c3-94c4-51c32ad5d168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "04497e03-2f83-4556-a9fb-5e20f63a858d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a78c8dc-7ae8-43c3-94c4-51c32ad5d168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "766ed0d3-27a2-4af3-b104-d2d7b4d3eb4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a78c8dc-7ae8-43c3-94c4-51c32ad5d168",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "e129f7c6-9f1a-487b-b77c-1c365d37e315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a78c8dc-7ae8-43c3-94c4-51c32ad5d168",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "5eafe3cd-4ad8-49dd-b4bf-7100f88e5c9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "003129e5-4102-462b-8629-dee6fa2b4b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eafe3cd-4ad8-49dd-b4bf-7100f88e5c9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8e72ab5-552b-47da-a34a-345a98398e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eafe3cd-4ad8-49dd-b4bf-7100f88e5c9d",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "e895d3c8-4ad5-4d6f-b717-99c59b0c0670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eafe3cd-4ad8-49dd-b4bf-7100f88e5c9d",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "0019dd2d-0aa2-4874-b26e-6beda64b50a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "8324015e-2eaf-4116-a9bf-9a28f3a754c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0019dd2d-0aa2-4874-b26e-6beda64b50a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83448a7-ac38-47b6-966d-c26bd60545cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0019dd2d-0aa2-4874-b26e-6beda64b50a9",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "ee9930cf-e316-4f66-ba19-aed6a9ce6013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0019dd2d-0aa2-4874-b26e-6beda64b50a9",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "29aa94ee-19e6-4e43-8907-717d263bfe49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "18b3f06e-b823-4766-a77c-6ec07d5ed11f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29aa94ee-19e6-4e43-8907-717d263bfe49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382c546f-55b3-4f76-babc-84efe51a8e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29aa94ee-19e6-4e43-8907-717d263bfe49",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "f85e8809-b146-4cc7-94e3-88290d88a7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29aa94ee-19e6-4e43-8907-717d263bfe49",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "8cfec13e-15fc-4c33-8753-3c3026823d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "b89f4f7b-a750-4b44-86fe-452bfa87fee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cfec13e-15fc-4c33-8753-3c3026823d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d40f7ad5-939d-42bd-98c3-4104e459e565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cfec13e-15fc-4c33-8753-3c3026823d22",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "3f5a0ad2-cb2a-4bf7-b758-b6627d33f6d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cfec13e-15fc-4c33-8753-3c3026823d22",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "b4831b51-d166-4f0f-80e0-b953f8c76476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "847c017c-dd6d-4d9a-ac24-443f47013aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4831b51-d166-4f0f-80e0-b953f8c76476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9f68d5-ebc3-4f12-8823-719961e58e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4831b51-d166-4f0f-80e0-b953f8c76476",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "a214809f-dad2-4858-96a2-081d6105b4df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4831b51-d166-4f0f-80e0-b953f8c76476",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "f176571a-5f6f-4597-b448-55e362a73580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "817ba893-d3dc-4cc1-a4df-41030b1725fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f176571a-5f6f-4597-b448-55e362a73580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f285adb-9cfa-4e81-8bd9-3f14b063d3eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f176571a-5f6f-4597-b448-55e362a73580",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "6f9de756-3b83-4890-808e-1748078f21ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f176571a-5f6f-4597-b448-55e362a73580",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        },
        {
            "id": "9ef88cbb-1f44-4613-927b-688ec4b71ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "compositeImage": {
                "id": "01fd241e-834a-4c80-83ca-385249465191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef88cbb-1f44-4613-927b-688ec4b71ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657ba4d0-d532-48d3-901d-5a5375ed12e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef88cbb-1f44-4613-927b-688ec4b71ac1",
                    "LayerId": "f886be31-a532-4df6-9796-f15c6c665440"
                },
                {
                    "id": "0b90bd66-263a-4039-8b51-a84fd20d899b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef88cbb-1f44-4613-927b-688ec4b71ac1",
                    "LayerId": "a80f2591-007e-40cd-a1c3-41b78c421493"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a80f2591-007e-40cd-a1c3-41b78c421493",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f886be31-a532-4df6-9796-f15c6c665440",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "630cfe44-a2b2-42bf-802e-04d2bb556374",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4280824586
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": -3,
    "yorig": 4
}