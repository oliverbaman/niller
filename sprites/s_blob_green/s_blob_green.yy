{
    "id": "acd5bd56-3c36-4ed5-8a59-71a865cfe26a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_blob_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27c5bcb4-eff2-4ac1-b3f3-012616690465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd5bd56-3c36-4ed5-8a59-71a865cfe26a",
            "compositeImage": {
                "id": "933b68be-a4f7-4826-9962-3d094612d423",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27c5bcb4-eff2-4ac1-b3f3-012616690465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4da3dc6-738f-4131-a44d-7244d1195e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27c5bcb4-eff2-4ac1-b3f3-012616690465",
                    "LayerId": "9d6116b0-bcc9-4f30-9182-34fe9c3ae425"
                }
            ]
        },
        {
            "id": "ce0d54af-de99-470a-b755-0ece3dba2390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd5bd56-3c36-4ed5-8a59-71a865cfe26a",
            "compositeImage": {
                "id": "aec3517c-2357-4630-bea2-50bef874de47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce0d54af-de99-470a-b755-0ece3dba2390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c53750f5-55be-4e2c-87c1-7ca9ce1f5a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce0d54af-de99-470a-b755-0ece3dba2390",
                    "LayerId": "9d6116b0-bcc9-4f30-9182-34fe9c3ae425"
                }
            ]
        },
        {
            "id": "5e87a9c6-3cc6-4e34-8bd3-23292c8dc9f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd5bd56-3c36-4ed5-8a59-71a865cfe26a",
            "compositeImage": {
                "id": "ac7326fa-f746-438e-ad1d-9a84ac5eb69f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e87a9c6-3cc6-4e34-8bd3-23292c8dc9f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50f9c051-d1dc-4314-9e22-e60debead953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e87a9c6-3cc6-4e34-8bd3-23292c8dc9f9",
                    "LayerId": "9d6116b0-bcc9-4f30-9182-34fe9c3ae425"
                }
            ]
        },
        {
            "id": "971cba72-a987-413a-90c9-35aa0a53353b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd5bd56-3c36-4ed5-8a59-71a865cfe26a",
            "compositeImage": {
                "id": "d2c8cb6a-be8e-4117-ad2a-bca5b1ef5e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "971cba72-a987-413a-90c9-35aa0a53353b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f990449c-ac24-40b5-a3a0-57a06aba04a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "971cba72-a987-413a-90c9-35aa0a53353b",
                    "LayerId": "9d6116b0-bcc9-4f30-9182-34fe9c3ae425"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "9d6116b0-bcc9-4f30-9182-34fe9c3ae425",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acd5bd56-3c36-4ed5-8a59-71a865cfe26a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4288806736,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        1931299855
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 1,
    "yorig": 1
}