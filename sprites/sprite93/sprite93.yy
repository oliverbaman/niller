{
    "id": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite93",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 289,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ded03b8f-4458-4119-9df6-d76249bf69e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "40565ea3-4e9e-4d36-9e5b-8a86cf39cc6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded03b8f-4458-4119-9df6-d76249bf69e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0953aa9-e27b-4e67-b906-eba0c2c62562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded03b8f-4458-4119-9df6-d76249bf69e8",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "2443bd0a-5e8c-4595-9cab-984a9be68279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "953b2138-32c7-4293-b011-7e9981740b46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2443bd0a-5e8c-4595-9cab-984a9be68279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bb34761-dd92-4f5d-9f4b-d445f0029b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2443bd0a-5e8c-4595-9cab-984a9be68279",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "92614717-d78f-4956-842e-25477e52426c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "e02393eb-244b-4125-8d23-6d763da79207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92614717-d78f-4956-842e-25477e52426c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45fe7be9-118f-48ce-8675-e77ca5bfc147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92614717-d78f-4956-842e-25477e52426c",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "fffd0d1b-86c4-4e7a-bcbb-65b2e08b951b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "07278b8c-785c-4bde-8dda-8d03158af786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fffd0d1b-86c4-4e7a-bcbb-65b2e08b951b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990aa8da-3207-4fd3-8e8b-6dc081223d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fffd0d1b-86c4-4e7a-bcbb-65b2e08b951b",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "f514e9d0-baa9-427d-a6b3-8544ca6f365e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "30ed0c2c-8ba0-4289-b5ac-62bd86edc57b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f514e9d0-baa9-427d-a6b3-8544ca6f365e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2025e2-2db3-4dea-910c-5ea648ae9dab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f514e9d0-baa9-427d-a6b3-8544ca6f365e",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "55ff4d17-b845-4624-a4d2-0bdb6025d59e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "58cf190c-a2c1-43f9-b7de-4598b0b008f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ff4d17-b845-4624-a4d2-0bdb6025d59e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e549305e-29d6-4fff-9f1d-eff9b145b0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ff4d17-b845-4624-a4d2-0bdb6025d59e",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "c62bdf98-a899-4f5a-8b03-bc5f7c9d4770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "2911e6ec-86bb-4a5c-8acd-6094cf30f53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c62bdf98-a899-4f5a-8b03-bc5f7c9d4770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d16aa6d-f01d-49cd-a2c4-eae79ad43e20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c62bdf98-a899-4f5a-8b03-bc5f7c9d4770",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "401c2c91-4fc8-4dc3-8a65-71c598c24501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "da89ad64-65a7-4585-a600-671f0895b41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401c2c91-4fc8-4dc3-8a65-71c598c24501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40679978-94a3-4f9a-8f45-9519b5d651d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401c2c91-4fc8-4dc3-8a65-71c598c24501",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "fb78f2e0-36b3-4d2e-b741-14cfa158a52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "125100d8-f155-4d72-8f69-faf7a5143fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb78f2e0-36b3-4d2e-b741-14cfa158a52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90e808a7-1aa6-4dd1-9768-cab928a54ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb78f2e0-36b3-4d2e-b741-14cfa158a52e",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "06831078-79dc-4e0d-94da-a577619518b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "b737109a-2b05-4754-8e3d-c1646c9f96ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06831078-79dc-4e0d-94da-a577619518b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf007e8f-8513-4ed0-a077-705339535f60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06831078-79dc-4e0d-94da-a577619518b7",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "8faef496-b9b2-4c69-9ded-966f22de0d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "9a872a04-1830-42a2-8806-b10698ab8c19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8faef496-b9b2-4c69-9ded-966f22de0d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d381e64-fc4d-40d0-9bc5-80e7a294fd3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8faef496-b9b2-4c69-9ded-966f22de0d65",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "afd98046-7808-4f76-acb2-1cbcf16fb70d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "80e3d113-db7f-4fd8-a02c-5dd359c43cb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd98046-7808-4f76-acb2-1cbcf16fb70d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53c2490b-ec08-46f2-a722-e58fc7446373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd98046-7808-4f76-acb2-1cbcf16fb70d",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "aa7bcc63-6736-47f0-bc89-d285125b1029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "2b41f4c5-bc30-4af9-9742-bcdfc54e7b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7bcc63-6736-47f0-bc89-d285125b1029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a54397-938c-4606-9c97-f8aa7d3f0d7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7bcc63-6736-47f0-bc89-d285125b1029",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "8011e382-eddc-49d5-a508-44e60ff6ee54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "37b94850-2961-4e17-b434-7e2d33d2b1a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8011e382-eddc-49d5-a508-44e60ff6ee54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cb97d3-eb73-49db-98f0-425f19e5e3cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8011e382-eddc-49d5-a508-44e60ff6ee54",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "545b31dc-0ebf-42b2-9472-642332e6deb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "ea2ca4b3-e931-4790-bc0a-6fc79863a9dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "545b31dc-0ebf-42b2-9472-642332e6deb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb94e533-7385-447d-8048-81d92fb876a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "545b31dc-0ebf-42b2-9472-642332e6deb5",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "c713e94d-c8b6-4679-9fbb-fa4cbcba4755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "3304555e-7824-4913-840b-2305eb420d1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c713e94d-c8b6-4679-9fbb-fa4cbcba4755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91633371-92fb-4d05-a9dd-b32fb63cb439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c713e94d-c8b6-4679-9fbb-fa4cbcba4755",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "c2c2546d-a808-481d-9e17-240ab36ed297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "a56effc9-b3bf-46c2-a893-5b512188b634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c2546d-a808-481d-9e17-240ab36ed297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b027e957-4c2f-41d3-a43e-820d230c9f8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c2546d-a808-481d-9e17-240ab36ed297",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "f414dda0-d96f-4aaa-b957-16356aeb1181",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "dd33cafe-e14f-41c2-a251-4a5b4b5a64ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f414dda0-d96f-4aaa-b957-16356aeb1181",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d0b3afe-2add-426c-9669-218a1f26db18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f414dda0-d96f-4aaa-b957-16356aeb1181",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "4fd418ca-b544-41f9-b1e2-93fc59a3f8b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "0994b51f-e378-42b8-9128-dec3b945cc9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd418ca-b544-41f9-b1e2-93fc59a3f8b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "897cddfe-fa9d-47de-9e10-c91d8b1931a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd418ca-b544-41f9-b1e2-93fc59a3f8b5",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "372553d2-82e4-4360-b75f-8b05641b81cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "536f1fbc-ed94-4273-b873-17393a711731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372553d2-82e4-4360-b75f-8b05641b81cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22b0f61-e25b-4bb6-b093-39c1e7688e76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372553d2-82e4-4360-b75f-8b05641b81cc",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "16187aa3-eccc-4d87-8ed0-a4890eb49d54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "a1bddcd0-c217-480b-9880-fbaf0ab7db23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16187aa3-eccc-4d87-8ed0-a4890eb49d54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd19871d-95e5-435a-a8b6-e44e31fb0b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16187aa3-eccc-4d87-8ed0-a4890eb49d54",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "81145e35-f61b-4fb0-8e39-760a1339f364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "11ee23bb-e2ee-4e29-8c3d-80af136a8d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81145e35-f61b-4fb0-8e39-760a1339f364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86f244e6-7ce3-434f-ab0e-61359cc0d929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81145e35-f61b-4fb0-8e39-760a1339f364",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "19314d83-4dd3-4b8b-b182-34c4b02e5145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "970dfe5a-c9f1-4aed-bb0d-b13aa3ad4624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19314d83-4dd3-4b8b-b182-34c4b02e5145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcf6cf2a-f140-4a8b-aae6-b302fcd62d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19314d83-4dd3-4b8b-b182-34c4b02e5145",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "1e3a51dd-e02e-4c00-a6ad-f0e33c0dfbaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "750a7e83-5c6f-4fd0-8dbe-a8c09785b3db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e3a51dd-e02e-4c00-a6ad-f0e33c0dfbaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b0a725-7f75-4286-ad68-ef585d4fdc6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e3a51dd-e02e-4c00-a6ad-f0e33c0dfbaa",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "46622216-cda5-4c60-8cd3-b54da0b14d5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "a1f35ccf-2834-4429-823b-409eaf3e1cb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46622216-cda5-4c60-8cd3-b54da0b14d5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5338ae50-64ae-42ac-ade5-920af1999cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46622216-cda5-4c60-8cd3-b54da0b14d5f",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "6b284816-9d5f-4437-b85f-d7e5ee8ccb1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "a0453f3d-bda0-4c4e-802e-48474ea4033d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b284816-9d5f-4437-b85f-d7e5ee8ccb1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "389aaea3-11f0-42a2-994a-a34e22735fad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b284816-9d5f-4437-b85f-d7e5ee8ccb1a",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "3dbc6a61-33e1-4d34-93bf-370e3662b825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "f341c008-da1a-4a1a-89f8-6da31d227ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dbc6a61-33e1-4d34-93bf-370e3662b825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7880cfa-c016-4149-8fec-7b706927a56e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dbc6a61-33e1-4d34-93bf-370e3662b825",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "9ec62853-87c4-4539-a242-19faa32a9874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "58be0f70-6cc4-43f6-bc34-eba34fa4a456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec62853-87c4-4539-a242-19faa32a9874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b843bc9-238d-4eb2-bf29-0ee6fa3c8067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec62853-87c4-4539-a242-19faa32a9874",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "f4d879cf-2c86-4e7d-b3d3-6af5a913c8cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "2db56f4a-9ff7-4dfd-8c69-72d24d44f82a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4d879cf-2c86-4e7d-b3d3-6af5a913c8cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c65a618-a15a-4e2e-ad1b-229c6b6fecf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d879cf-2c86-4e7d-b3d3-6af5a913c8cc",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "a288e5ea-c91d-45c9-a41e-4440f0789f89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "d00c6406-28df-4ad9-8138-791b1c2b9242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a288e5ea-c91d-45c9-a41e-4440f0789f89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "791fb924-7d62-4ea5-800d-bcf40b0f9756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a288e5ea-c91d-45c9-a41e-4440f0789f89",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "0bbf6995-1e95-41c3-8e51-73a9ce6e0210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "c65eb9fe-d658-40dc-843a-9878bebf3541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbf6995-1e95-41c3-8e51-73a9ce6e0210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10222bc8-43dc-41b7-b13e-181b6acaf5df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbf6995-1e95-41c3-8e51-73a9ce6e0210",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "dd53fa56-c0d6-4df4-90e5-740c409366a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "282e136b-bddf-49f2-9722-2c3bea1cc61d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd53fa56-c0d6-4df4-90e5-740c409366a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddf9835a-8c00-4cac-8635-599a302e405f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd53fa56-c0d6-4df4-90e5-740c409366a6",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "2f0e3329-7474-4799-8847-baf939b3113a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "0b8f7e65-5aba-430a-b219-6cde38330701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0e3329-7474-4799-8847-baf939b3113a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a04c45d-d336-4c4b-9be5-db7b1c6cf366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0e3329-7474-4799-8847-baf939b3113a",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "adc80791-44fa-423f-850d-7fcc5178080e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "cdfb2209-9668-4c82-a291-3f8d58feec1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adc80791-44fa-423f-850d-7fcc5178080e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b86f9d7b-38d6-4a8d-9aa5-758f2d3bb239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adc80791-44fa-423f-850d-7fcc5178080e",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "dace6d18-7230-4dfd-988f-86f68ac0cfb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "29e951bf-9e9f-4c61-9f1a-3beb03c78588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dace6d18-7230-4dfd-988f-86f68ac0cfb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "139b99bf-7b66-43fb-8074-b15274179f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dace6d18-7230-4dfd-988f-86f68ac0cfb6",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "98f0f4aa-739e-4dd1-a80c-3e9fcd41afb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "5bee0c86-8209-450d-a4e2-f1c9086f55b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f0f4aa-739e-4dd1-a80c-3e9fcd41afb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0bbcd5e-ae2f-45b2-a77f-89841820ac51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f0f4aa-739e-4dd1-a80c-3e9fcd41afb8",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "7109f039-1e86-4a5c-8263-5a3cca88fef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "6e30ce91-2138-4afb-acd4-dc1a69c5a3be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7109f039-1e86-4a5c-8263-5a3cca88fef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8a982e-f70d-4c49-9d95-3576799f4d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7109f039-1e86-4a5c-8263-5a3cca88fef0",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "5ded7553-6dee-4603-a9ec-16fb519ec5a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "9cbc934c-b918-4a71-b62a-4aef307c6594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ded7553-6dee-4603-a9ec-16fb519ec5a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a79ee7e6-c210-4056-83e3-b7cc334fc14f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ded7553-6dee-4603-a9ec-16fb519ec5a3",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "2ca77010-cc77-409d-915a-056ef1cfc193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "22523854-8375-4052-b22a-1b1a876a9596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca77010-cc77-409d-915a-056ef1cfc193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc96458d-97ef-4624-9c4f-e706cb20440e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca77010-cc77-409d-915a-056ef1cfc193",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        },
        {
            "id": "c4abb527-3d8f-4dae-b212-5f0b91a38415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "compositeImage": {
                "id": "e0597eb3-9737-41a5-9b96-688dbdc19b54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4abb527-3d8f-4dae-b212-5f0b91a38415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f437dce7-3aae-452c-a702-3b4ec7dc8e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4abb527-3d8f-4dae-b212-5f0b91a38415",
                    "LayerId": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "b9e7d28f-58b9-4c42-b2f8-cfd70b4107d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 290,
    "xorig": 0,
    "yorig": 0
}