{
    "id": "945338bc-ac97-4dc1-8337-0f255ff41392",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_boostR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 5,
    "bbox_right": 16,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89901d7c-372a-4a75-aeb7-88ddb6d5753b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945338bc-ac97-4dc1-8337-0f255ff41392",
            "compositeImage": {
                "id": "8d5a5374-83dc-4b8b-baf7-bdd101f75e48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89901d7c-372a-4a75-aeb7-88ddb6d5753b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46d527fc-e549-45ed-941e-3cd821ed8502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89901d7c-372a-4a75-aeb7-88ddb6d5753b",
                    "LayerId": "9cba2615-00c1-445d-a5eb-0c68c872bfa6"
                }
            ]
        },
        {
            "id": "2d5285e5-b839-4bf1-b9e2-99c4cde048c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945338bc-ac97-4dc1-8337-0f255ff41392",
            "compositeImage": {
                "id": "9b8d595d-ccbb-406a-89f9-f473bb0a95d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5285e5-b839-4bf1-b9e2-99c4cde048c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ac3d9a-b666-4d2b-bc67-b9bce03dbf3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5285e5-b839-4bf1-b9e2-99c4cde048c6",
                    "LayerId": "9cba2615-00c1-445d-a5eb-0c68c872bfa6"
                }
            ]
        },
        {
            "id": "2dba7ee0-9de7-4f97-b7eb-c39c48cb98ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945338bc-ac97-4dc1-8337-0f255ff41392",
            "compositeImage": {
                "id": "e2daeb40-ab61-4344-bc06-0ca611421a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dba7ee0-9de7-4f97-b7eb-c39c48cb98ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b41507a9-e388-402c-9dd2-f0f317d56ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dba7ee0-9de7-4f97-b7eb-c39c48cb98ae",
                    "LayerId": "9cba2615-00c1-445d-a5eb-0c68c872bfa6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "9cba2615-00c1-445d-a5eb-0c68c872bfa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "945338bc-ac97-4dc1-8337-0f255ff41392",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 12,
    "yorig": 7
}