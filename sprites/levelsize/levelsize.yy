{
    "id": "824d66cc-692a-4890-97d1-061ad93f0b8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "levelsize",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 289,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6449d37-1f42-4b4b-b337-da9f749b64fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "824d66cc-692a-4890-97d1-061ad93f0b8f",
            "compositeImage": {
                "id": "756b84ef-b09d-4d2b-8128-cb18aeed9347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6449d37-1f42-4b4b-b337-da9f749b64fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4cff1c5-5cab-45d6-be47-68c8ac6366d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6449d37-1f42-4b4b-b337-da9f749b64fa",
                    "LayerId": "c2419992-96d0-4a3a-a6c9-4b4a6d819482"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "c2419992-96d0-4a3a-a6c9-4b4a6d819482",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "824d66cc-692a-4890-97d1-061ad93f0b8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 290,
    "xorig": 0,
    "yorig": 0
}