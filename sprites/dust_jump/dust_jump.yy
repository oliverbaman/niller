{
    "id": "46923846-e426-4896-aab2-36515d3d20a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dust_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e05aed15-128e-47d7-a561-c20773c281aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46923846-e426-4896-aab2-36515d3d20a5",
            "compositeImage": {
                "id": "58453bcb-a225-49f7-a9bd-e5885c78541d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05aed15-128e-47d7-a561-c20773c281aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea7b0cad-15af-40be-a61a-580f30a19151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05aed15-128e-47d7-a561-c20773c281aa",
                    "LayerId": "3b516ef4-5a65-445b-91f8-a74a81253d52"
                }
            ]
        },
        {
            "id": "dde457d2-5060-41c3-a8aa-2616931b5e29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46923846-e426-4896-aab2-36515d3d20a5",
            "compositeImage": {
                "id": "d48a6224-8f25-44be-83da-6b97487a4ca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dde457d2-5060-41c3-a8aa-2616931b5e29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc1a7bab-9472-4d8a-9985-1660d83d4eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dde457d2-5060-41c3-a8aa-2616931b5e29",
                    "LayerId": "3b516ef4-5a65-445b-91f8-a74a81253d52"
                }
            ]
        },
        {
            "id": "b752e051-a8e0-4e69-9f14-9fbbf6df1704",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46923846-e426-4896-aab2-36515d3d20a5",
            "compositeImage": {
                "id": "55aa3210-28b4-4c5b-a341-fb7af4ef9a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b752e051-a8e0-4e69-9f14-9fbbf6df1704",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e0b1efd-7946-4fc3-bae4-7704c52e85ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b752e051-a8e0-4e69-9f14-9fbbf6df1704",
                    "LayerId": "3b516ef4-5a65-445b-91f8-a74a81253d52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "3b516ef4-5a65-445b-91f8-a74a81253d52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46923846-e426-4896-aab2-36515d3d20a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 6,
    "yorig": 8
}