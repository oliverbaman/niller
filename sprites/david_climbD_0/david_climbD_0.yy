{
    "id": "0f7793f2-d1d3-4cf8-80e5-768497959d0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_climbD_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf24a222-ce0e-488e-bdc8-3d80c1fcd46e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f7793f2-d1d3-4cf8-80e5-768497959d0d",
            "compositeImage": {
                "id": "dc1d89dd-662c-410d-9da9-240ea3e0302d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf24a222-ce0e-488e-bdc8-3d80c1fcd46e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c83a076-035b-4344-a220-99895701b998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf24a222-ce0e-488e-bdc8-3d80c1fcd46e",
                    "LayerId": "9a933512-6d9d-4569-8e8c-315a8f10bc22"
                },
                {
                    "id": "f3ddae0b-1b8a-43d0-aa3b-19522367ad21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf24a222-ce0e-488e-bdc8-3d80c1fcd46e",
                    "LayerId": "2964284c-66c8-48c7-9cef-d3b03010da25"
                },
                {
                    "id": "cb842786-d677-4ca6-8009-1a92e75430ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf24a222-ce0e-488e-bdc8-3d80c1fcd46e",
                    "LayerId": "a4ef46b2-22fb-47be-a84b-8c03c950fbdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a4ef46b2-22fb-47be-a84b-8c03c950fbdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f7793f2-d1d3-4cf8-80e5-768497959d0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "9a933512-6d9d-4569-8e8c-315a8f10bc22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f7793f2-d1d3-4cf8-80e5-768497959d0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "2964284c-66c8-48c7-9cef-d3b03010da25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f7793f2-d1d3-4cf8-80e5-768497959d0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}