{
    "id": "431811cb-3945-49a4-b1f1-71535d7929f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite85",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ede9ee06-f3c1-4243-a000-824b10d1150d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "431811cb-3945-49a4-b1f1-71535d7929f4",
            "compositeImage": {
                "id": "edd0e37f-6534-49c3-a382-46db9bef0893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede9ee06-f3c1-4243-a000-824b10d1150d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ea102d-625a-4ba4-8c84-e1f54b723d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede9ee06-f3c1-4243-a000-824b10d1150d",
                    "LayerId": "90c0c952-74ed-429c-9d32-d35c83be2199"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "90c0c952-74ed-429c-9d32-d35c83be2199",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "431811cb-3945-49a4-b1f1-71535d7929f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}