{
    "id": "4f3d560d-9fa9-475a-ba36-d5ccaec42ef6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_climbO_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4813730-dea9-465d-9c1f-5f2f362b137a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f3d560d-9fa9-475a-ba36-d5ccaec42ef6",
            "compositeImage": {
                "id": "9e77d63d-0fb8-4faf-9e18-db3c79c279a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4813730-dea9-465d-9c1f-5f2f362b137a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41824ca-fa56-49c0-b912-15b943ddf23e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4813730-dea9-465d-9c1f-5f2f362b137a",
                    "LayerId": "3c4e3f25-8a79-4038-aaa9-0bfa2c5d4508"
                },
                {
                    "id": "eba4cea4-1a99-46be-a51b-eea4c72a06b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4813730-dea9-465d-9c1f-5f2f362b137a",
                    "LayerId": "541e168c-baf4-400d-b248-32a338feacca"
                }
            ]
        },
        {
            "id": "4c3a2f54-c4ac-428f-a1b1-d300a324ea91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f3d560d-9fa9-475a-ba36-d5ccaec42ef6",
            "compositeImage": {
                "id": "f86741e6-f546-47f4-be64-22c1970b3fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c3a2f54-c4ac-428f-a1b1-d300a324ea91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e807166e-99d3-4dbc-8855-5a329fb5f9f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c3a2f54-c4ac-428f-a1b1-d300a324ea91",
                    "LayerId": "541e168c-baf4-400d-b248-32a338feacca"
                },
                {
                    "id": "608a8c18-75f1-4d16-a335-2f38a8b374cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c3a2f54-c4ac-428f-a1b1-d300a324ea91",
                    "LayerId": "3c4e3f25-8a79-4038-aaa9-0bfa2c5d4508"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "541e168c-baf4-400d-b248-32a338feacca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f3d560d-9fa9-475a-ba36-d5ccaec42ef6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "3c4e3f25-8a79-4038-aaa9-0bfa2c5d4508",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f3d560d-9fa9-475a-ba36-d5ccaec42ef6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 4,
    "yorig": 5
}