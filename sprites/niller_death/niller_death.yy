{
    "id": "fd2a297a-8951-4fe6-92e1-4770327584c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 28,
    "bbox_right": 37,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2664c7e6-4ffb-4c13-9278-4f0e9477f852",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "d82f6f63-9584-4af0-91e4-2c389217b7ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2664c7e6-4ffb-4c13-9278-4f0e9477f852",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8f0fc40-8995-4314-95e6-09ef58b91eec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2664c7e6-4ffb-4c13-9278-4f0e9477f852",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "945a4557-be52-4cd7-b68f-13c377d1f43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2664c7e6-4ffb-4c13-9278-4f0e9477f852",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "053162d3-3064-41b7-bf88-6d43cece9179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "b9abae2f-5eae-4afb-abcf-b37f472cdac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "053162d3-3064-41b7-bf88-6d43cece9179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1feefea8-0363-4375-a7a8-f626ded89ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053162d3-3064-41b7-bf88-6d43cece9179",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "96bc9324-2b55-4a9a-abeb-400fe3a37ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053162d3-3064-41b7-bf88-6d43cece9179",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "6d68386c-d7f2-42b6-8839-d10de682745b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "82dd98e1-ae4b-4e68-ba6e-d8ed800df9e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d68386c-d7f2-42b6-8839-d10de682745b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ec60f8d-8e2d-405b-afe8-82c1c61b3972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d68386c-d7f2-42b6-8839-d10de682745b",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "4f89a27d-827b-4991-9863-0f5ab204b5e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d68386c-d7f2-42b6-8839-d10de682745b",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "14b517ec-8332-4d2f-8193-54daaff3bfd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "20efba04-2ffc-4f81-a7b7-ffae34212ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b517ec-8332-4d2f-8193-54daaff3bfd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4f0b52-3b8e-4003-af34-d267e9456f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b517ec-8332-4d2f-8193-54daaff3bfd6",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "70959559-940d-41cf-a7a5-93019eaae754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b517ec-8332-4d2f-8193-54daaff3bfd6",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "f051583e-9eff-4c9e-90fa-5b238d944536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "61939a5d-8de4-453a-ab67-a085ceed8943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f051583e-9eff-4c9e-90fa-5b238d944536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16e1ce0b-295b-44ca-9bdd-369638e6473b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f051583e-9eff-4c9e-90fa-5b238d944536",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "c1007844-2b79-42a5-8a9e-ee7d06e57e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f051583e-9eff-4c9e-90fa-5b238d944536",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "d2f0d74a-ac53-4754-a3e8-bf190489fa6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "4c8e619b-6338-4f84-b720-d2ba0a20aa49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f0d74a-ac53-4754-a3e8-bf190489fa6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a16d28e7-ec49-4a63-97f6-61321450e259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f0d74a-ac53-4754-a3e8-bf190489fa6b",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "38807727-d637-476c-a468-f79b03817e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f0d74a-ac53-4754-a3e8-bf190489fa6b",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "2451aade-e608-4241-b98f-8e112744e7e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "c75279c7-2743-4d29-95f8-fed8399c8f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2451aade-e608-4241-b98f-8e112744e7e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e0df53-1096-4758-a79e-19b4e4bacc79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2451aade-e608-4241-b98f-8e112744e7e7",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "f0293b8e-f8f1-4689-9e71-ff52d673868e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2451aade-e608-4241-b98f-8e112744e7e7",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "f97ccf2f-12b9-47e9-9391-b56a16ecfc0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "eb90cb1c-10b1-4970-a3ad-8c05c212ddb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f97ccf2f-12b9-47e9-9391-b56a16ecfc0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f2bad5a-555a-4994-9fd0-103ea6b18bc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f97ccf2f-12b9-47e9-9391-b56a16ecfc0b",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "f0eaf2a3-82a2-4b11-9935-03e5daffb018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f97ccf2f-12b9-47e9-9391-b56a16ecfc0b",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        },
        {
            "id": "e1faafd5-1c5b-4796-b03c-01f1c024b46e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "compositeImage": {
                "id": "bf0f0b20-325c-4f3e-ab3c-8f69da399a55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1faafd5-1c5b-4796-b03c-01f1c024b46e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c79ac967-e4e5-4f50-b9ee-2125335d32ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1faafd5-1c5b-4796-b03c-01f1c024b46e",
                    "LayerId": "b2f743a0-ea72-4f98-938c-fff042a8e0e2"
                },
                {
                    "id": "d56ed829-3f26-4428-83b0-5566b740bc2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1faafd5-1c5b-4796-b03c-01f1c024b46e",
                    "LayerId": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "c87a1eae-d4f3-47c9-8d52-fde2e9d957a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "blendMode": 3,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "b2f743a0-ea72-4f98-938c-fff042a8e0e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 30
}