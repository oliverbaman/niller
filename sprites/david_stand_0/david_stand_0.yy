{
    "id": "918cf776-db69-4c22-b901-982dfc66cdd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_stand_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a83f0eb-5b9d-46b0-b899-a7fd0d4f00db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "98ce5e91-a151-404c-adca-48cb34fc5f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a83f0eb-5b9d-46b0-b899-a7fd0d4f00db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ca07c7-935b-4ca4-b350-ed16acc23f9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a83f0eb-5b9d-46b0-b899-a7fd0d4f00db",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "ebee3d81-deba-41a1-9938-5f07087eb51d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "2f21cc34-05ef-4cae-92e5-1067a1f0db0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebee3d81-deba-41a1-9938-5f07087eb51d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c5778a2-6fe6-4121-826e-cab031811083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebee3d81-deba-41a1-9938-5f07087eb51d",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "1af9b69c-c952-4750-b058-a3308f1c7b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "08dbaea5-e630-4724-8ab1-fdf7aca7ed33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1af9b69c-c952-4750-b058-a3308f1c7b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81359a0f-2fba-47dd-b07f-1a9261fafd7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af9b69c-c952-4750-b058-a3308f1c7b72",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "f746c44b-7a9a-4159-9c3d-81571faf9f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "ab9f93d0-db2a-404c-800e-2014d0a5448f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f746c44b-7a9a-4159-9c3d-81571faf9f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0109106e-bb55-4d4a-88b3-370225b85ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f746c44b-7a9a-4159-9c3d-81571faf9f53",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "eeedeca7-f0f9-445f-87d0-eb8cb9dd50e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "8966c15b-4021-48af-9531-baf88b4e0f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeedeca7-f0f9-445f-87d0-eb8cb9dd50e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d141f3a-b392-42c1-87e7-6b912daea5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeedeca7-f0f9-445f-87d0-eb8cb9dd50e3",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "124c20c4-9582-406b-8260-0e027d4c2857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "680d09be-89fa-4f08-a816-e6eab7b63001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "124c20c4-9582-406b-8260-0e027d4c2857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf3ee3ba-b930-413f-ba61-4947c7bf7113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124c20c4-9582-406b-8260-0e027d4c2857",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "8e19e226-f5f6-4c38-8019-8002de011ec4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "e43e3965-d8f6-451f-8cfe-9dbf07841822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e19e226-f5f6-4c38-8019-8002de011ec4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace75a85-bb32-4b37-bd05-0e4d3d4e913d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e19e226-f5f6-4c38-8019-8002de011ec4",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "2725a5cd-f451-434c-a444-9ff6481d2cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "1e0ab18c-a85d-49d8-9b4e-909dac66c487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2725a5cd-f451-434c-a444-9ff6481d2cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59e7a528-21dd-413e-9411-8e9bf656d503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2725a5cd-f451-434c-a444-9ff6481d2cd6",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "1bf51244-78a8-4d6e-9689-031128b2147c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "c0e3fe59-aeb3-421d-a48a-8b0efdcb5d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bf51244-78a8-4d6e-9689-031128b2147c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a1c973e-aa6e-4f35-9271-67ac6367651e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf51244-78a8-4d6e-9689-031128b2147c",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "0c3dab4d-656e-4a07-b8ca-a6a1baa36425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "f2206a1a-aba2-4f5b-b3a8-b00cc0d6b5cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3dab4d-656e-4a07-b8ca-a6a1baa36425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73784385-2d9f-4ce9-9271-ced84af1adbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3dab4d-656e-4a07-b8ca-a6a1baa36425",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "78eae0e9-67f1-4bd5-b785-95fb58a05b9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "ddaad8fa-e1c5-4893-8788-c9d5fbee80c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78eae0e9-67f1-4bd5-b785-95fb58a05b9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abeeb891-8229-4b73-83e3-de86d73164e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78eae0e9-67f1-4bd5-b785-95fb58a05b9d",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        },
        {
            "id": "026682ca-133d-4afa-b0e6-2739fa90986c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "compositeImage": {
                "id": "c3f0347f-4cd0-4d34-a445-d882e3c74717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026682ca-133d-4afa-b0e6-2739fa90986c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e5cf71-df8a-4490-a650-23c14499f155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026682ca-133d-4afa-b0e6-2739fa90986c",
                    "LayerId": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "da60a0d3-52e2-4f6c-b065-59bfffcd5fd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "918cf776-db69-4c22-b901-982dfc66cdd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}