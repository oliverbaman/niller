{
    "id": "ec4d056e-dfef-41f2-80f9-93ea1ff1442e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_climbD_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84663af4-361e-4e89-8e2c-9c006f2984c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec4d056e-dfef-41f2-80f9-93ea1ff1442e",
            "compositeImage": {
                "id": "95745645-5372-45bc-8bb9-90f79cbe3850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84663af4-361e-4e89-8e2c-9c006f2984c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6825c895-46f4-4cca-869f-018f241aab50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84663af4-361e-4e89-8e2c-9c006f2984c6",
                    "LayerId": "663aea1a-a7eb-4469-aa01-5a11b18b2f99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "663aea1a-a7eb-4469-aa01-5a11b18b2f99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec4d056e-dfef-41f2-80f9-93ea1ff1442e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 6,
    "yorig": 8
}