{
    "id": "38b8ca4b-8ed6-454c-898d-fe756867766a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_menu_niller",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 184,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a479b308-84db-44b8-96f3-56afc82502f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b8ca4b-8ed6-454c-898d-fe756867766a",
            "compositeImage": {
                "id": "45fe49d1-a381-4efd-8a58-f90500c42d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a479b308-84db-44b8-96f3-56afc82502f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b94ac1-96f8-42e4-85a7-a0ef1f7b98ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a479b308-84db-44b8-96f3-56afc82502f4",
                    "LayerId": "07be0771-e504-4f98-86c5-03dd24e1bea6"
                }
            ]
        },
        {
            "id": "a528b692-d98d-4a73-875e-b853e9e922df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b8ca4b-8ed6-454c-898d-fe756867766a",
            "compositeImage": {
                "id": "bd9a97b3-22ab-4388-a0e3-3b4a7f1c9ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a528b692-d98d-4a73-875e-b853e9e922df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c216b5-ce6d-4e8b-b544-78b91f206def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a528b692-d98d-4a73-875e-b853e9e922df",
                    "LayerId": "07be0771-e504-4f98-86c5-03dd24e1bea6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "07be0771-e504-4f98-86c5-03dd24e1bea6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38b8ca4b-8ed6-454c-898d-fe756867766a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 185,
    "xorig": 92,
    "yorig": 16
}