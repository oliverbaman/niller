{
    "id": "efffe5d1-cc7a-4ffc-93ed-7dd3f7eba45c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tuborg_none",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 7,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7628eb7e-057c-45ba-a323-a141b8c835de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efffe5d1-cc7a-4ffc-93ed-7dd3f7eba45c",
            "compositeImage": {
                "id": "0680aaea-84f7-4092-bb26-f01b9a26c05c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7628eb7e-057c-45ba-a323-a141b8c835de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65496c02-d731-48ff-91af-43792b4e1ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7628eb7e-057c-45ba-a323-a141b8c835de",
                    "LayerId": "79ac4e3e-6413-420d-8bed-70acba80a214"
                },
                {
                    "id": "4e81bf2c-626b-4152-bcc2-b6fadd678507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7628eb7e-057c-45ba-a323-a141b8c835de",
                    "LayerId": "d64848f8-c685-4a08-a5a1-c13c333091b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "d64848f8-c685-4a08-a5a1-c13c333091b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efffe5d1-cc7a-4ffc-93ed-7dd3f7eba45c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "79ac4e3e-6413-420d-8bed-70acba80a214",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efffe5d1-cc7a-4ffc-93ed-7dd3f7eba45c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 9
}