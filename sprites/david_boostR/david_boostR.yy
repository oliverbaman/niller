{
    "id": "e5ee2029-7c70-4733-8f07-e372169bf1d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_boostR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83cb9457-a4a0-417b-a87c-805d4f18f39b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5ee2029-7c70-4733-8f07-e372169bf1d7",
            "compositeImage": {
                "id": "662df760-7f21-4ee9-a546-f4871f72e451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83cb9457-a4a0-417b-a87c-805d4f18f39b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fb41de7-dfbc-48de-a735-f4de21746ba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cb9457-a4a0-417b-a87c-805d4f18f39b",
                    "LayerId": "4b2ade0c-b1aa-400a-a96c-2f09e8a6b9f6"
                },
                {
                    "id": "ab3318fd-8e1f-4bfa-a103-a6f6afb86288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cb9457-a4a0-417b-a87c-805d4f18f39b",
                    "LayerId": "2f72ff55-8e7e-42e7-819d-1eb063f8330c"
                }
            ]
        },
        {
            "id": "e2d1bdf8-9130-4420-80a3-ee0c1bfd7b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5ee2029-7c70-4733-8f07-e372169bf1d7",
            "compositeImage": {
                "id": "3d09f8e0-dec1-4b21-a4e3-7e511a4929d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d1bdf8-9130-4420-80a3-ee0c1bfd7b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6683449-84ad-4740-b3d5-a871c1be671a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d1bdf8-9130-4420-80a3-ee0c1bfd7b13",
                    "LayerId": "4b2ade0c-b1aa-400a-a96c-2f09e8a6b9f6"
                },
                {
                    "id": "39383b72-4040-4996-ab9d-2ee67b084753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d1bdf8-9130-4420-80a3-ee0c1bfd7b13",
                    "LayerId": "2f72ff55-8e7e-42e7-819d-1eb063f8330c"
                }
            ]
        },
        {
            "id": "622355d7-296f-4c9f-b31a-faefe60f570e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5ee2029-7c70-4733-8f07-e372169bf1d7",
            "compositeImage": {
                "id": "edd88127-c491-443d-b011-b53ff33ab3a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622355d7-296f-4c9f-b31a-faefe60f570e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26309231-b3f6-44cf-8565-21cce66f9464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622355d7-296f-4c9f-b31a-faefe60f570e",
                    "LayerId": "4b2ade0c-b1aa-400a-a96c-2f09e8a6b9f6"
                },
                {
                    "id": "fee328b5-1cab-4de2-9d0b-e549d245b309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622355d7-296f-4c9f-b31a-faefe60f570e",
                    "LayerId": "2f72ff55-8e7e-42e7-819d-1eb063f8330c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "2f72ff55-8e7e-42e7-819d-1eb063f8330c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5ee2029-7c70-4733-8f07-e372169bf1d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 53,
            "visible": true
        },
        {
            "id": "4b2ade0c-b1aa-400a-a96c-2f09e8a6b9f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5ee2029-7c70-4733-8f07-e372169bf1d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 12,
    "yorig": 7
}