{
    "id": "e817a650-1f27-4c6f-b74a-4101afc54525",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_menu_niller2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 4,
    "bbox_right": 109,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fbbeab6-14e1-449f-a2ee-3a749112393d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e817a650-1f27-4c6f-b74a-4101afc54525",
            "compositeImage": {
                "id": "b50ef11e-5862-43e7-96c0-be5eab90642a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fbbeab6-14e1-449f-a2ee-3a749112393d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1cbc33-05f5-499c-94e8-86e44906af47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fbbeab6-14e1-449f-a2ee-3a749112393d",
                    "LayerId": "ba471638-d21f-4456-ba6a-ff9aa1b519f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "ba471638-d21f-4456-ba6a-ff9aa1b519f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e817a650-1f27-4c6f-b74a-4101afc54525",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 0,
    "yorig": 0
}