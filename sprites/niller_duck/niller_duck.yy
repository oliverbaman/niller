{
    "id": "940eaf93-aba0-4a82-88c3-554327b7ec32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23d64111-b11d-49f1-a73f-5a2ea795eb06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "compositeImage": {
                "id": "7a872319-026e-4d7f-9090-d14999c383d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d64111-b11d-49f1-a73f-5a2ea795eb06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a65c40-be3f-41d4-b108-f79ddbdc51df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d64111-b11d-49f1-a73f-5a2ea795eb06",
                    "LayerId": "44a002c1-7183-42a7-8d7b-8799b04ca664"
                }
            ]
        },
        {
            "id": "ffe0e10d-3098-4dca-b602-edd59606f122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "compositeImage": {
                "id": "4c10e619-10d8-4922-a17e-9b225eba1c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffe0e10d-3098-4dca-b602-edd59606f122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb681ce-4e4b-48a3-a98c-cc49990cb7b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffe0e10d-3098-4dca-b602-edd59606f122",
                    "LayerId": "44a002c1-7183-42a7-8d7b-8799b04ca664"
                }
            ]
        },
        {
            "id": "f862c9f2-4650-4f9e-b691-6da04bab86eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "compositeImage": {
                "id": "0b772b25-67fe-4352-bec6-b3a600dee377",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f862c9f2-4650-4f9e-b691-6da04bab86eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad1606d-ed79-4df4-98b6-3a0df2723d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f862c9f2-4650-4f9e-b691-6da04bab86eb",
                    "LayerId": "44a002c1-7183-42a7-8d7b-8799b04ca664"
                }
            ]
        },
        {
            "id": "bb90a581-d412-433b-9cdd-42cf14fa815e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "compositeImage": {
                "id": "cb9f25b7-1526-47eb-931a-d13acaa31901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb90a581-d412-433b-9cdd-42cf14fa815e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d3a1208-a273-402f-a1a1-695ba6f97013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb90a581-d412-433b-9cdd-42cf14fa815e",
                    "LayerId": "44a002c1-7183-42a7-8d7b-8799b04ca664"
                }
            ]
        },
        {
            "id": "ad55ca22-7ad7-4787-b02b-dd5797f4122e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "compositeImage": {
                "id": "a971c5b9-ae4a-4f04-a0ce-80a8e1f3f20f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad55ca22-7ad7-4787-b02b-dd5797f4122e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e49f0388-a947-4cc8-9ddd-20aac04d9328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad55ca22-7ad7-4787-b02b-dd5797f4122e",
                    "LayerId": "44a002c1-7183-42a7-8d7b-8799b04ca664"
                }
            ]
        },
        {
            "id": "1b91ea5e-5420-4462-8aa7-859917a35178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "compositeImage": {
                "id": "280f5f8a-9b8c-4b46-b1e7-c41de420ea89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b91ea5e-5420-4462-8aa7-859917a35178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f877edb-0fb1-436c-9366-6a382b02df69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b91ea5e-5420-4462-8aa7-859917a35178",
                    "LayerId": "44a002c1-7183-42a7-8d7b-8799b04ca664"
                }
            ]
        },
        {
            "id": "b31bc62c-074c-48a8-ad50-45276bdac1aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "compositeImage": {
                "id": "499e59d2-0d58-445c-890c-c37fa455082d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b31bc62c-074c-48a8-ad50-45276bdac1aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e686e1b-db3a-4b58-8d7b-90b92ee6693f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31bc62c-074c-48a8-ad50-45276bdac1aa",
                    "LayerId": "44a002c1-7183-42a7-8d7b-8799b04ca664"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "44a002c1-7183-42a7-8d7b-8799b04ca664",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "940eaf93-aba0-4a82-88c3-554327b7ec32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}