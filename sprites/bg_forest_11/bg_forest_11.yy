{
    "id": "1776d56f-e36d-4e73-8c79-47d6973433d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_forest_11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 424,
    "bbox_left": 0,
    "bbox_right": 579,
    "bbox_top": 113,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1fab4e7-c122-4870-95f8-8672f809f0de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1776d56f-e36d-4e73-8c79-47d6973433d2",
            "compositeImage": {
                "id": "54b3b4f3-b1f2-44df-93ee-cc8821ebb63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1fab4e7-c122-4870-95f8-8672f809f0de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c7d79b-bdba-4637-a2ea-9dddcc82989b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1fab4e7-c122-4870-95f8-8672f809f0de",
                    "LayerId": "2fac83f9-d61e-4e47-bbe0-063e08412b87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 425,
    "layers": [
        {
            "id": "2fac83f9-d61e-4e47-bbe0-063e08412b87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1776d56f-e36d-4e73-8c79-47d6973433d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 580,
    "xorig": 0,
    "yorig": 0
}