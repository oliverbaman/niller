{
    "id": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_nextroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 19,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a5812c2-b3d3-4d97-9d0b-6aaf21639d98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "40dcf65a-55b9-4f04-a016-66cf24250955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a5812c2-b3d3-4d97-9d0b-6aaf21639d98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68818382-0390-4d9b-beaa-3918b7a4884e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a5812c2-b3d3-4d97-9d0b-6aaf21639d98",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "82365edc-a1f5-4820-bdcd-236650a343ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "a508e21c-b041-409f-b87a-23d99899aa3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82365edc-a1f5-4820-bdcd-236650a343ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3fd6e4c-c395-4a98-b9d8-971bebbd959d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82365edc-a1f5-4820-bdcd-236650a343ec",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "f929541d-213e-4266-9878-7def1fe22019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "88d72505-42f8-4032-8749-322e7b16bed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f929541d-213e-4266-9878-7def1fe22019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7cdfb99-eefa-4b21-a73f-ea214340941e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f929541d-213e-4266-9878-7def1fe22019",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "9ac78858-437c-42be-89d2-ffd6539c25a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "4df6f51e-c5e9-4b3f-9121-ef6c077f4bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac78858-437c-42be-89d2-ffd6539c25a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fce2218-32bf-41bb-a95e-371a986edb25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac78858-437c-42be-89d2-ffd6539c25a6",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "e5cdbcb2-50e5-4efb-acc6-65f9ba19652e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "d1e7812e-b150-48d0-bf73-1ee5ea6df65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5cdbcb2-50e5-4efb-acc6-65f9ba19652e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf587476-12e8-4607-8454-ce4103ecec38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5cdbcb2-50e5-4efb-acc6-65f9ba19652e",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "bd0bfc1c-3f2e-4e0b-9413-808ed969217e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "491e340a-bbea-4fb7-82de-0a6e93232e9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0bfc1c-3f2e-4e0b-9413-808ed969217e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7623583-19a9-452b-85ae-553599110385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0bfc1c-3f2e-4e0b-9413-808ed969217e",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "a69f635e-648f-4109-a6da-ef70ced68e38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "9ad69af3-6e05-48cd-880e-6582f770d53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a69f635e-648f-4109-a6da-ef70ced68e38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33f0524b-c889-4f78-a0c0-94d0cc451944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a69f635e-648f-4109-a6da-ef70ced68e38",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "8f909711-7266-498f-81a3-5b0afa572308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "070cc778-a510-42d9-9cd1-be2b3ee6a570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f909711-7266-498f-81a3-5b0afa572308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8650aeb6-3fe8-47d4-9084-58b7aabd163b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f909711-7266-498f-81a3-5b0afa572308",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "de5ec8d2-3c57-4ea2-b4ae-d951e912f89b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "ebe47f76-fb47-488a-b387-784d98f24b56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de5ec8d2-3c57-4ea2-b4ae-d951e912f89b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c4f1e17-1a4d-4e8a-878e-db0ea8fe9852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de5ec8d2-3c57-4ea2-b4ae-d951e912f89b",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "fec6d5ac-6d4f-4196-aa84-7f805bc068c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "fec54f28-b4fb-43ad-bf41-1c1082e59c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fec6d5ac-6d4f-4196-aa84-7f805bc068c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f1df0b-4eca-4bfc-8223-7fd4aea7dc74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fec6d5ac-6d4f-4196-aa84-7f805bc068c0",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "e1f2eab0-48f8-42ae-a7e7-59410e4ce992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "688717a3-997b-4802-82a3-6c3927037a3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f2eab0-48f8-42ae-a7e7-59410e4ce992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "229e901a-5605-4a81-a2e4-08c4af771a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f2eab0-48f8-42ae-a7e7-59410e4ce992",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "178cfd48-8087-4a5d-8c32-c16e0c60bd5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "85882831-8ee0-4e0d-aeba-581a42874ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178cfd48-8087-4a5d-8c32-c16e0c60bd5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b797b1c-ebdc-4675-98dc-cd0c85adb7df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178cfd48-8087-4a5d-8c32-c16e0c60bd5b",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "db9726b2-e00f-4d7b-bddf-a47da492d7fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "9a462092-9d8f-4f26-80ba-756f0df24d42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db9726b2-e00f-4d7b-bddf-a47da492d7fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ea9ecf-fddc-408f-b9f4-d7ecc3fd6795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db9726b2-e00f-4d7b-bddf-a47da492d7fb",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "7c055ff4-36a4-4a7c-885b-e705f7c4c356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "8007dce0-70d3-4e24-aa9d-70892f702765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c055ff4-36a4-4a7c-885b-e705f7c4c356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c335a845-ef65-4360-9403-248936ecf6c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c055ff4-36a4-4a7c-885b-e705f7c4c356",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "94f7c2b6-90c7-4190-9bac-67ae6d575463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "2fee7289-7208-4180-b372-a73845e2e2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f7c2b6-90c7-4190-9bac-67ae6d575463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aa9cd27-7629-4e25-b95d-1ccdb11de823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f7c2b6-90c7-4190-9bac-67ae6d575463",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "92f90084-3e7a-49f9-b956-3405adb04ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "0e78704c-7887-496e-9c4e-ae24cedb1a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92f90084-3e7a-49f9-b956-3405adb04ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d5a67b7-12e5-4acf-a530-2ce67189a6ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92f90084-3e7a-49f9-b956-3405adb04ad1",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "322fdb93-0247-4788-8ce5-33086b6b1d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "94af9265-21e1-463f-a576-1328fde635ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "322fdb93-0247-4788-8ce5-33086b6b1d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80251861-17b1-449b-810f-527398572107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "322fdb93-0247-4788-8ce5-33086b6b1d1c",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "e70d6bb7-dd58-48f4-9408-7f005c82da71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "39647547-f8ab-49d3-981e-b12e986f4e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e70d6bb7-dd58-48f4-9408-7f005c82da71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76c2aeaf-d501-48b3-8d2a-a95d89be9b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e70d6bb7-dd58-48f4-9408-7f005c82da71",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        },
        {
            "id": "198c1a82-8de7-452a-ad8c-489d3dbd5535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "compositeImage": {
                "id": "8c1da781-a84a-4d85-9183-0ffa75b0b5d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "198c1a82-8de7-452a-ad8c-489d3dbd5535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "542107c2-4dba-43c4-962e-13ec1a53376a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "198c1a82-8de7-452a-ad8c-489d3dbd5535",
                    "LayerId": "3396a919-2259-43d7-96ce-dc09dcc72fe6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "3396a919-2259-43d7-96ce-dc09dcc72fe6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}