{
    "id": "a7bcb1d9-3260-4ccf-8ec7-7615fe0ade33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_forest_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 424,
    "bbox_left": 0,
    "bbox_right": 579,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d092bd8b-13de-4246-8900-0dfbb5319bea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7bcb1d9-3260-4ccf-8ec7-7615fe0ade33",
            "compositeImage": {
                "id": "391e7e2d-b8ae-497c-891a-fd7c08635617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d092bd8b-13de-4246-8900-0dfbb5319bea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f6d24c-22b6-4a21-ad9d-28fb2bd88c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d092bd8b-13de-4246-8900-0dfbb5319bea",
                    "LayerId": "793285af-c8e2-47b5-a688-dda2e1fc7501"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 425,
    "layers": [
        {
            "id": "793285af-c8e2-47b5-a688-dda2e1fc7501",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7bcb1d9-3260-4ccf-8ec7-7615fe0ade33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 580,
    "xorig": 0,
    "yorig": 0
}