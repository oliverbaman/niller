{
    "id": "e3d0e5c2-a148-4656-abf6-dc73ab136ef1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lg_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc13035b-c475-4a60-8943-998d0fcfa7c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3d0e5c2-a148-4656-abf6-dc73ab136ef1",
            "compositeImage": {
                "id": "ea5fcd08-4583-40f6-978a-0f3b188916be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc13035b-c475-4a60-8943-998d0fcfa7c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e5cc53-f2ae-4335-89ca-af3b3a0f509b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc13035b-c475-4a60-8943-998d0fcfa7c9",
                    "LayerId": "97836947-8c7c-47cf-85e0-134bcece3c9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "97836947-8c7c-47cf-85e0-134bcece3c9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3d0e5c2-a148-4656-abf6-dc73ab136ef1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}