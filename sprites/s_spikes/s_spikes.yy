{
    "id": "af1a5cab-3db1-40fa-8280-a3f5dc9a69a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_spikes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dacbc8b9-e45c-434b-84a9-36ec0f514a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af1a5cab-3db1-40fa-8280-a3f5dc9a69a9",
            "compositeImage": {
                "id": "8672a9d8-7852-4efa-8861-ed3b1723dbd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dacbc8b9-e45c-434b-84a9-36ec0f514a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68abf83c-f6d6-4d2d-88af-5fc1b41c6beb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dacbc8b9-e45c-434b-84a9-36ec0f514a6f",
                    "LayerId": "f9cbb464-5334-4555-bb50-47eb05515b00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f9cbb464-5334-4555-bb50-47eb05515b00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af1a5cab-3db1-40fa-8280-a3f5dc9a69a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}