{
    "id": "b66df675-2d7f-4f97-8178-340d01855d83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bridge_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cfc44ea-9b6f-45ef-9812-62b033556524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b66df675-2d7f-4f97-8178-340d01855d83",
            "compositeImage": {
                "id": "8b17a402-c3b0-4300-ac62-015aab189699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cfc44ea-9b6f-45ef-9812-62b033556524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eafb042a-a569-4f2f-a502-7f01df62676b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cfc44ea-9b6f-45ef-9812-62b033556524",
                    "LayerId": "bead1766-4865-4746-b5d3-05b2db52fbc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "bead1766-4865-4746-b5d3-05b2db52fbc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b66df675-2d7f-4f97-8178-340d01855d83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}