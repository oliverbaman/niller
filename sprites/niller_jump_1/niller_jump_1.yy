{
    "id": "00930ac4-1131-46da-a6a1-d8b85f115af7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_jump_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bee500b-af7d-4709-999b-d219163826a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00930ac4-1131-46da-a6a1-d8b85f115af7",
            "compositeImage": {
                "id": "3a5575eb-b413-445f-912d-1afc0a653643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bee500b-af7d-4709-999b-d219163826a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d71c4f-56ca-4567-80dc-037e8d97954a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bee500b-af7d-4709-999b-d219163826a6",
                    "LayerId": "ec487640-4cd6-4ceb-8032-302422bd67d1"
                }
            ]
        },
        {
            "id": "96868c44-c1a6-4015-b06e-b261899da290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00930ac4-1131-46da-a6a1-d8b85f115af7",
            "compositeImage": {
                "id": "db559b74-01e9-4587-a195-f480a63d9e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96868c44-c1a6-4015-b06e-b261899da290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9d7a61-1339-44dc-8915-4517ddd9b512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96868c44-c1a6-4015-b06e-b261899da290",
                    "LayerId": "ec487640-4cd6-4ceb-8032-302422bd67d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "ec487640-4cd6-4ceb-8032-302422bd67d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00930ac4-1131-46da-a6a1-d8b85f115af7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 5,
    "yorig": 8
}