{
    "id": "06a20dac-e878-4f55-86a2-face071eae52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "niller_run_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "398bdc3f-1a6f-4b11-974b-048705ce2767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "393c7e7f-bf23-49e1-a9f5-36a1dc6030e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "398bdc3f-1a6f-4b11-974b-048705ce2767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87fea23c-2ac5-49e2-ae1d-658c3eafb98d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "398bdc3f-1a6f-4b11-974b-048705ce2767",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "a08b9d20-a399-41b0-a068-0daa67c08213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "0f996e40-bd6f-41c0-b939-289007c272b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a08b9d20-a399-41b0-a068-0daa67c08213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f559c87e-fff4-4015-b4ee-1b38e681a0bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a08b9d20-a399-41b0-a068-0daa67c08213",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "fdfb3d79-0bd5-400b-a94c-2682e39583d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "adbf7cfb-fbc3-4200-b824-3f0688232706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdfb3d79-0bd5-400b-a94c-2682e39583d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668e5916-f58f-4504-8df0-baecfd3b7dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdfb3d79-0bd5-400b-a94c-2682e39583d1",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "26f9738c-3334-4564-abf0-bde14cf58bd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "359aeee1-a4cc-43a2-8d3e-021fce7d9d07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f9738c-3334-4564-abf0-bde14cf58bd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98a2ced8-3c2e-4b59-bb9e-eb8086ce6ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f9738c-3334-4564-abf0-bde14cf58bd5",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "338329f6-1fe0-43d9-ae47-8daa4b111de4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "9d186338-2b10-435a-8029-8799f65683f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338329f6-1fe0-43d9-ae47-8daa4b111de4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc48d350-b871-470d-8f63-e970e59d9ad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338329f6-1fe0-43d9-ae47-8daa4b111de4",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "c04f8422-d232-4fa0-aa23-a2e8318e75f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "f9fe6308-c871-4842-8e77-081595e8024c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c04f8422-d232-4fa0-aa23-a2e8318e75f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf15437-abac-4d91-b777-3bb78f7a6560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c04f8422-d232-4fa0-aa23-a2e8318e75f7",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "710683e2-8b7f-4871-ae27-90cd3c9b2bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "f063c541-d82e-43a3-896c-50c562ccd7ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710683e2-8b7f-4871-ae27-90cd3c9b2bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033314ec-9b54-48d5-95e1-a057cb12d6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710683e2-8b7f-4871-ae27-90cd3c9b2bd0",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "9fa580cd-e9cd-44d2-8709-e0ef49fa93d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "b9d451a9-58a0-4a67-a251-b082cd305c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa580cd-e9cd-44d2-8709-e0ef49fa93d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c420681f-ce97-4a9e-a833-d862e89f3c41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa580cd-e9cd-44d2-8709-e0ef49fa93d9",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        },
        {
            "id": "8513455e-2211-4453-bed1-73413f78f1e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "compositeImage": {
                "id": "08a97fb7-32e2-44d0-9777-1f906fbd13ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8513455e-2211-4453-bed1-73413f78f1e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3fb968e-d98e-4f5e-85f8-f6abd00fa876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8513455e-2211-4453-bed1-73413f78f1e8",
                    "LayerId": "64e5d32c-f26f-4426-a323-e9e98d83e048"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "64e5d32c-f26f-4426-a323-e9e98d83e048",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06a20dac-e878-4f55-86a2-face071eae52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}