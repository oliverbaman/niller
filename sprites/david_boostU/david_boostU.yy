{
    "id": "658d5049-3a21-4085-aa71-f594aedeb13f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "david_boostU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2ad54fc-bfcc-4a46-a538-d0a26f004086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658d5049-3a21-4085-aa71-f594aedeb13f",
            "compositeImage": {
                "id": "6e78a9d8-47c4-4d81-963e-ea9348eeccf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2ad54fc-bfcc-4a46-a538-d0a26f004086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a54c3d-15dd-4ba2-998c-78c11fb4806c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2ad54fc-bfcc-4a46-a538-d0a26f004086",
                    "LayerId": "f4e3f4b6-fec9-4368-91bd-658ac631b183"
                }
            ]
        },
        {
            "id": "3e9d6be4-7b03-4445-9195-17fa13ec6b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658d5049-3a21-4085-aa71-f594aedeb13f",
            "compositeImage": {
                "id": "63373178-fe1f-49d9-b732-9f7053ee60de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e9d6be4-7b03-4445-9195-17fa13ec6b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef52d1d5-5a6c-4c0e-bf02-3a83014e790b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e9d6be4-7b03-4445-9195-17fa13ec6b93",
                    "LayerId": "f4e3f4b6-fec9-4368-91bd-658ac631b183"
                }
            ]
        },
        {
            "id": "2edcebb7-d67d-4c9c-8cbd-48b280668a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658d5049-3a21-4085-aa71-f594aedeb13f",
            "compositeImage": {
                "id": "67f72afb-1b47-4895-969f-e8c29df6ae60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2edcebb7-d67d-4c9c-8cbd-48b280668a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d55e0f-a4a1-4330-a07d-daa2c2f6e5b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2edcebb7-d67d-4c9c-8cbd-48b280668a82",
                    "LayerId": "f4e3f4b6-fec9-4368-91bd-658ac631b183"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "f4e3f4b6-fec9-4368-91bd-658ac631b183",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "658d5049-3a21-4085-aa71-f594aedeb13f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 8,
    "yorig": 8
}