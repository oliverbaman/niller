{
    "id": "b512e3db-1b96-45f0-8e71-431c00b11474",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_spawnpoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7da00623-a46f-4324-9a39-5fdec3cfb791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b512e3db-1b96-45f0-8e71-431c00b11474",
            "compositeImage": {
                "id": "c1c0615f-a5d8-4566-b3b9-9584d9b553fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da00623-a46f-4324-9a39-5fdec3cfb791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c83b17-1981-4199-9cbf-6474d38feaf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da00623-a46f-4324-9a39-5fdec3cfb791",
                    "LayerId": "d7ea0542-958c-4005-9f0c-dcde60f3c64e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d7ea0542-958c-4005-9f0c-dcde60f3c64e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b512e3db-1b96-45f0-8e71-431c00b11474",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}