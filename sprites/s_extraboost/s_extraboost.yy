{
    "id": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_extraboost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e3ac8e6-2bb7-4789-9bba-c07e115bf2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "compositeImage": {
                "id": "6cccce72-b5cd-4a05-9267-88cd98804d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e3ac8e6-2bb7-4789-9bba-c07e115bf2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da59f847-ca33-4a23-b00e-f5820169bb3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3ac8e6-2bb7-4789-9bba-c07e115bf2c1",
                    "LayerId": "c3673127-678c-490b-961b-390ef5ea19a9"
                },
                {
                    "id": "d096ef90-a0aa-42b8-a2ba-db06ea8c1f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3ac8e6-2bb7-4789-9bba-c07e115bf2c1",
                    "LayerId": "d616b18f-8bc9-4cd4-a182-0ab71f6de52b"
                },
                {
                    "id": "7ddd4e07-b8c7-4638-a886-2c5b27a0a36a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3ac8e6-2bb7-4789-9bba-c07e115bf2c1",
                    "LayerId": "8d541d45-2ba6-45c7-98b2-fd1a98e94b48"
                }
            ]
        },
        {
            "id": "895a791c-dba9-4ac7-a325-7a58d48518de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "compositeImage": {
                "id": "5fe8a9df-5cfc-4360-a2c0-bcb1f2676d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895a791c-dba9-4ac7-a325-7a58d48518de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2753409c-1e0b-427f-894d-3ead3eb66358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895a791c-dba9-4ac7-a325-7a58d48518de",
                    "LayerId": "c3673127-678c-490b-961b-390ef5ea19a9"
                },
                {
                    "id": "1a688f73-93f5-430b-9f45-26ad7a2fa79d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895a791c-dba9-4ac7-a325-7a58d48518de",
                    "LayerId": "d616b18f-8bc9-4cd4-a182-0ab71f6de52b"
                },
                {
                    "id": "bd8a746a-5791-4f0c-9aeb-3119ab7509a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895a791c-dba9-4ac7-a325-7a58d48518de",
                    "LayerId": "8d541d45-2ba6-45c7-98b2-fd1a98e94b48"
                }
            ]
        },
        {
            "id": "bd28f5eb-0974-4652-91b1-51cf92503ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "compositeImage": {
                "id": "d1f32ec5-18af-426d-97e2-de817ed060e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd28f5eb-0974-4652-91b1-51cf92503ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ebf0b8-7054-4949-ae01-44ded5120406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd28f5eb-0974-4652-91b1-51cf92503ad9",
                    "LayerId": "c3673127-678c-490b-961b-390ef5ea19a9"
                },
                {
                    "id": "e6903692-dc7e-4064-a413-88c0929fd152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd28f5eb-0974-4652-91b1-51cf92503ad9",
                    "LayerId": "d616b18f-8bc9-4cd4-a182-0ab71f6de52b"
                },
                {
                    "id": "f2ab5779-9e4b-4f24-bab1-fce134edd9e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd28f5eb-0974-4652-91b1-51cf92503ad9",
                    "LayerId": "8d541d45-2ba6-45c7-98b2-fd1a98e94b48"
                }
            ]
        },
        {
            "id": "d12d12c0-1a3b-4f62-8607-323b80e952dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "compositeImage": {
                "id": "9ec416b1-45bf-4ca5-8b97-49e0005518e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d12d12c0-1a3b-4f62-8607-323b80e952dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa66105e-41b8-4e20-bbd7-26d9100ed0b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12d12c0-1a3b-4f62-8607-323b80e952dc",
                    "LayerId": "c3673127-678c-490b-961b-390ef5ea19a9"
                },
                {
                    "id": "e5991a66-4c16-4225-a60c-4b279ce2196a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12d12c0-1a3b-4f62-8607-323b80e952dc",
                    "LayerId": "d616b18f-8bc9-4cd4-a182-0ab71f6de52b"
                },
                {
                    "id": "c6e135e5-c847-4294-9b48-a3570f3fa7a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12d12c0-1a3b-4f62-8607-323b80e952dc",
                    "LayerId": "8d541d45-2ba6-45c7-98b2-fd1a98e94b48"
                }
            ]
        },
        {
            "id": "a96811f6-8ac2-4b62-a0e7-66c822d40d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "compositeImage": {
                "id": "b1984952-8fba-4840-b3b0-e90398393e3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a96811f6-8ac2-4b62-a0e7-66c822d40d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4427a1cb-3b26-4f01-ae6f-7097ab71ce50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a96811f6-8ac2-4b62-a0e7-66c822d40d99",
                    "LayerId": "c3673127-678c-490b-961b-390ef5ea19a9"
                },
                {
                    "id": "b3ba998d-f798-4dfd-8483-05fb8bffc577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a96811f6-8ac2-4b62-a0e7-66c822d40d99",
                    "LayerId": "d616b18f-8bc9-4cd4-a182-0ab71f6de52b"
                },
                {
                    "id": "d9a2a1ca-5e97-4f8f-b9e9-14518c7387b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a96811f6-8ac2-4b62-a0e7-66c822d40d99",
                    "LayerId": "8d541d45-2ba6-45c7-98b2-fd1a98e94b48"
                }
            ]
        },
        {
            "id": "d69c0507-ac19-44d1-8202-df3edd25ca2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "compositeImage": {
                "id": "2c08a5bb-d9e8-498a-a570-4626c3eddfad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d69c0507-ac19-44d1-8202-df3edd25ca2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b185ad2c-ea4b-4dc1-a9ef-a91729440628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d69c0507-ac19-44d1-8202-df3edd25ca2e",
                    "LayerId": "c3673127-678c-490b-961b-390ef5ea19a9"
                },
                {
                    "id": "3e5f43c9-bdd4-4ebf-9652-e3acdb650cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d69c0507-ac19-44d1-8202-df3edd25ca2e",
                    "LayerId": "d616b18f-8bc9-4cd4-a182-0ab71f6de52b"
                },
                {
                    "id": "6294428c-5dd6-49a8-a345-4bf2efd2544f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d69c0507-ac19-44d1-8202-df3edd25ca2e",
                    "LayerId": "8d541d45-2ba6-45c7-98b2-fd1a98e94b48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "8d541d45-2ba6-45c7-98b2-fd1a98e94b48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 55,
            "visible": false
        },
        {
            "id": "d616b18f-8bc9-4cd4-a182-0ab71f6de52b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 54,
            "visible": false
        },
        {
            "id": "c3673127-678c-490b-961b-390ef5ea19a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d4170be-cbcd-46e7-8ce0-a01e8e27f736",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}