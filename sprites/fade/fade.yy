{
    "id": "a362655e-d1a2-4cdc-84af-7d79492cc352",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fac980b8-3f03-4a09-8b96-ae00fea15b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a362655e-d1a2-4cdc-84af-7d79492cc352",
            "compositeImage": {
                "id": "2f109b25-94c2-42be-8aa4-d5ba821205b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fac980b8-3f03-4a09-8b96-ae00fea15b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f65ddf5b-f92d-413f-9444-de3b62506650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fac980b8-3f03-4a09-8b96-ae00fea15b72",
                    "LayerId": "e835b8d1-2b40-485c-ad6b-77f4e07ed703"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e835b8d1-2b40-485c-ad6b-77f4e07ed703",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a362655e-d1a2-4cdc-84af-7d79492cc352",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}