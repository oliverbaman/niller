{
    "id": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird_flying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1e1ce41-c95b-44e2-b2c7-4e4f2b633dc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "7bda0fb7-81b9-4465-b3aa-619141b2edc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e1ce41-c95b-44e2-b2c7-4e4f2b633dc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cb6715d-4a54-4326-ba68-ab614eee96d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e1ce41-c95b-44e2-b2c7-4e4f2b633dc9",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "7926c2e8-60e9-4e20-9eee-3836b2af9e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "954b0313-e7e3-4c47-aaec-b6b27c06eddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7926c2e8-60e9-4e20-9eee-3836b2af9e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37cf4ab7-fc4b-4029-b254-b3bd068d6d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7926c2e8-60e9-4e20-9eee-3836b2af9e8a",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "b61dae69-fe21-4bcb-b016-2e69b07e9029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "9d75b31a-2cc0-48c9-bdef-fae0d3972e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61dae69-fe21-4bcb-b016-2e69b07e9029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6a33e53-81a5-4440-96d9-4ef64da96e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61dae69-fe21-4bcb-b016-2e69b07e9029",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "5f952efe-c56a-42df-9219-55960e0947fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "9a260738-0d93-4916-8c02-f291cf872138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f952efe-c56a-42df-9219-55960e0947fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "046d247d-99e3-4796-8a90-e47b7ee85090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f952efe-c56a-42df-9219-55960e0947fa",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "097a1612-d0cd-4d36-a40c-e9934f05935a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "69933f8a-a23d-4fda-9c49-ac17bd315d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "097a1612-d0cd-4d36-a40c-e9934f05935a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1007681-4afa-40d5-8e6f-e03328c1c41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097a1612-d0cd-4d36-a40c-e9934f05935a",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "5dd10bdd-b4e0-41cb-9721-2cb90892a153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "fcc3a0e5-7355-43f4-a817-84001f5c2027",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd10bdd-b4e0-41cb-9721-2cb90892a153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6594fa43-6aca-4310-a110-c08214bb2c6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd10bdd-b4e0-41cb-9721-2cb90892a153",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "38e63290-341a-442d-b6d0-94755652d18d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "969063e7-5afd-4e0d-bf71-7239fa27acc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38e63290-341a-442d-b6d0-94755652d18d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e0be3c1-edbf-4c48-be64-c9877405f2f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38e63290-341a-442d-b6d0-94755652d18d",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "0c51994c-a8dc-4653-85c0-4649bd438f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "c5634b1b-d8e3-4f10-8cf2-7a3ab663223f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c51994c-a8dc-4653-85c0-4649bd438f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135a0eb5-4ddb-4170-ad7f-afd745fa1566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c51994c-a8dc-4653-85c0-4649bd438f6e",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        },
        {
            "id": "29457954-2de3-435c-84ba-082996debc9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "compositeImage": {
                "id": "93424d23-36aa-40b0-9de3-f6977fb4cb0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29457954-2de3-435c-84ba-082996debc9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c869c88-698e-429e-bcf1-3a9938fa1ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29457954-2de3-435c-84ba-082996debc9a",
                    "LayerId": "bfb94929-cd89-47e3-9dde-84d8ed826fa2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "bfb94929-cd89-47e3-9dde-84d8ed826fa2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d35f5cc7-72f0-4479-bf0b-7760c671b97f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}