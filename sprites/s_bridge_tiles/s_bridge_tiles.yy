{
    "id": "2f4b4c7c-78d6-4357-b377-b53307cd5548",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bridge_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 279,
    "bbox_left": 4,
    "bbox_right": 175,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0feba372-fe31-48ae-8334-d72e75b854ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f4b4c7c-78d6-4357-b377-b53307cd5548",
            "compositeImage": {
                "id": "362a4ab4-825d-4ce7-9804-7b55924f712a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0feba372-fe31-48ae-8334-d72e75b854ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dfd572a-7b55-4796-be60-ddf9548d1075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0feba372-fe31-48ae-8334-d72e75b854ac",
                    "LayerId": "153a6cd0-97da-4aa4-b20e-c24662099c71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 290,
    "layers": [
        {
            "id": "153a6cd0-97da-4aa4-b20e-c24662099c71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f4b4c7c-78d6-4357-b377-b53307cd5548",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 176,
    "xorig": 0,
    "yorig": 0
}