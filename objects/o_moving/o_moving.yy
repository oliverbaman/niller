{
    "id": "8634000e-c8c7-4cbb-b8b8-b300ce587817",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_moving",
    "eventList": [
        {
            "id": "2469a4da-11e5-45b8-b6e3-119195f3306f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8634000e-c8c7-4cbb-b8b8-b300ce587817"
        },
        {
            "id": "16228d07-678f-46a4-bfa0-8e7cc29fa4ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8634000e-c8c7-4cbb-b8b8-b300ce587817"
        }
    ],
    "maskSpriteId": "853748aa-e653-43ef-a7bc-f37ea6c7cf23",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "853748aa-e653-43ef-a7bc-f37ea6c7cf23",
    "visible": true
}