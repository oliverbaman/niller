{
    "id": "711a7943-76e2-443d-917e-d68aed0a19f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_Final_score",
    "eventList": [
        {
            "id": "fa72a1ef-156a-442d-aa18-01062a14770d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "711a7943-76e2-443d-917e-d68aed0a19f1"
        },
        {
            "id": "38d9d6c0-725d-4c7d-8a09-1ca7b39e6f91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "711a7943-76e2-443d-917e-d68aed0a19f1"
        },
        {
            "id": "47b37c72-ed90-4f99-b0fc-de442724068b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "711a7943-76e2-443d-917e-d68aed0a19f1"
        },
        {
            "id": "9e827b6a-4368-4a3e-82fb-95cf7cc992b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "711a7943-76e2-443d-917e-d68aed0a19f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}