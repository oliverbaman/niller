//////draw_text(x,y,"Tuborgs collected: " + string(global.tuborgs))

////draw_set_font()
////draw_text_transformed(x+10,y,"Tuborgs collected: " + string(global.tuborgs),0.2,0.2,0)
draw_set_font(f_menu2)
draw_set_halign(fa_left)
draw_text_transformed(x,y,"Tuborgs: " + string(global.tuborgs) + "/" + string(global.tuborgs_total),0.2,0.2,0)
draw_text_transformed(x,y+15,"Time: " + string(global.show_time_m) + "m " + string(global.show_time_s) + "s",0.2,0.2,0)
draw_text_transformed(x,y+30,"Deaths: " + string(global.nr_of_deaths),0.2,0.2,0)
