{
    "id": "529370a3-cd75-44e6-a111-90b1030fa9cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "fallande_tom",
    "eventList": [
        {
            "id": "6ddfd3aa-b894-474d-80ec-9b0df1696854",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "529370a3-cd75-44e6-a111-90b1030fa9cb"
        },
        {
            "id": "d0509f20-cb18-46b3-90af-49f93bb5a688",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "529370a3-cd75-44e6-a111-90b1030fa9cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9b14349-79d5-46ab-8b76-3ff37dfe9134",
    "visible": false
}