{
    "id": "a7532f57-b6b2-4aa9-a07e-cefdb7dc0e3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "checkpoint_cover",
    "eventList": [
        {
            "id": "df831ac5-d183-492d-9fc8-7e9b9b160a58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7532f57-b6b2-4aa9-a07e-cefdb7dc0e3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f4024d5-285f-43e6-9264-27da408ec01e",
    "visible": false
}