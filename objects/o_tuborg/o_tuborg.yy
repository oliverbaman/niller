{
    "id": "63899de4-d8bb-46d5-ab25-1eb37d470a76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tuborg",
    "eventList": [
        {
            "id": "080ccf04-aa96-4af2-9dc0-b651ee6b0c5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "63899de4-d8bb-46d5-ab25-1eb37d470a76"
        },
        {
            "id": "bf8b93fd-d61c-4972-8a96-ada537c59ada",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63899de4-d8bb-46d5-ab25-1eb37d470a76"
        },
        {
            "id": "8f2d780e-e3e4-4794-a788-cba4bb8e34a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "63899de4-d8bb-46d5-ab25-1eb37d470a76"
        }
    ],
    "maskSpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb08c011-c618-4022-a2ec-3bceb1d4b7b4",
    "visible": true
}