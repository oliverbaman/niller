if instance_exists(o_niller)
{
	
	if follow = 1
	{
		var spd = (point_distance(x, y, o_player.x, o_player.y) / 5)
		move_towards_point(o_player.x, o_player.y, spd)

		if spd < 0.05 and o_player.on_floor
		{
			global.tuborgs += 1
			instance_destroy()
		} 
		
	}
	else
	{
		x = origin_x
		y = origin_y
		follow = 0
	}
}
else
	{
		x = origin_x
		y = origin_y
		follow = 0
	}
