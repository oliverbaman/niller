{
    "id": "45c4aefe-d453-4b0e-97f5-9be35432e396",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_boost_niller",
    "eventList": [
        {
            "id": "1c51cd0f-9998-45bc-9cc6-59f07c9459db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45c4aefe-d453-4b0e-97f5-9be35432e396"
        },
        {
            "id": "ba0ca1f3-3cfe-4c3c-93b6-4cb7a7780bd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45c4aefe-d453-4b0e-97f5-9be35432e396"
        },
        {
            "id": "1bbb0f80-0d1e-4ede-a443-768c755658e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "45c4aefe-d453-4b0e-97f5-9be35432e396"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}