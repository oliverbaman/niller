cam = view_camera[0]

view_width = camera_get_view_width(cam)
view_height = camera_get_view_height(cam)

view_w_half = camera_get_view_width(cam) * 0.5
view_h_half = camera_get_view_height(cam) * 0.5

camera_move_speed = 0.2

