
if instance_exists(o_player)
{
	var current_levelsize = instance_position(o_player.x, o_player.y, o_levelsize)

	if current_levelsize
	{
		var camera_destination_x = o_player.x - view_w_half
		var camera_destination_y = o_player.y - view_h_half
		
		if camera_destination_x < current_levelsize.bbox_left
		{
			camera_destination_x = current_levelsize.bbox_left
		}
		if (camera_destination_x + view_width) > current_levelsize.bbox_right
		{
			camera_destination_x = current_levelsize.bbox_right - view_width
		}
		if camera_destination_y < current_levelsize.bbox_top
		{
			camera_destination_y = current_levelsize.bbox_top
		}
		if camera_destination_y + view_height > current_levelsize.bbox_bottom
		{
			camera_destination_y = current_levelsize.bbox_bottom - view_height
		}
		
		var delta_x = camera_destination_x - x
		var delta_y = camera_destination_y - y
		
		x += delta_x * 0.2
		y += delta_y * 0.2
		
		camera_set_view_pos(cam, x, y)	
	}
	
}


//var xTo, yTo
//if o_player.died = 1
//{
//	follow = o_death_animation
//}

//if (instance_exists(o_niller))
//{
//	xTo = (o_niller.x div view_wport[0]) * view_wport[0];
//	yTo = (o_niller.y div view_hport[0]) * view_hport[0];
	
//	var difx, dify
//	difx = (xTo - x)
//	dify = (yTo - y)
//	if (abs(difx) < 1) x = xTo; else x += (difx/15)
//	if (abs(dify) < 1) x = yTo; else y += (dify/15)
//}
//else 
//{
//	xTo = (o_death_animation.x div (view_wport[0])* (view_wport[0]));
//	yTo = (o_death_animation.y div (view_hport[0])* (view_hport[0]));
//}


//camera_set_view_pos(view_camera[0],x,y)





//xTo = follow.x
//yTo = follow.y
	
//x += (xTo - x) / 5
//y += (yTo - y) / 5

//x = clamp(x,0,level_size_x[0])
//y = clamp(y,view_h_half,room_height-view_h_half)

////// BOOST SHAKE \\\\\\\\\\\

if instance_exists(o_player)
{
var shake = 2

if o_player.in_boost
{
x += random_range(-shake, shake)
y += random_range(-shake, shake)

}
}

//camera_set_view_pos(cam,x-view_w_half,y-view_h_half)


//if (layer_exists("Background"))
//{
//	layer_x("Background",x)
//}
//if (layer_exists("bg1"))
//{
//	layer_x("bg1",x/6)
//	layer_y("bg1",y/6)
//	if room == r_forest2
//	{
//		layer_y("bg1",y/6 + 200)
//	}
//}

//if (layer_exists("bg2"))
//{
//	layer_x("bg2",x/5)
//	layer_y("bg2",y/5)
//	if room == r_forest2
//	{
//		layer_y("bg2",y/5 + 200)
//	}
//}

//if (layer_exists("bg3"))
//{
//	layer_x("bg3",x/4)
//	layer_y("bg3",y/4)
//	if room == r_forest2
//	{
//		layer_y("bg2",y/4 + 200)
//	}
//}

//if (layer_exists("bg4"))
//{
//	layer_x("bg4",x/2)
//	layer_y("bg4",y/2)
//	if room == r_forest2
//	{
//		layer_y("bg2",y/2 + 200)
//	}
//}
//if (layer_exists("bg5"))
//{
//	layer_x("bg5",x/1.5)
//	layer_y("bg5",y/1.5)
//	if room == r_forest2
//	{
//		layer_y("bg2",y/1.5 + 200)
//	}
//}



