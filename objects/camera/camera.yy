{
    "id": "1c30b4a6-1cd0-4f53-b74b-ede029769b7b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "camera",
    "eventList": [
        {
            "id": "bd141998-0762-4aa2-b973-31ca74c5a929",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c30b4a6-1cd0-4f53-b74b-ede029769b7b"
        },
        {
            "id": "25f5cf3c-b5ba-4f00-b2b5-b67c4cc2c626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c30b4a6-1cd0-4f53-b74b-ede029769b7b"
        },
        {
            "id": "24b1d92b-9fc2-4a87-ae49-6887bbe8355d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1c30b4a6-1cd0-4f53-b74b-ede029769b7b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "260588a0-a766-4790-9728-836aa039c889",
    "visible": false
}