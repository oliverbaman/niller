{
    "id": "2a701aa7-1286-4dd4-be30-eb36276f509b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_studs",
    "eventList": [
        {
            "id": "4cf2d0f8-6c1f-4871-b77f-d0dbbe5ec5e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a701aa7-1286-4dd4-be30-eb36276f509b"
        },
        {
            "id": "aa466c1f-56a2-4546-8cd7-7cb6a44fad7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a701aa7-1286-4dd4-be30-eb36276f509b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d85f1660-0347-437d-b8dd-86996c19bea2",
    "visible": true
}