{
    "id": "9234921f-cc79-4034-ba74-10966ba43556",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_blob_green",
    "eventList": [
        {
            "id": "b648de7f-4310-489e-9096-b145a58dc06a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9234921f-cc79-4034-ba74-10966ba43556"
        },
        {
            "id": "88a57ff0-039e-4194-89c5-f88adf27db57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9234921f-cc79-4034-ba74-10966ba43556"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "acd5bd56-3c36-4ed5-8a59-71a865cfe26a",
    "visible": true
}