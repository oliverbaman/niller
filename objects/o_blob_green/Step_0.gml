//ON RIGHT WALL
if placing = 1
{
	vsp = walksp * v_dir
	if place_meeting(x, y+vsp, o_solid) or !position_meeting(x+sprite_width,y-1,o_solid) or !position_meeting(x+sprite_width,y+1+sprite_height,o_solid)
	{
		
		if timer < timer_max
		{
			vsp = 0
			timer = timer + 1
		}
		else
		{
			v_dir = v_dir * -1
			vsp = walksp * v_dir
			timer = 0
		}
	}
}
//ON LEFT WALL
if placing = 2
{
	vsp = walksp * v_dir
	if place_meeting(x, y+vsp, o_solid) or !position_meeting(x-10,y-1,o_solid) or !position_meeting(x-10,y+sprite_height,o_solid)
	{
		if timer < timer_max
		{
			vsp = 0
			timer = timer + 1
		}
		else
		{
			v_dir = v_dir * -1
			vsp = walksp * v_dir
			timer = 0
		}
	}
}

//ON GROUND
if placing = 3
{
	hsp = walksp * h_dir
	if place_meeting(x+hsp, y, o_solid) or !position_meeting(x-18,y+10,o_solid) or !position_meeting(x+1,y+10,o_solid)
	{
		if timer < timer_max
		{
			hsp = 0
			timer = timer + 1
		}
		else
		{
			h_dir = h_dir * -1
			hsp = walksp * h_dir
			timer = 0
		}
	}
	
}

x = x + hsp

y = y + vsp

if vsp < 0
{
	image_speed = 0
	image_index = 2
}
else if vsp > 0
{
	image_speed = 0
	image_index = 0
}
else if vsp = 0
{
	image_speed = 1
}

if hsp < 0
{
	image_speed = 0
	image_index = 2
}
else if hsp > 0
{
	image_speed = 0
	image_index = 0
}
else if hsp = 0
{
	image_speed = 1
}

