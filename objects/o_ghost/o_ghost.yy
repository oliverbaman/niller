{
    "id": "240ca00f-5c96-4319-b342-6461acec44b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ghost",
    "eventList": [
        {
            "id": "7ee0fc5e-bee6-4e7e-bc44-48e86d8b2901",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "240ca00f-5c96-4319-b342-6461acec44b0"
        },
        {
            "id": "076e37d3-9050-40b1-9d18-ecccfb0873fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "240ca00f-5c96-4319-b342-6461acec44b0"
        },
        {
            "id": "2fe02a31-7f96-489b-966f-bddcf9bd66b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2d04d2cf-eb2f-41aa-a60c-503a4dd9b46c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "240ca00f-5c96-4319-b342-6461acec44b0"
        },
        {
            "id": "b074905f-8580-4cc9-9033-872c256f8edb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "27446cac-3fda-43cf-ae00-11ab3feefda2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "240ca00f-5c96-4319-b342-6461acec44b0"
        },
        {
            "id": "8c417695-9f3a-4599-af5c-f153f7ac72e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c346badf-70ff-4120-8e4f-5967f7a11b78",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "240ca00f-5c96-4319-b342-6461acec44b0"
        },
        {
            "id": "d97ae423-43c5-4438-b19e-c75bcd7f574f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0df9b5be-ad2f-4203-bbf8-5c2eff73f21d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "240ca00f-5c96-4319-b342-6461acec44b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "3f0f52c8-7e6e-4e26-a378-93f212a55547",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "dir_x",
            "varType": 0
        },
        {
            "id": "606462d6-7565-44dc-943f-51a47cc861e6",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "dir_y",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "c71ea742-73f2-41f4-8607-26e5cf5ade68",
    "visible": true
}