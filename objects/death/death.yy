{
    "id": "c431ce85-213e-47df-ab76-05e372604f38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "death",
    "eventList": [
        {
            "id": "8fccc48a-fff9-4ab3-8248-ee1b30c662c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c431ce85-213e-47df-ab76-05e372604f38"
        },
        {
            "id": "62dab6fe-2692-4ac9-8bd7-366a5c09b0d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c431ce85-213e-47df-ab76-05e372604f38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45649bcc-b962-4d7a-9a59-970d99de0c2f",
    "visible": false
}