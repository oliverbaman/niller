{
    "id": "354331ec-6228-4170-b5a4-de59fd8c8da4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "checkpoint",
    "eventList": [
        {
            "id": "09cb286e-abe4-4d61-83e6-2a02454564d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "354331ec-6228-4170-b5a4-de59fd8c8da4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ea7b6f4c-48e7-4b61-9610-7978456309de",
    "visible": false
}