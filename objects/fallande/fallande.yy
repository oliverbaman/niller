{
    "id": "98f3094d-08ab-486e-b33b-d59c27bc30d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "fallande",
    "eventList": [
        {
            "id": "631fa638-d084-4179-bafe-018d8c85f7a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "98f3094d-08ab-486e-b33b-d59c27bc30d1"
        },
        {
            "id": "775ad395-6fc3-467f-99ed-a10a3ac20f47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "98f3094d-08ab-486e-b33b-d59c27bc30d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5a742c92-82d5-469d-820d-ebcf0ca3c2c1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "bee8fc6b-c715-448a-b914-d50fc2959ab4",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "hsp",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "431811cb-3945-49a4-b1f1-71535d7929f4",
    "visible": true
}