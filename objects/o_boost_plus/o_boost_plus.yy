{
    "id": "0414713c-fa2c-4628-8d9c-c5587030a9d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_boost_plus",
    "eventList": [
        {
            "id": "7b2a5d74-8dd6-4f9a-839d-7171d50f4041",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0414713c-fa2c-4628-8d9c-c5587030a9d9"
        },
        {
            "id": "9791a251-faec-4024-af51-6fe2dfd0b27e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0414713c-fa2c-4628-8d9c-c5587030a9d9"
        },
        {
            "id": "eb603721-9b89-4c6b-a0b9-3293ae5da3e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0414713c-fa2c-4628-8d9c-c5587030a9d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dbf8a5a3-676a-473c-a8b6-462f98990dfe",
    "visible": true
}