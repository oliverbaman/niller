{
    "id": "43d4e6b9-a2f1-4536-b0a6-4fe440b2c241",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object46",
    "eventList": [
        {
            "id": "f4ad7beb-5dcf-4b4e-a5c3-752a0c050bab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43d4e6b9-a2f1-4536-b0a6-4fe440b2c241"
        },
        {
            "id": "5e992ca3-b457-4342-812b-408de195deef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43d4e6b9-a2f1-4536-b0a6-4fe440b2c241"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "6fdd4f6a-9bc7-4e1f-b957-bbfcfadfcb1a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "var_image_index",
            "varType": 0
        },
        {
            "id": "e6529756-1ffc-4d52-9f4b-f40cc78bd9b4",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "offset",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "cd606e4c-0960-4248-95b9-c55bcdf88f46",
    "visible": true
}