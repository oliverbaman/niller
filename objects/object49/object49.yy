{
    "id": "7472905e-c5d1-40d9-b86a-f6c123676a2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object49",
    "eventList": [
        {
            "id": "8d7352d4-e49b-412e-b448-97f4bbedf53e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7472905e-c5d1-40d9-b86a-f6c123676a2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99aa72e4-5a92-4ce6-8b3b-1d67a7f385b5",
    "visible": true
}