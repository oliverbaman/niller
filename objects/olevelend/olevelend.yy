{
    "id": "f1930b1c-509c-4f1f-b5f8-b14ca10e5571",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "olevelend",
    "eventList": [
        {
            "id": "b9e68d1c-f22e-4e0f-84b2-57a6df47ef62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f1930b1c-509c-4f1f-b5f8-b14ca10e5571"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "526e6d64-30ba-4279-83bf-ac37e5378fbf",
    "visible": true
}