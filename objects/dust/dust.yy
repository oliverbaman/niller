{
    "id": "d4bb1747-d874-4704-8f3b-c39194d2bc2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "dust",
    "eventList": [
        {
            "id": "c20ff406-81b1-448b-82f4-3b22ea964cb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d4bb1747-d874-4704-8f3b-c39194d2bc2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c6dda367-3c27-4542-a1a8-f8403401fd98",
    "visible": true
}