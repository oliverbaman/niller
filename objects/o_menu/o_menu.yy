{
    "id": "892aedb8-37f5-44b8-8394-ad9b6a60300e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_menu",
    "eventList": [
        {
            "id": "a5c30c3e-b371-4e33-bc8e-549e30bbb4ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "892aedb8-37f5-44b8-8394-ad9b6a60300e"
        },
        {
            "id": "259a45f9-64a2-417d-ba1d-779923f99077",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "892aedb8-37f5-44b8-8394-ad9b6a60300e"
        },
        {
            "id": "47fd40f7-0e74-4e92-85c2-b07c807c4353",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "892aedb8-37f5-44b8-8394-ad9b6a60300e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}