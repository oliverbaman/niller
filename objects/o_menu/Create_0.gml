gui_width = display_get_gui_width()/2
gui_height = display_get_gui_height()/2
gui_margin = 32

menu_x = gui_width 
menu_y = gui_height + 100
menu_y_target = gui_height
menu_speed = 20
menu_font = f_menu
menu_itemheight = font_get_size(f_menu)
menu_comitted = -1
menu_control = true

menu[2] = "start"
menu[1] = "continue"
menu[0] = "exit"

menu_items = array_length_1d(menu)
menu_cursor = 2 
timer = 0