timer ++
if timer > 150
{

	menu_y += (menu_y_target - menu_y) / menu_speed

	if ((menu_y - menu_y_target) < 2)
	{
		menu_y = menu_y_target
	}
}


if (menu_control)
{
	if (keyboard_check(ord("W")) or gamepad_button_check_pressed(0, gp_padu))
	{
		menu_cursor++
		if (menu_cursor >= menu_items) menu_cursor = 0
	}
	
	if (keyboard_check(ord("S")) or gamepad_button_check_pressed(0, gp_padd))
	{
		menu_cursor--
		if (menu_cursor < 0) menu_cursor = menu_items-1
	}
	
	if (keyboard_check_pressed (ord("N")) or gamepad_button_check_pressed(0, gp_face3) or gamepad_button_check_pressed(0, gp_face1))
	{
		menu_comitted = menu_cursor
		menu_control = false
	}
	switch (menu_comitted)
	{
		case 2: room_goto_next(); break;
		case 0: game_end(); break;
	}
}
