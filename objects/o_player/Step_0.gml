//////////  KEYBOARD  //////////
#region Keyboard

if hascontrol
{
	if player = "niller"
	{
		key_left = keyboard_check(ord("A")) or gamepad_button_check(0, gp_padl);
		key_right = keyboard_check(ord("D")) or gamepad_button_check(0, gp_padr);
		key_up = keyboard_check(ord("W")) or gamepad_button_check(0, gp_padu);
		key_down = keyboard_check(ord("S")) or gamepad_button_check(0, gp_padd);
		key_jump = keyboard_check_pressed(ord("J")) or (gamepad_button_check_pressed(0, gp_face1)) or (gamepad_button_check_pressed(0, gp_face2))
		key_grab = keyboard_check(ord("K")) or gamepad_button_check(0, gp_shoulderr)
		key_boost = keyboard_check_pressed (ord("N")) or (gamepad_button_check_pressed(0, gp_face3)) or (gamepad_button_check_pressed(0, gp_face4))
		key_jump_released = keyboard_check_released(ord("J")) or (gamepad_button_check_released(0, gp_face1)) or gamepad_button_check_released(0, gp_face2)
		
		key_time = keyboard_check_pressed(ord("T"))

		if key_time
		{
			if show_time == 0
			{
				show_time = 1
			}
			else
			{
				show_time = 0
			}
		}
		
		//key_right = gamepad_button_check(0, gp_padr)
		//key_left = gamepad_button_check(0, gp_padl)
		//key_up = gamepad_button_check(0, gp_padu)
		//key_down = gamepad_button_check(0, gp_padd)
		//key_grab = gamepad_button_check(0, gp_shoulderr)
		//key_jump = (gamepad_button_check_pressed(0, gp_face1)) or (gamepad_button_check_pressed(0, gp_face2))
		//key_boost = (gamepad_button_check_pressed(0, gp_face3)) or (gamepad_button_check_pressed(0, gp_face4))
		//key_jump_released = (gamepad_button_check_released(0, gp_face1)) or gamepad_button_check_released(0, gp_face2)
	}
	else if player = "david"
	{
		key_right = gamepad_button_check(0, gp_padr)
		key_left = gamepad_button_check(0, gp_padl)
		key_up = gamepad_button_check(0, gp_padu)
		key_down = gamepad_button_check(0, gp_padd)
		key_grab = gamepad_button_check(0, gp_shoulderr)
		key_jump = (gamepad_button_check_pressed(0, gp_face1)) or (gamepad_button_check_pressed(0, gp_face2))
		key_boost = (gamepad_button_check_pressed(0, gp_face3)) or (gamepad_button_check_pressed(0, gp_face4))
		key_jump_released = (gamepad_button_check_released(0, gp_face1)) or gamepad_button_check_released(0, gp_face2)
	}
}
else
{
	key_right = 0
	key_left = 0
	key_up = 0
	key_down = 0
	key_jump = 0
	key_grab = 0
	key_boost = 0
	key_jump_released = 0
}

#endregion

////////// COLISION VAR. //////////
#region Collision vars

var was_on_floor = on_floor
on_floor = place_meeting (x, y+1, o_solid)
var on_roof = place_meeting (x, y-1, o_solid)
on_right_wall = position_meeting (x+(6*image_xscale), y, o_solid)


#endregion

////////// BOOST //////////
#region Boost

if in_boost and boost_time > boost_time_max
{
	in_boost = 0
	boost_time = 0
}

if in_boost
{
	boost_time += 1
}

if on_floor and !in_boost
{
	no_boost = 1
}

// Action

if key_boost and !in_boost and no_boost > 0
{
	boostsp = 9
	no_boost = 0
	in_boost = 1
	
	var h_dir = key_right - key_left
	var v_dir = key_down - key_up
	
	if (key_right or key_left) and (key_up or key_down)
	{
		boostsp *= 0.9
		//no_boost = no_boost * 3
	}
	if key_right or key_left or key_up or key_down
	{
		hsp = h_dir * boostsp
		vsp = v_dir * boostsp
	}
	else
	{
		hsp = image_xscale * boostsp
	}
}

if bounce
{
	bounce_time ++
	
}
if bounce_time > 8
{
	bounce = 0
	bounce_time = 0
}

#endregion
	
////////// GRAVITATION //////////
#region Gravitation

if in_boost
{
	vsp -= sign(vsp)
	if abs(vsp) < 1
	{
		vsp = 0
	}
}
else if !on_floor
{
	vsp = vsp + grv
	vsp = min(vsp, max_grv)
}

#endregion

////////// HORISONTAL MOVE ////////// 
#region Horizontal move



if !in_boost
{
	var move = key_right - key_left;
	
	if on_floor 
	{
		hsp += move
	}
	else 
	{
		hsp += move
	}	

	if move == 0
	{
		if on_floor 
		{
			hsp -= sign(hsp)
		}
		else
		{
			hsp -= sign(hsp) * grv
		}
		if abs(hsp) < 1
		{
			hsp = 0
		}
			
	}
	if !bounce
	{
	hsp = clamp(hsp, -walksp, walksp)
	}
	
} 

else
{
	hsp -= (sign(hsp) / 2)
}

#endregion

////////// JUMPING //////////
#region Jumping

if key_jump
{
	
	if on_floor or jump_delay > 0
	{
		vsp = -jumpsp
		hold_on = 1
		hold = 0
		instance_create_layer(x, y+7, "niller", dust)
	}
	//else if on_right_wall and !key_grab
	//{
	//	vsp = -jumpsp
	//	hsp = -jumpsp*move
	//	hold_on = 1
	//	hold = 0
	//}
	//else if on_left_wall and !key_grab
	//{
	//	vsp = -jumpsp
	//	hsp = jumpsp
	//	hold_on = 1
	//	hold = 0
	//}
}

/////// hanging in air ///////////

//if (vsp > 2 and hold_on == 1) or hold_on == 2
//{
//if hold < hold_max
//{
//	if key_grab
//		{
//			hold_on = 2
//			vsp = vsp - 1
//			hold = hold + 4
//			if hold >= hold_max
//			{
//				hold_on = 0
//			}
//		}
//	}

//}
//	if keyboard_key_release("K")
//			{
//				hold_on = 0
//			}
			
#endregion

////////// CLIMBING  //////////
#region Climbing

if !in_boost and key_grab and on_right_wall and !in_climb_up
{
	//climb = 1	
	if vsp > 0
	{
		vsp = 0
	}
	hsp = 0
	
	if key_up and vsp > -1
	{
		vsp = -climbsp
		if on_right_wall and !position_meeting(x+(6*image_xscale),y-climbsp,o_solid)
		{
			if place_meeting(x+(6*image_xscale),y-climbsp,o_spikes)
			{
				vsp = 0
			}
			else
			{

				climb_target_x = x+4*image_xscale
				climb_target_y = y-9
				in_climb_up = 1
				hascontrol = 0
				move_towards_point(climb_target_x, climb_target_y, climb_up_spd)
			
				
			}
		}
	}
		
	
	else if key_down
	{
		vsp = climbsp * 1.5
	
	}

	
	if key_jump
	{
		vsp = -jumpsp*0.9
		
		if move == -image_xscale or key_down
		{
			hsp = -jumpsp*image_xscale
		}

		
	
	
		//else if key_right and on_left_wall
		//{
		//	vsp = -jumpsp*0.9
		//	hsp = jumpsp
		//}
		
	}	
}

if in_climb_up = 1
{
	if point_distance(x,y,climb_target_x,climb_target_y) <= climb_up_spd
	{ 
		hascontrol = 1
		in_climb_up = 0
		x = climb_target_x
		y = climb_target_y
		speed = 0
	}
	else
	{
		move_towards_point(climb_target_x, climb_target_y, 1)
	}
	
}

#endregion

//////// JUMP CONTROL //////////
#region Jump control

if key_jump_released and !in_boost
{
	if vsp < 0 
	{
		vsp /= 2;
	}
}

#endregion

////////// OBJECT COLLITION //////////
#region Object collision


//var sprite_bbox_top = bbox_top - y;
//var sprite_bbox_bottom = bbox_bottom - y;
//var sprite_bbox_right = bbox_right - x;
//var sprite_bbox_left = bbox_left - x;

////Horizontal collisions
//x += hsp

////Snap
//var wall = instance_place(x+sign(hsp), y-1, o_solid)
//if wall != noone {
//    if hsp > 0 {
//        x = (wall.bbox_left-1) - sprite_bbox_right
//    } else if hsp < 0 {
//        x = (wall.bbox_right+1) - sprite_bbox_left
//    }
//    hsp = 0
//}

////Vertical collisions
//y += vsp

////Snap
//var wall = instance_place(x, y+sign(vsp), o_solid)
//if wall != noone {
//    if (vsp > 0) { //down
//        y = (wall.bbox_top-1) - sprite_bbox_bottom
//    } else if (vsp < 0) { //up
//        y = (wall.bbox_bottom+1) - sprite_bbox_top
//    }
//    vsp = 0
//}



if (place_meeting(x+hsp,y,o_solid))
{
	//x = round(x-sign(hsp))
	while (!place_meeting(x+sign(hsp),y,o_solid))
	{
		x = x + sign(hsp)
	}
	hsp = 0
} else {
	x = x + hsp
}

if (place_meeting(x,y+vsp,o_solid))
{
	//y = round(y-sign(vsp))
	while (!place_meeting(x,y+sign(vsp),o_solid))
	{
		y = y + sign(vsp)
	}
	vsp = 0
} else {
	y = y + vsp
}

#endregion

////////// MOVEMENT RESULT //////////
#region Movement result

//x += hsp
//y += vsp

#endregion

////////// ANIMATION //////////
#region Animation

if in_boost == 1
{
	var o_boost = instance_create_layer(x, y, "niller", o_boost_niller)
	o_boost.image_index = image_index
	o_boost.sprite_index = sprite_index
	o_boost.image_xscale = image_xscale
	o_boost.name = player
	
	if vsp < 0 and (abs(hsp)>0)
	{
		image_speed = 1
		sprite_index = asset_get_index(player + string("_boostUR"))
	}
	else if (abs(hsp)>0)
	{
		image_speed = 1
		sprite_index = asset_get_index(player + string("_boostR"))
	}
	else if vsp < 0 and hsp = 0
	{
		image_speed = 1
		sprite_index = asset_get_index(player + string("_boostU"))
	}
}

else if key_grab and (on_right_wall)
{
	if no_boost == 1
	{
	sprite_index = asset_get_index(player + string("_climb_1"))
	}
	else
	{
		sprite_index = asset_get_index(player + string("_climb_0"))
	}
	
	if vsp = 0
	{
		image_speed = 0
		image_index = 1
	}
	
	if vsp < 0
	{
		image_speed = 1
	}
		
	if vsp > 0
	{
		if no_boost = 1
		{
		sprite_index = asset_get_index(player + string("_climbD_1"))
		}
		else 
		{
			sprite_index = asset_get_index(player + string("_climbD_0"))
		}
		image_speed = 1
	}
	if vsp < -2
	{
		if no_boost == 1
		{
		sprite_index = asset_get_index(player + string("_jump_1"))
		}
		else
		{
			sprite_index = asset_get_index(player + string("_jump_0"))
		}	
		image_speed = 0
		image_index = 1
	}
}
else if !on_floor
{
	if no_boost = 1
	{
	sprite_index = asset_get_index(player + string("_jump_1"))
	}
	else
	{
		sprite_index = asset_get_index(player + string("_jump_0"))		
	}
	image_speed = 0 
	if (sign(vsp) < 0) image_index = 1; else image_index = 0
}
else if on_floor
{
	image_speed = 1
	
	if (hsp == 0)
	{
		sprite_index = asset_get_index(player + string("_stand_1"))
		if key_down
		{
			sprite_index = niller_duck
		}
	}
	
	else 
	{
		sprite_index = asset_get_index(player + string("_run_1"))
	}

}

if on_floor and !was_on_floor and !in_climb_up
{
	instance_create_depth(x, y+7, "niller", dust)
}

if in_climb_up
{
	sprite_index = niller_climbO_1
	image_speed = 1
}

if (hsp != 0) image_xscale = sign(hsp)



	

#endregion

