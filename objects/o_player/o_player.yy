{
    "id": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "1920ac29-33a2-4444-8b9c-439d185ef4d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "5c2fdc65-9403-4f32-aa5a-cc2e7e5a2483",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "717b92e0-57eb-4c22-9007-401861b64608",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 10,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "2d344a29-e989-48d9-b3c1-9e2535095686",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "a5ff2304-28b5-4c6a-95ef-7c6e6ab0286d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "81f88953-e442-4861-b515-693518e61ce3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "22cf3ac9-7fb5-49c0-b92d-cfaaec20e20f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9234921f-cc79-4034-ba74-10966ba43556",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "8c13266a-8d2d-41a6-a981-4b858d385c86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "63899de4-d8bb-46d5-ab25-1eb37d470a76",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "4c7b62fa-74aa-49fd-b345-dc23ed233765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "f622980d-52d6-4ddd-b98b-0a3643d51dc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "240ca00f-5c96-4319-b342-6461acec44b0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "c6b5bf44-9ed8-4728-af0f-21d4dbba16f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c431ce85-213e-47df-ab76-05e372604f38",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "dc32e804-9687-4036-8725-d8b6a51d0b3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "654a7a70-160e-4efb-b5de-7fcee2a7b124",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "85649d4a-a66d-487d-9d63-7f66086a4c5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "63ad5c3c-4153-4693-b876-f4a85b2cb1ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        },
        {
            "id": "31b0a0b5-d231-489a-af55-52e9e126e622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2a701aa7-1286-4dd4-be30-eb36276f509b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7"
        }
    ],
    "maskSpriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "aa3644ec-3b5b-4861-b717-7e10c3120dd4",
    "visible": true
}