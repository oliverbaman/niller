player = "niller"
color = $FF00D4

hsp = 0
vsp = 0

grv = 0.6

max_grv = 6

walksp = 2.5

jumpsp = 5
jump_delay = 0

hold_max = 30
hold = 0
hold_on = 0

climb = 0
climbsp = 1.5
in_climb_up = 0
climb_up_spd = 1.5

boostsp = 5
in_boost = 0
no_boost = 1
boost_time = 0
boost_time_max = 4

hascontrol = true

died = 0
timer = 0
countdown = 30

on_floor = 0

bounce = 0
bounce_time = 0

show_time = 0
