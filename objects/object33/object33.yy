{
    "id": "1953a123-7f11-49ff-ab23-86a712225ca2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object33",
    "eventList": [
        {
            "id": "8d759d8d-d26d-40c3-8111-790f23f0c420",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1953a123-7f11-49ff-ab23-86a712225ca2"
        },
        {
            "id": "104afd3e-ec7c-41cb-a37d-d768147fdc0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1953a123-7f11-49ff-ab23-86a712225ca2"
        },
        {
            "id": "cfe951bb-0a48-4525-b90a-4db27c94d29b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7b83e4c-c8a1-45ae-98e0-7d6b39d073d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1953a123-7f11-49ff-ab23-86a712225ca2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "296e1fb2-5dc9-4fd2-bec0-283edccf174c",
    "visible": true
}