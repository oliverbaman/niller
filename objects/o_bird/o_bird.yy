{
    "id": "5b4e2878-1b41-4161-b416-63bafc1cc4e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bird",
    "eventList": [
        {
            "id": "ea4b4de9-46c7-47b3-816b-75cc2913a11f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5b4e2878-1b41-4161-b416-63bafc1cc4e9"
        },
        {
            "id": "eb2e959a-0278-4701-997e-8e309ca650d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5b4e2878-1b41-4161-b416-63bafc1cc4e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "74813390-9ccb-4501-9347-e736784c239b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "index",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "5882407e-ae11-46be-aacf-31e984e778e9",
    "visible": true
}