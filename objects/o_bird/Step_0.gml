

if instance_exists(o_niller)
{
	if o_niller.x > (x-30)
	{
		is_flying = true	
	}

	if is_flying
	{
		if sprite_index != bird_flying
		{
			sprite_index = bird_flying
			image_speed = 15
		}
	} 
	else 
	{
		if sprite_index != bird 
		{
			sprite_index = bird
			image_speed = 0
			image_index = index
		}
	}

	if is_flying
	{
		move_towards_point(x+dir, y-100, 4)
		if y <= (origin_y - 150)
		{
			instance_destroy()
		}
	}
}