{
    "id": "9b4f977e-e61f-49cc-96da-932cec4f8709",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "spawnpoint",
    "eventList": [
        {
            "id": "c7bf4669-98fb-4a9f-b0e4-7946d3c81b78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9b4f977e-e61f-49cc-96da-932cec4f8709"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b512e3db-1b96-45f0-8e71-431c00b11474",
    "visible": false
}