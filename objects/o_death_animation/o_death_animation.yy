{
    "id": "63d8f0bb-372a-4a0b-838c-1461fc2a6fc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_death_animation",
    "eventList": [
        {
            "id": "3078ef19-8430-4228-a358-04e108a66516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63d8f0bb-372a-4a0b-838c-1461fc2a6fc8"
        },
        {
            "id": "f2589933-7ec9-47d6-b278-eb8e4c961b0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "63d8f0bb-372a-4a0b-838c-1461fc2a6fc8"
        },
        {
            "id": "653d0f48-114b-45b6-a650-ca0eb8f2f583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "63d8f0bb-372a-4a0b-838c-1461fc2a6fc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "bd78920f-c02e-4af7-a9f5-5a24b215cb32",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "$FF22E204",
            "varName": "color",
            "varType": 7
        }
    ],
    "solid": false,
    "spriteId": "fd2a297a-8951-4fe6-92e1-4770327584c5",
    "visible": true
}