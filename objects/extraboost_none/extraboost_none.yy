{
    "id": "c926338f-3cb1-401e-8567-f18ddd6cc297",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "extraboost_none",
    "eventList": [
        {
            "id": "1efd965c-f1a5-4cd2-ba3f-1529b5e12e7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c926338f-3cb1-401e-8567-f18ddd6cc297"
        },
        {
            "id": "b5d7e646-7ca1-4772-ac43-c3c4d27f2f96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c926338f-3cb1-401e-8567-f18ddd6cc297"
        }
    ],
    "maskSpriteId": "20749e0f-aa20-4e2d-9f9b-e404b7998b29",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "efffe5d1-cc7a-4ffc-93ed-7dd3f7eba45c",
    "visible": true
}