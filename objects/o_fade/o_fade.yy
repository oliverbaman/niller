{
    "id": "3b12a0a6-7331-4485-876d-6ee2d0b44ae5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_fade",
    "eventList": [
        {
            "id": "b7f3acf7-9c5b-48aa-9149-807d59760d08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3b12a0a6-7331-4485-876d-6ee2d0b44ae5"
        },
        {
            "id": "4c338f78-28a5-468a-add7-2ff8bf4967dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3b12a0a6-7331-4485-876d-6ee2d0b44ae5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a362655e-d1a2-4cdc-84af-7d79492cc352",
    "visible": true
}