timer ++
if timer > 30 and timer < 40
{
	fade_in = 1
	
}

if fade_in and image_alpha > 0
{
	image_alpha = image_alpha - 0.02
}
else
{
	fade_in = 0
}

if fade_out = 1
{
	image_alpha = image_alpha + 0.02
}

if fade_out and image_alpha = 1
{
	room_goto_next()
}