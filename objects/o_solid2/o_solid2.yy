{
    "id": "c623aaab-cb08-4fd7-8207-fc431356ac3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_solid2",
    "eventList": [
        {
            "id": "48e93f77-a731-4f22-8415-38497d5e3272",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c623aaab-cb08-4fd7-8207-fc431356ac3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5a742c92-82d5-469d-820d-ebcf0ca3c2c1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "d7e9be38-b0f4-48f6-a792-01ce263becad",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "hsp",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "c47f297c-ccdd-4221-9e4c-adeb86dc531b",
    "visible": true
}