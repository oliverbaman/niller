{
    "id": "be43eb30-c56a-4e70-b73e-627874af9dcf",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Apple Braille",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cdd2ff8f-174f-4451-9023-32c625e5e074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 180,
                "y": 128
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6815acd1-3144-4cb0-8fbc-fce6c3bfa3aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 27,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 8,
                "y": 159
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bd3134f3-290f-43ea-80e6-39b2c3bf8925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 27,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 138,
                "y": 128
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cbdf96e8-88c3-499a-be6d-88201f476f0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 204,
                "y": 4
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "29cd7e23-b501-41cc-a755-75fb1fed53f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 194,
                "y": 97
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d611eb4e-c11d-47d8-bb24-bc7ba7bc90b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 55,
                "y": 4
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5d1814ff-fe25-4457-9088-0c3a72880175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 72,
                "y": 4
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "556b8fe3-2457-4f9d-9a1a-f8a7d5f5092e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 27,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 246,
                "y": 128
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "398da481-651d-4410-9a3a-e3a4267ed2ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 27,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 160,
                "y": 128
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "aea59243-fc23-4358-82d5-bdb62e8ddf5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 27,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 170,
                "y": 128
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4bf51c6f-0ae0-4f05-a6a4-672566f72e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 97
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1bd5927e-986b-4bf6-802d-7cb2df66df01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 97
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "22fdbf5c-f2b0-4424-83f4-d17f651cbd41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 27,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 233,
                "y": 128
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "32bc379a-ff30-4c5f-9cf9-b7ddde7faaec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 27,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 149,
                "y": 128
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7e3511a0-0017-4363-9589-9fc0e4c5bd10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 27,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 240,
                "y": 128
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "894e6ca5-77f7-4e15-8f57-54979bca834f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 114,
                "y": 128
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "defab480-aca3-4eac-bfe5-b88538de8d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 240,
                "y": 66
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cd5c0a3b-e8bb-462d-94b1-8bbc925db9e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 200,
                "y": 128
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "40d3efc9-934a-455b-b819-fcfc4f3d0527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 226,
                "y": 66
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a72f5ecc-e5dd-4c74-9a68-41ea909f12d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 184,
                "y": 66
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3bf7c058-7554-4eaa-ad26-afd41706ff6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "35b47ad3-8f38-4ce2-a67b-4b875561c5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 220,
                "y": 97
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "79495f55-14a6-4ce6-b187-57254e3d727e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 97
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "03b87fcd-18f5-423d-a1eb-9983c3a0c9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 97
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "25b736ca-c000-474c-bd9a-2bdf4ccb676c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 198,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8546a58a-b726-47ae-bf60-53b030a874a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 212,
                "y": 66
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e521a55a-2f00-4d6d-b13d-20e6fc611f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 27,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 2,
                "y": 159
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4b70b416-76d2-404a-947c-fff536fd480c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 27,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 226,
                "y": 128
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5051a8a2-4a8f-4304-bd22-fd1483b958f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3bc3b025-0efe-4eef-8932-ff425c2ab319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 97
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "16cfbed6-6135-40d0-9886-a5175e9cb3cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 97
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "400fdaf7-93d2-46dc-a6ed-f4d48585da36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 97
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a3c0b302-acd1-4bca-9169-ecf7d0e81087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 106,
                "y": 4
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "84f2e04f-d715-497b-a985-b160fec3d3bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 123,
                "y": 4
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "97cac78b-6618-4852-80d9-76e734c0dbe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 18,
                "y": 35
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "41690250-3500-4054-986d-f0dd2953e645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 93,
                "y": 35
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ff1697ef-dcd0-411d-a4d9-a1f355ddc150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 33,
                "y": 35
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7fb5a0ab-53fe-4eee-9582-963effe2382b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 28,
                "y": 128
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "510e789d-bc7c-4ea1-b6ed-26c492c9a87f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1de65c2e-70f1-4d8b-b5a5-dc192fa1f1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 97
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bbb03074-8a8a-4ca1-bec5-370a4f05b908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 97
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "df7a8996-e5fa-4b7d-ae9f-5bc6781fcc82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 78,
                "y": 128
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4626ab57-761e-4073-a5a5-604835835d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 233,
                "y": 97
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "47a0f60c-734d-43b0-8f3e-66f456a165f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 108,
                "y": 35
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c0c8e755-a6ca-4422-a55c-2d347d739585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 181,
                "y": 97
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b2fae2bd-58fa-438d-8414-7fb6a59f9fda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 138,
                "y": 35
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "28f466f2-c814-43aa-a843-55b47a90e2ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 213,
                "y": 35
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3d3d3cbf-19c3-4dcd-9cc2-d4389c0478ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 172,
                "y": 4
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ed5f4030-5ba7-4e27-bc79-d91756ff4a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 15,
                "y": 128
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4251eab6-a9be-4114-b73a-bb68031fbf59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 220,
                "y": 4
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "dae0a903-0bd4-4d81-920e-b83547a9c908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 227,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f6c8cfd7-7828-41b0-87ed-652c40379000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 241,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "17310e98-2c74-4304-b319-232195b4eccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 48,
                "y": 35
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5cc042e2-d940-4a40-bbe8-2e5a265828dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 66
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0347fdec-c188-4c57-89a2-7ccb7eb4e80e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 89,
                "y": 4
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "35b09209-d555-4b49-80ec-ff596552e07f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 38,
                "y": 4
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7346ba26-0264-408b-84a8-297840646424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 156,
                "y": 4
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2980308a-7447-489f-b6f9-4fc701585744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 140,
                "y": 4
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c68fff10-a25c-42a8-afb7-01de89abaca3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 63,
                "y": 35
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6a7cb8d8-c022-49d9-8b0c-4966ba58bd5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 27,
                "offset": 4,
                "shift": 14,
                "w": 8,
                "x": 190,
                "y": 128
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "23867af1-8e03-47bd-b473-c48674fd6a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 66,
                "y": 128
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d76cb196-fd19-40c4-85aa-911bc1f490e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 27,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 209,
                "y": 128
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3794dfe6-f7fa-443b-81db-fe790b1c591b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 78,
                "y": 35
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3ed383d0-3f05-458c-957c-b9f51ed1cca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 27,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 2,
                "y": 4
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5f294961-3899-4155-8049-8d338c4c6932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 27,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 218,
                "y": 128
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a5809f2b-a2fe-4bff-8102-26a1a6f95b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 66
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f8a2e7bc-73c9-4884-8339-e4e6c4238808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 66
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "199cc5be-d993-424b-9c5e-c9c1ddbcf30c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 41,
                "y": 128
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "068230ea-ac7a-4574-9d3e-cb2fbbfa7d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 66
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "022bf7f9-a201-4f5c-bfdd-6685a5745654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 66
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a7437d99-b795-4085-8801-f2bdd104d80a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 153,
                "y": 35
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b41ff897-de63-4a99-b6b1-8796e95bc8cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 168,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "658dbc40-fc4b-4106-83e8-c0f511480b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 66
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e1745a24-52db-4996-815b-15069344947f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 155,
                "y": 97
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5f4f3f61-6258-4411-a2ff-2918a70eae63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 102,
                "y": 128
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "af5c604c-aaf0-49a9-aa5d-8ffb4ecbfd11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 66
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4e6be67c-dd44-4022-9c84-bcda74fd02ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 142,
                "y": 97
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "736f03d6-6cd2-4d30-8100-18ce9043422f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 198,
                "y": 35
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bd8191c5-1361-42a1-bc5c-1da79545b2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 66
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "927066d5-3706-4fba-9721-8a75d51759dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 123,
                "y": 35
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f38748ff-e392-440a-80a5-f0c3eae5fc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 66
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0c9ed61b-1704-49b8-9100-34fb644b10f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 66
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1c9f9c07-7046-42bc-a95b-ad5432f77416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 27,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 126,
                "y": 128
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b982d86a-2326-4960-be89-06e270680b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 168,
                "y": 97
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b7b6a627-3c02-4c66-82ce-7e70d7e997ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 66
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c0d5e259-f702-40ea-99ea-e58d2062f36d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "608cc444-8efd-4eaa-8edf-a4e1851474cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 188,
                "y": 4
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e0bb0bde-a298-4c92-9c15-53f01b4028b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 27,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 20,
                "y": 4
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7d56088c-6b8d-45ba-b710-979b814d7d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 183,
                "y": 35
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "affb4b0c-171e-4497-a4b1-237d85fff97a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 236,
                "y": 4
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "08d17354-836a-4f64-b77d-d2dccd2b870c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 207,
                "y": 97
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3f55512b-434a-4a6a-a072-0bb1d22acf3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 54,
                "y": 128
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "91d65a10-87ab-44c3-bbde-71c9fecec1ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 27,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 14,
                "y": 159
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "59cdedf6-8cc7-406a-ab70-c9fe6cdbcd23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 27,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 90,
                "y": 128
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f4de3d0b-4820-4a9b-b2aa-60542b8659d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 66
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Outline 6 Dot",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}