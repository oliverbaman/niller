{
    "id": "deabc265-7d21-4750-a066-f9d977e70633",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_menu2",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial Black",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "46d13b21-b86d-4d75-ac13-8b2bd1844683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 66,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 420,
                "y": 274
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "719e8628-8c91-44fc-ab55-3f7f516a8a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 66,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 117,
                "y": 342
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c3c08b92-aa28-48e0-bb86-b2cec12c1351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 66,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 228,
                "y": 274
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "83f5fee3-1205-440e-a13c-4d8009fd5549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 67,
                "y": 138
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "25505ff9-2e3b-4565-9958-278798c567c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 98,
                "y": 138
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8c1e3898-dc36-4919-8b83-8994377bc0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 66,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "968848ae-8bee-446f-b8a2-78f024dae6f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 66,
                "offset": 3,
                "shift": 42,
                "w": 37,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "210612ab-22f5-42f6-81dd-585829858aab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 66,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 91,
                "y": 342
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0087864b-add6-4b1d-8c1c-952b0ec50b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 66,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 489,
                "y": 274
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fb25f58e-17ba-471a-889d-166ec310bfca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 66,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 455,
                "y": 274
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "808bddba-5188-49f9-8870-710c0d13d0fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 66,
                "offset": 4,
                "shift": 26,
                "w": 18,
                "x": 362,
                "y": 274
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f8055864-17b1-4500-b9c4-9349062a5c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 89,
                "y": 274
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f3e73805-e6cf-4cd0-8282-70de7b9db972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 66,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 143,
                "y": 342
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "adca1b8d-3d51-4f9e-aa20-0e30458fb668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 66,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 342
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6dce2198-73c9-4b15-9641-65395f8110fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 66,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 78,
                "y": 342
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6e7b6ab6-0765-4a94-b252-c5bb66279a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 66,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 342
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3540d422-45fe-48bf-aefb-b0dbe62ad0f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 64,
                "y": 206
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "dc9a0bac-79c2-4b9b-a20b-033dc214d898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 66,
                "offset": 3,
                "shift": 31,
                "w": 21,
                "x": 253,
                "y": 274
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7175565f-d50c-4cca-8e3c-0cfa701e0cf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 439,
                "y": 138
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "26f5cfcd-4ec5-47e7-886c-929c1712c6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 2,
                "y": 206
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5f81aa73-d4ac-4793-bfae-4dbd41550bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 66,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e836ab81-35d3-438f-86c3-7e64b7f8e71e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 470,
                "y": 138
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ac988b92-aeee-435a-a5f4-d272056f3d58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 346,
                "y": 138
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5365be94-6e93-4ca3-963a-9ed00b33ac8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 215,
                "y": 206
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3708098a-f105-404b-9224-789f3718a5ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 377,
                "y": 138
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "19bc1fd4-ad9f-4e35-be91-fb36ae5f4d4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 408,
                "y": 138
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5b859697-ebba-4606-9097-9120b75588df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 66,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 104,
                "y": 342
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d8a9033b-d9bf-4fbb-b102-a95e621ff5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 66,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 130,
                "y": 342
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8d8d4544-378c-48a7-a044-0707a7da8231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 60,
                "y": 274
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2b2f49a5-a3a6-4a6e-917d-baa8b90905b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 145,
                "y": 274
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9b5a0068-704f-48eb-857b-b314b052069a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 31,
                "y": 274
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c3b82a11-e986-4a85-82b0-e4aa893f2f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 66,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 274
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3ef540e0-92e0-4d7e-92e6-7e71acd63926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 66,
                "offset": -1,
                "shift": 35,
                "w": 36,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "714d417c-39c3-4dac-82aa-a6c3c4e2fb4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 66,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6a2f7e47-76e4-4b39-be38-fe8e911c0357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 66,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 393,
                "y": 70
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5300b5b9-4cc3-4467-ad8c-3f71d3dba304",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 66,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 150,
                "y": 70
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a3761d25-b157-4d41-9111-1c5793726683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 66,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 325,
                "y": 70
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1ad3c9bb-fd65-432b-a840-52ef4985ddc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 66,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 222,
                "y": 138
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b068f943-a45b-4f74-ba73-a33da2b116bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 66,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 480,
                "y": 206
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e6814de5-3d00-4b7e-8899-9ff1a01cbcc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 66,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 40,
                "y": 70
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "524ee034-33ba-4e27-ab83-2777f4dddde7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 66,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 185,
                "y": 70
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9479fff7-b037-4fdf-bbfa-94c77ed0729e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 66,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 50,
                "y": 342
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bbeea73f-1061-4e81-862b-4cc87083aa41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 66,
                "offset": 0,
                "shift": 31,
                "w": 28,
                "x": 275,
                "y": 206
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b2f97ac9-2fe4-45f4-a31a-ddadb3ef4a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 66,
                "offset": 3,
                "shift": 39,
                "w": 37,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "be185438-3b7e-47f1-9717-4850b03dfb64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 66,
                "offset": 3,
                "shift": 31,
                "w": 28,
                "x": 305,
                "y": 206
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6558c9c4-e0b8-4526-9816-af8faadb10ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 66,
                "offset": 3,
                "shift": 44,
                "w": 39,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "36b691a2-d4ee-475e-a537-3248017581ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 66,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 290,
                "y": 70
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0e20699c-9b39-4b91-8a60-7a49ed180cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 66,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 77,
                "y": 70
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2d0f78a7-6bf2-4d1d-9b34-3ed831c79e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 66,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 253,
                "y": 138
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b8a55ba6-069b-4a37-9352-9a5f5c869461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 66,
                "offset": 2,
                "shift": 39,
                "w": 37,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "11743b93-296f-4031-bf7d-ed0a7185f7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 66,
                "offset": 3,
                "shift": 37,
                "w": 34,
                "x": 114,
                "y": 70
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "004ac042-7739-4f9b-ac7f-ed11376e126e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 66,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 461,
                "y": 70
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0c9a0715-adc3-4de9-8492-1460cab5560f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 66,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 427,
                "y": 70
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "488c0228-2252-4ff6-a956-1d8708bba50c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 66,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 255,
                "y": 70
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "94f23745-8e57-4850-b38a-fdef98776169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 66,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6d657d3e-7d4f-43b6-b9d7-c3cba28f2a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 66,
                "offset": -1,
                "shift": 47,
                "w": 49,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a6c09076-e491-444f-b17f-210aa283a531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 66,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ca8bd813-9b4f-42b5-bc91-181280a9c0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 66,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "db8d744e-a44f-431b-b06d-35bc01e2606f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 66,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 220,
                "y": 70
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "737f7098-1c64-45f6-9c3c-7e1c1d948146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 66,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 472,
                "y": 274
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8c1d47dd-014d-4103-a3f6-9bc6b99b6d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 66,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 34,
                "y": 342
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d5760809-aa37-469a-9002-67a391cf9a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 66,
                "offset": 0,
                "shift": 18,
                "w": 15,
                "x": 438,
                "y": 274
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d9ae64b1-7021-441e-8f43-41804e505f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 117,
                "y": 274
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e1e93635-9f14-4cee-9c2e-198577c4b465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 66,
                "offset": -1,
                "shift": 24,
                "w": 25,
                "x": 201,
                "y": 274
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a25a75d5-f6b2-4a6d-be33-42968897da97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 66,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 64,
                "y": 342
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f503e7e9-1209-4337-be36-c2372be6c968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 191,
                "y": 138
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "999fc497-bdd1-4cd7-ab07-7452d9df524e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 185,
                "y": 206
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6d35a906-e427-4a4f-8c8d-09f35cff55b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 33,
                "y": 206
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8cf54a56-6668-455b-abdb-474dd69bec43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 125,
                "y": 206
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c2256195-5d3b-4008-b42c-a32190d88ad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 315,
                "y": 138
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ff9adce9-1b2d-45d4-9b5d-96c5df9d005a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 66,
                "offset": 0,
                "shift": 18,
                "w": 20,
                "x": 299,
                "y": 274
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2a09b7a0-db59-432f-ad45-0e30d8c29ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 155,
                "y": 206
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6580a4e9-aba9-4bf1-8436-5fcc34fa5a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 335,
                "y": 206
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ab089899-50c8-4529-afd6-56d51110c3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 66,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 156,
                "y": 342
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c8b35310-aa87-405c-b3e6-70c1ca420486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 66,
                "offset": -3,
                "shift": 16,
                "w": 16,
                "x": 402,
                "y": 274
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8f681ac2-f00e-4402-9944-1609d07d6268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 30,
                "x": 35,
                "y": 138
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1b38aad0-c0b2-413b-b81e-abd42b4914e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 66,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 168,
                "y": 342
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "810ac24f-7e22-4fe6-8434-12fda6147b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 66,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3ad591dd-9359-4c9c-820c-01c6b76a0d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 393,
                "y": 206
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a9c75b54-a888-4fee-a11c-da7e1d3a991c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 284,
                "y": 138
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0f8e0076-37fa-4575-b4cc-9be1f68112e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 95,
                "y": 206
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e7a8834a-d9ab-4204-a6a8-77533a016a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 66,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 245,
                "y": 206
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3fe28122-dd12-4e19-af53-54b31aa13a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 66,
                "offset": 2,
                "shift": 21,
                "w": 21,
                "x": 276,
                "y": 274
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "37a956c4-4ae7-41f2-aa45-26dc4667990f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 66,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 422,
                "y": 206
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f78320ff-3bc5-4769-98d3-4f1eac11ff2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 66,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 321,
                "y": 274
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5783bd4c-e7c8-4aa5-b492-f8c2731cf7ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 364,
                "y": 206
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "813e3da0-eaf0-42c6-87b4-d086e5c1b0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 66,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 129,
                "y": 138
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2b8579ec-104e-4fa9-aae4-f13bd82f937e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 66,
                "offset": 0,
                "shift": 44,
                "w": 45,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a554765a-650a-46fd-8abc-81dd25cc37a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 66,
                "offset": 0,
                "shift": 31,
                "w": 32,
                "x": 359,
                "y": 70
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bda9ea2e-4fa0-4eb3-9c40-395a6b4c7108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 66,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 160,
                "y": 138
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a298532b-c51c-4b6c-8b3e-7af33ea88e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 66,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 173,
                "y": 274
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "93a5ae98-9aa4-45a9-9132-de7924d18f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 66,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 382,
                "y": 274
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c34bcebf-98d2-4ecf-805a-e4336bdad326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 66,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 180,
                "y": 342
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f64f4b63-f849-4e91-8b43-adf5002fe5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 66,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 342,
                "y": 274
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d5f40aee-bb45-43b4-8078-dfc6e3f77fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 451,
                "y": 206
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "216ef2e6-0444-48c5-85fa-7ba4aecc4d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "f8e40288-9246-4988-a14e-2e69a092355c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "dfba935c-0754-4a09-9ac6-5b0e629f018f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "3548e7df-2249-44b6-bdd1-a62cb5f3c43b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "a6b872c8-4dde-4cbb-b6be-2da990fab54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "bc9e222e-2771-4871-bfd3-e030a2819cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 85
        },
        {
            "id": "7eaa0b94-d7c0-4884-90b9-36848c8ac07c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "5e171f00-107c-44e9-88c9-ff4d66bb0ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "2dda4479-e30c-476e-b233-1d1a2edc1362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 112
        },
        {
            "id": "3558ff14-1f0a-4d9a-b0d5-358a610975a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "848ba175-2812-4fe2-9c1e-e429e3f8ccda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "546da2f9-cc66-434d-b22f-0782521e2ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "f6a45303-87a3-4c96-b02c-bc8e323a15bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 44
        },
        {
            "id": "85ecf735-baf6-46d9-991a-ffcf3357e9fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 46
        },
        {
            "id": "3c98c86b-4832-421c-a2fb-d8df0f5eaf53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "4ef3e213-e6f4-4ab2-ad12-1b6d85c60d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "92946ad0-26c4-4406-9ed6-57a6b5448008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 196
        },
        {
            "id": "14b5e99a-66a1-4248-be44-b329d1310729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 197
        },
        {
            "id": "830edcc0-62ab-4c49-9a03-b38604027758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 220
        },
        {
            "id": "63d4cf4e-6e37-4d56-964b-a91f5580185d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 44
        },
        {
            "id": "35b2afd1-491b-49c3-84af-61737be0d5ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 46
        },
        {
            "id": "d52a528e-f5d7-4caf-ba40-56190cb38a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "e674bc9a-e4be-47d4-88fa-ba32265f43c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 65
        },
        {
            "id": "5fb85d9c-d032-4f02-a7fc-9781c1d513b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 86
        },
        {
            "id": "8e1c75d3-8d7b-427d-9ca6-14504e1646bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 87
        },
        {
            "id": "1d5ad317-2183-4c61-8ea3-8405d1958c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 89
        },
        {
            "id": "f0a40298-ff4c-471f-83f0-92be18a7bb90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 196
        },
        {
            "id": "5297cd86-3ef5-46e2-a7bd-c4e2e0af8d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 197
        },
        {
            "id": "58993419-499c-4d91-9b9d-45c1b8555bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "2523cb93-6482-4bc5-a637-532beafde276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "5532d6c8-e049-4a77-bf17-6404b0a58135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "a3560dca-953f-4459-b6cd-b9fe8f545338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "7b504e23-c2dd-4fdc-b7b0-ea3afecb1dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 101
        },
        {
            "id": "0346450a-fe5d-4294-83ca-4423ba0d9992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 111
        },
        {
            "id": "c0f9cc2c-1289-42c3-beee-169740908f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 114
        },
        {
            "id": "ceb8b687-bdb7-4102-a751-8f969953df5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 196
        },
        {
            "id": "198130db-9207-470f-9ebe-b9f32a83a4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 197
        },
        {
            "id": "1afbe716-5cfc-4144-acb6-f9404e84221f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "542e11be-de28-4304-bbde-e6eaef608a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 229
        },
        {
            "id": "470d10b9-a2ec-4a8f-9bba-0ab4a516e1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 246
        },
        {
            "id": "54b17a53-8ce8-4131-b149-6eeed0ad7662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 44
        },
        {
            "id": "1bce3429-d31a-4c24-9d48-ee5096153338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "c5f3d3d4-4637-4614-8e8e-3cec8dad1b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "2f8168a6-d831-4182-9165-5bbb3635669b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "f97bd0eb-d2ac-4d3e-a97f-948d5cb9ae74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 97
        },
        {
            "id": "6d3b0989-93dd-43f8-ba5a-784f02373b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 101
        },
        {
            "id": "3852e919-4ceb-4126-9d5a-85827aa0b256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 111
        },
        {
            "id": "81bd62c7-fb6c-4810-bbfa-5a3fbad86d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 117
        },
        {
            "id": "632fad50-ec2a-4f91-ad53-687e4b602039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 121
        },
        {
            "id": "074971fd-ae0e-4fd5-bffe-929c97234e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "030e275d-6c52-49f5-9591-aab52ffb930c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "eb8d915a-5e73-437d-84d3-dcadc08e54ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 228
        },
        {
            "id": "7cf811db-99e1-4b87-9902-fb7ac7a69067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 229
        },
        {
            "id": "fb7fa8e5-52f3-4f82-8aa8-b8d7ddd83bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 246
        },
        {
            "id": "5e06949c-86f3-4b40-8706-c4853c1108b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 252
        },
        {
            "id": "793c0096-fc0e-4ef6-adbe-f324a6b354ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "05ca2139-3795-4826-b098-89795b7cc55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "4ca8f521-8f66-41b4-9cbb-98d53c163246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "f44fb0f1-cb0f-4bd8-a57e-9568010293c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 101
        },
        {
            "id": "882461ed-8dff-479e-a357-7c56e8b0b311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 111
        },
        {
            "id": "c321d61d-9bdb-4efd-bc0b-d95378545732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "8ed7c28f-b8f4-447f-8d72-15dda5e8c4c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "79407a2f-a1af-4d80-a904-7b37b90c134e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "5ae3cd5d-3eac-459b-b941-55d110e28908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 214
        },
        {
            "id": "e5dcea0e-9815-451a-a874-ffc8b1c08f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 246
        },
        {
            "id": "930020ff-1c8f-497f-99b4-e95208564a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 252
        },
        {
            "id": "a02fd573-9eeb-4839-87f4-226ec7e7c4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "ea05fcd6-59d2-417d-8dbd-7591121b86e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "e0e2e636-faa6-454a-8c6c-869edede9813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "3f878a0c-223d-4f35-86ba-2d36b3693cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "c8ac6012-d24b-45bc-adeb-8a349a67b20e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "16c61a93-e302-4916-9d98-5f81cc37a72a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "3de5c302-7330-4ad1-8bbc-7d60bcac0980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "fcc96a72-0904-45b3-9a57-b596535c81cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "73be8968-4ce5-4e2a-a0b0-51882e6ca50b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "903c7bd2-f88e-4d2b-8ca9-41f852819067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "9b95ac2a-0748-4be0-8398-9dea00a410a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "a013b505-a746-4272-b86f-f0fbe7216993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "eba2dd45-1bea-44af-ad20-021a18899c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "606aa93b-08e2-4ad5-ab42-20d1ecbd5cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "e1efb37e-1518-4747-ba1b-e358f8110180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "3e40a4c2-330c-438e-9a7b-bd46e3ffcf85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 65
        },
        {
            "id": "6cf0f645-0fcc-4508-bc7a-430e39041aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "8b8674c1-97d3-4a29-a472-ca292c9e1d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 86
        },
        {
            "id": "36bfba43-08b5-4c33-a6d6-3a35ebbed468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "c3609d08-eb53-4a49-b84a-6f96ae6f73fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 88
        },
        {
            "id": "03e02c7b-0105-475c-8227-473de1ef9e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 89
        },
        {
            "id": "7c00170b-ae79-48aa-9612-91c45d49ba4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 196
        },
        {
            "id": "04f2ff78-b600-4e0e-8458-02ae04665240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 197
        },
        {
            "id": "1574927e-2612-4c14-9d87-73cfd8a1dba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 44
        },
        {
            "id": "66914ca7-6503-4bbb-86f9-76e35771c6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 46
        },
        {
            "id": "35149cbd-88a5-4d75-a2d3-a7e616d3e0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "9815d298-3bfb-4845-83d4-59d09993f183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "88fed9ec-a72d-45bd-a6e7-856f0945174f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "11739b40-cf73-4c5b-a7fb-96b5583212df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "022a8a24-75bd-4a72-9327-f69d941c8cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 196
        },
        {
            "id": "036cf17e-3c38-41e8-86f6-2fad9ef49c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 197
        },
        {
            "id": "29b0339c-b22f-40a5-a1e8-017ad5dceaf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 229
        },
        {
            "id": "201e5260-7091-411b-90cf-895b1da132c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 44
        },
        {
            "id": "131c8608-6b7b-400a-891f-b6cec2d5bcbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 46
        },
        {
            "id": "d1245e56-1625-46d2-bb6e-ac5818575aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 65
        },
        {
            "id": "8e49b3a0-d07c-4bda-92fc-0b1e0b18d154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "3ccd6dd5-f676-4662-b232-707cf0705f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 86
        },
        {
            "id": "04630f32-ffe1-46e4-85e4-3b63641def5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 89
        },
        {
            "id": "091a969a-20e3-4e1d-9e28-e94532f0eb7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "74b9dcca-bef7-4619-bf47-59d3a2588385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "ee4fa054-3fc3-47cc-bd8e-cd897d4e6537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "37f20de7-cc07-4b06-9159-e85ec1ae364b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "2be5c421-918a-4c55-8ce6-73a780f8de9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 85
        },
        {
            "id": "44e1c79c-01d0-4684-b1e3-f9e8a23ff1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "0eff1fc5-116c-4e71-9e2c-c0725bb669a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "7cd307c3-6db4-4f60-a2ad-67dd6b53b200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "f08f605e-e3ca-41d1-b97f-0899fdb1c2a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "39806f05-38a7-46a4-abe5-33b435c02222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "da7ae6d4-ec0c-44e5-8b12-5921f45ebb74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 119
        },
        {
            "id": "9532b2ac-b745-4958-aeb6-72618f92c6e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 214
        },
        {
            "id": "e4bdd21b-d8f6-4741-8454-cc55510cf169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 220
        },
        {
            "id": "189fca22-0802-4b53-a8cd-2bd9c93586d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "7b410955-647a-40da-9b3f-eecf8820c8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 252
        },
        {
            "id": "23f236f9-be80-452f-8518-5f005fb7919a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "af672903-88ca-49cd-a20b-0d25ce665f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "30e69a87-d6b2-4983-8afc-af9c0528887f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "3992b48f-6aaf-4212-a7c1-fbbed75413b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "cbdaf54a-7410-49fe-9b45-e373d3203210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "3784ec33-6260-4e9c-8b90-6acb864f4df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "29071af0-419c-4a99-9d1b-c1d1904065f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 67
        },
        {
            "id": "6542d25d-4751-4d2e-bd1a-16e34081848a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 71
        },
        {
            "id": "c3bb1273-a6a0-414a-b692-2ce2af6215a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 79
        },
        {
            "id": "6c75200b-df4e-4c9f-9a89-22a0da3d519b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 81
        },
        {
            "id": "615591f8-4af3-4bf7-ae7c-43d598757d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "8e1c3835-b2c0-4ce3-a90d-c3e864525fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "8f2a91f2-6117-4765-9891-1362e84db825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "bfa2caf6-257f-404a-a555-0de1ca36350e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 104
        },
        {
            "id": "be2705ac-8523-49a3-ad08-87d8c61f348f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 108
        },
        {
            "id": "effefaec-d28d-46bd-995c-6ae4421c2ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "228c4b51-0dfc-4cbb-8e07-5a452497e8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "acad2e48-b2f0-488a-b5e4-bd3e233c5356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "2003fd37-4340-4460-8488-eeda92935c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "59222a8f-c94c-41bf-85c2-575d5d2d5bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "0fb551f6-b1df-4953-a18e-b9b8cc2f282a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "eea5708f-3082-41b6-90a3-cca709eb2378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "4ad4ebb2-13c4-4e4b-8a38-755319228500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "3e7cb6de-6016-4365-89b5-d85f307688a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 196
        },
        {
            "id": "89a92ade-be5c-4658-9d45-1a8d38fd9d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 197
        },
        {
            "id": "59a7190b-ab15-47db-bca9-dbb6d1392abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 214
        },
        {
            "id": "7d34364e-b473-41e3-a88a-582e6c6631b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 229
        },
        {
            "id": "44888f4a-5475-4736-a6b7-c262957461e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "da72007b-e54f-43f9-8252-77ab026b1aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "cd38f24d-05b7-4b61-9bbe-3f069f772333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 46
        },
        {
            "id": "d3eb9505-217b-4f42-a976-67f18d5d2929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 65
        },
        {
            "id": "196dfc32-91aa-4a20-9ab8-19add7798cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 196
        },
        {
            "id": "adfd5e7b-fcaf-4ece-82dd-8e7f0382f9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 197
        },
        {
            "id": "a578f6e3-3feb-4e06-b3d2-733f2a5d46be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "bd0311bf-036e-4d2e-b5f3-40fd00e41d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "be298501-4abd-48ab-baae-8408a58dcd86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "0a91e146-8ad7-426a-9aa1-86d6caba627f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "81c46b4b-0310-40fd-9cd0-a5e9ca971995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "96d082a9-f42a-4f94-9c77-3663416f690e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "905acf12-9fdf-4ca5-b3be-6d164460cfc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 67
        },
        {
            "id": "8d96dcab-63f5-4f6a-8cfd-e09ee34bce68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 71
        },
        {
            "id": "6ef7bb12-5bc5-45ea-bea1-4e40c3b92d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 79
        },
        {
            "id": "1d752071-9301-44d1-95d4-4c73c26566ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 81
        },
        {
            "id": "2f8892f5-a5fb-446e-9836-07a98e9eaccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "c92c6212-1e9c-4ec8-a232-5e0ed34139ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "be822646-e154-429b-9842-40a9b4bc9180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 105
        },
        {
            "id": "5058e1ed-e3fd-4659-9c69-472c47163a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "efdf5beb-5821-419b-b02c-c7b09590c342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "f7f5248f-a75a-4db7-ba2a-e76ba5379e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "c22330dc-f1db-48be-bfe2-b60d6715a7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "1b6a4bed-cb12-4ef5-bb1d-c5c0060a23ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "9dd3751b-8fef-4de9-a658-3ff6a595b6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "8806989d-4887-4bae-ab30-2516b7772cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 214
        },
        {
            "id": "86dc920c-89ad-4d4c-860c-0f06f5f61fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 229
        },
        {
            "id": "bcd371d1-267c-42f8-9982-19808d14fb58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "06de35f1-94f2-4c2b-9341-68ef27e59306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "49978b3b-6748-497a-a963-5fdb41dc6e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "feb55b83-6052-4861-92fa-b09b8498e905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 67
        },
        {
            "id": "882a7ee3-b7e0-4916-b532-4a46ad449b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 71
        },
        {
            "id": "e7c342ef-173b-4c37-811e-28e751500c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 79
        },
        {
            "id": "f494fce6-81e1-4474-be1f-82d57ed5ff3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "b20fc8eb-56d3-4d74-a228-27f17baa304d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "9590e863-930d-4089-9665-4a032102843f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "e8d3efe5-1c2c-4f6c-8c78-d145a8b98930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 104
        },
        {
            "id": "7130c773-af91-463e-88ed-72b2baceddcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 105
        },
        {
            "id": "4f16c76d-baa9-4300-b08f-9fa444ed6035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "becb5b9f-0bde-4555-8125-e9863da395f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "87339567-67bd-47d5-8228-eea48310e60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 214
        },
        {
            "id": "aa5aa3ac-40c8-4675-9488-5277cc4d87eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "df54625a-2d22-49f3-9879-f516a5f18d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 67
        },
        {
            "id": "506a2fde-42be-4cd5-926e-5377a02f2b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 71
        },
        {
            "id": "b597c54e-926b-4f8d-9ab2-534cbe54b308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 79
        },
        {
            "id": "4d28c1b0-f631-41d2-bc08-3a98e5073837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 214
        },
        {
            "id": "00343108-df29-494f-9770-26db25696bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 44
        },
        {
            "id": "b33aef33-05ca-4505-b611-f4dd41ede173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "84d6d98a-c720-40a7-bdb1-33146f12905e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 46
        },
        {
            "id": "df4019e7-28c0-42e0-97d0-048df98d488c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "1813328d-e445-4a8c-b7de-ff60c8d6b64d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "441fdc20-c48a-49a2-b328-9727c6ae50be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "c7aaca41-6e3e-4095-ab0d-a4aaf1bc2bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 67
        },
        {
            "id": "de00d9e6-674e-482f-9f5b-689c07aba862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 71
        },
        {
            "id": "c1ffe60a-a860-4371-b8d1-96f799b32420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 79
        },
        {
            "id": "af7ca14e-3141-49f0-8c4a-d95de3f4d7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 83
        },
        {
            "id": "1146b882-3a25-4072-95e1-d98e006bed1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "e10f73a9-7251-43af-a3e8-3b5f9c8ca77e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 100
        },
        {
            "id": "2e425c37-6fe3-4955-97e2-9f9d8446920c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "7461897f-a86b-4981-a33a-89ca04c49a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "70e2d276-56e0-4f08-b4db-aa68a4df7aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "e5b8756c-09d9-4f0d-a6f3-10b5143f9f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "3386a68c-b7d7-435d-a3ac-e4d6c94f146e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "e71cb2c6-08b4-4044-8cc0-38719de815f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "c2045135-92f7-4444-b807-38a3975410d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "4fc03166-fade-4f30-8fef-08d0fa8945ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 196
        },
        {
            "id": "fca8d151-25c7-4e26-9295-fd9dee2dbc3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 197
        },
        {
            "id": "61a2fe9f-93cc-4c6d-81cf-ad8fb9d04244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 214
        },
        {
            "id": "8bd50ac4-58c8-41d5-bc81-6304598b71c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 229
        },
        {
            "id": "d0cf422e-28ca-4fb5-84c0-e232c450477c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "5b0f8eb4-54ef-4453-b8bb-5794cbb2a51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 103
        },
        {
            "id": "b02f5cfd-e14f-46aa-a105-090732d628c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "62a2ddf3-e2a6-44fa-af29-b0af72bc55e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "7875e2fd-3ccd-40eb-911b-380437ff5a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "0c45d311-71f7-47f6-8c97-7bae91c3b490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 104
        },
        {
            "id": "a580861b-60a6-46ca-85b2-4d8c59370769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 107
        },
        {
            "id": "7362d630-f21b-4405-a260-38e22bf35877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 108
        },
        {
            "id": "d4d057e1-5b15-407b-b39f-f574b77920a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 121
        },
        {
            "id": "fd319915-7fda-4444-8dad-833b6f7d4b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 101,
            "second": 103
        },
        {
            "id": "b5d0552a-b049-4343-87af-d637060db8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "2def88bd-ab31-4f1d-9c8c-ff257a65f88c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "6c89f053-c1b5-4930-b240-8e1adc82fb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 32
        },
        {
            "id": "49183054-ade6-4f76-a42f-d8aad964c605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 33
        },
        {
            "id": "6e8df99f-4eb7-4374-b70a-a606681f665a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "c6ecf771-d43d-4b3c-9da6-712f6713b53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 46
        },
        {
            "id": "a36e79a0-7980-4454-92e1-e6f381ab6dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 63
        },
        {
            "id": "a2e6c326-1103-4fff-841c-9456b2a239b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 101
        },
        {
            "id": "c31af3ce-e799-4bac-a25c-d08f1a4e25f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 102
        },
        {
            "id": "a4c8add4-2d52-478a-8a18-1baa457211d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 111
        },
        {
            "id": "e37e542e-1a24-407f-bfe9-e718cf48ecc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 8217
        },
        {
            "id": "5d7275f7-0449-4606-b9bd-112102d96379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 8221
        },
        {
            "id": "b547cf1b-475c-4955-add8-863ec9e6b2de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 114
        },
        {
            "id": "97a93788-19df-4f9b-acd7-d88b91cea21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 121
        },
        {
            "id": "039c7037-031f-4066-bea6-9b7c0eb83d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 101
        },
        {
            "id": "7e028d40-bf47-438e-af6a-00050594ac62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 111
        },
        {
            "id": "12431c8b-4954-4f14-9263-e8897c252ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 121
        },
        {
            "id": "e0ab2513-e8d7-4d8e-bc44-f6cd3c68fe53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 246
        },
        {
            "id": "3a4746cf-6810-4da5-a186-2c1e2609c28a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 108,
            "second": 121
        },
        {
            "id": "d9ece1ee-3f6b-4adb-90fa-835b7977749d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 118
        },
        {
            "id": "6ba322d7-6b83-4b49-aa75-94ecd4121b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8217
        },
        {
            "id": "7c74c222-82b7-4f55-8105-e0b33d2f1c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 46
        },
        {
            "id": "840724eb-9b91-43dd-8cf0-aa195c642d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 118
        },
        {
            "id": "9e19b9c3-cde5-429f-8880-bf8703a0f4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "449a0918-ec33-4bd5-a4ca-014f0e98cfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 120
        },
        {
            "id": "a08e56de-0937-45a7-88b7-8b04701111e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "fee11d41-ef0e-4fa9-a678-72f59f95ccae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 122
        },
        {
            "id": "ea64afd9-85c1-4b5d-9801-b7e6ea13579b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 46
        },
        {
            "id": "a587b40e-27df-412c-a3cf-c4b22c704617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "506d3b86-226c-4ec7-8002-585e231719e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "c754ee1b-ed6a-4c75-bd9f-05e825de6e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "50828ee3-1af4-47ef-a739-31071cced782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "75f84ca3-38e2-430c-a5ed-15e982117e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 58
        },
        {
            "id": "61a309cf-81c7-4a4a-adfe-e50c3bd7ecb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 59
        },
        {
            "id": "4a13b55a-c8bd-42eb-92b0-07e7069f0c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 97
        },
        {
            "id": "42fb012b-e4cb-4fa5-a925-fd9fdb9656d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 99
        },
        {
            "id": "3c722554-bd2c-40da-bf6d-11c00d9366fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 100
        },
        {
            "id": "da111cea-fb16-474e-83c7-78b9c3ef5800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 101
        },
        {
            "id": "ee39e553-e4f6-40c8-b717-fa6c3333a53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 103
        },
        {
            "id": "b485e259-964c-436c-914d-3c26b1c90d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 106
        },
        {
            "id": "68ef2b47-0a03-4827-b04b-d845f3183f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 107
        },
        {
            "id": "445a556d-7787-4dac-a302-976f19e134e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 108
        },
        {
            "id": "9867d8e2-6f28-499b-9d5a-87a71e123995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 109
        },
        {
            "id": "c03ad079-5add-4729-8f21-987c17faf673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 110
        },
        {
            "id": "c8677da6-4721-42b2-b3c7-403f946dda7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 111
        },
        {
            "id": "387e14e2-05e6-40da-92b6-2fea5d9e1118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 112
        },
        {
            "id": "26f0b8cd-b344-4d4e-92a1-572786dc7f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 113
        },
        {
            "id": "5dfefd0d-8d11-4cbe-8106-52e90c02d7f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 114
        },
        {
            "id": "cea359ca-ea49-4d93-870e-5afaf75e698a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 115
        },
        {
            "id": "4c9f43c7-7fbf-472f-a798-fb092d89824c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 116
        },
        {
            "id": "8477bef3-1cd4-45c9-9f58-9d531653df6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 117
        },
        {
            "id": "a79da8e8-a3cd-4085-868e-b3e80844f87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 118
        },
        {
            "id": "e6fedc50-eb1d-4efd-b9c8-503ec2b8530f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 121
        },
        {
            "id": "ab9f3c3d-a80e-4ad6-ba48-4f61837a5096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 228
        },
        {
            "id": "fcfc6720-0b2d-4738-bcbc-f10e638d3125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 229
        },
        {
            "id": "98425a6b-0e26-4050-91d3-15a4844b7a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 246
        },
        {
            "id": "44bec937-ac76-4d00-9fcd-b5676840aec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 252
        },
        {
            "id": "56f9d9ab-c542-4083-8631-406f9deeeedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 894
        },
        {
            "id": "5a5ff20f-5abd-4ea5-97b1-006d8146a01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "210ab8f3-01ed-46c9-b315-04f06785a443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "123040db-f7d0-4213-950b-78887e8d06ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "17efd4f8-6ddd-4b2b-bef5-e9ec7dcf14a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 99
        },
        {
            "id": "a5ece71a-e237-40dd-825b-41a7cb14b804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 100
        },
        {
            "id": "5d3f3e28-24de-4a12-9198-b589fe5d56d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 101
        },
        {
            "id": "68de7e06-e3db-402e-ac93-ac06d6f41a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 111
        },
        {
            "id": "ccebf8ca-5624-477c-9e16-d540d25f5a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 113
        },
        {
            "id": "4462002e-55f1-4a1e-88c2-26227e16bfd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 246
        },
        {
            "id": "f1feaa31-02d9-4fbd-aaf4-1b4b4dee4860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "dcda69c4-d1bb-4532-99bb-1298cc093f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "1d63acd6-a264-4278-92e5-1dd6e496f3e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 97
        },
        {
            "id": "a32c9d57-7450-4430-b1da-c5afb0d142f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 100
        },
        {
            "id": "12915158-13a8-4d0f-94c1-59ffedf45b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 101
        },
        {
            "id": "8c2921c3-0f07-498f-876f-98d94cb8fe62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 104
        },
        {
            "id": "9cd5d472-e51e-41d2-9750-a79d80a8bb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 111
        },
        {
            "id": "65480ffb-592d-428e-b598-388088f40100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 113
        },
        {
            "id": "4de52c42-d920-4ba7-8cc3-86bdab719c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 228
        },
        {
            "id": "bf9ebcb6-62e7-4d42-87d9-f618c54d7fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 229
        },
        {
            "id": "eaca0b4d-3865-4105-80c1-2a7b166bd0fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 246
        },
        {
            "id": "d358665b-bb9d-4986-8b3e-31c6fb541c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "11682eaf-be5d-4a9c-8087-5d698a30e0f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "ae97ec13-7a51-4a27-856c-6fc051f3388d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "04a4b0b2-1d67-4505-bd0e-0df0f32acb99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "bb04c29d-bedf-401f-b482-174fe820e108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "8df01446-c749-429c-894b-b6e57e7caf85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "63ec52dc-fc26-4efe-a323-a899d4e03592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        },
        {
            "id": "ef6d34ed-4e82-4e6b-8b3e-8998a6291681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 97
        },
        {
            "id": "cf74b3dc-8491-4d90-b86c-eb7060c0babc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 99
        },
        {
            "id": "55bb825c-4782-4248-82e7-1059946edbc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 100
        },
        {
            "id": "7ad1a999-7195-4813-b791-8bdaf8462b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 101
        },
        {
            "id": "1950d463-432c-4c97-834f-4fbeadd46123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 111
        },
        {
            "id": "d7c79072-41fe-43ce-ba58-b8f04f10105f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 228
        },
        {
            "id": "14f09c3b-0323-4f66-a94c-1c05daf58ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 229
        },
        {
            "id": "c09d47e8-97f6-4c70-a6e6-b19a945a8d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 246
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 35,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}